class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :contact_type_id
      t.string :person_id
      t.string :name
      t.boolean :is_close

      t.timestamps
    end
  end
end
