class CreateTruckComments < ActiveRecord::Migration
  def change
    create_table :truck_comments do |t|
      t.string :truck_id
      t.text :comment
      t.string :user_id
      t.boolean :is_close

      t.timestamps
    end
  end
end
