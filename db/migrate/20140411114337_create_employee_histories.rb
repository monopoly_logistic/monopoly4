class CreateEmployeeHistories < ActiveRecord::Migration
  def change
    create_table :employee_histories, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :employee_id
      t.date :employment_date
      t.date :leaving_date
      t.date :start_date
      t.date :end_date
      t.uuid :contractor_id
      t.string :contractor_name
      t.uuid :department_id
      t.string :department_name
      t.uuid :position_id
      t.string :position_name
      t.uuid :person_id
      t.uuid :staff_unit_id

      t.timestamps
    end
  end
end
