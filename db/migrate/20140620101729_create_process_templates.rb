class CreateProcessTemplates < ActiveRecord::Migration
  def change
    create_table :process_templates do |t|
      t.string :process_type_id
      t.string :name
      t.string :descr
      t.boolean :is_close

      t.timestamps
    end
  end
end
