class CreateAgreements < ActiveRecord::Migration
  def change
    create_table :agreements do |t|
      t.string :name
      t.text :descr
      t.integer :lifetime
      t.integer :time_use
      t.integer :uses_number
      t.boolean :is_close


      t.timestamps
    end
  end
end
