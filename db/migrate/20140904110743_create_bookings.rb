class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.string :booking_type_id
      t.string :truck_id
      t.string :trailer_id
      t.string :driver_id
      t.string :user_id
      t.text :comment
      t.boolean :status
      t.string :status_user_id
      t.datetime :start_date
      t.datetime :end_date
      t.string :source_id
      t.string :target_id

      t.timestamps
    end
  end
end
