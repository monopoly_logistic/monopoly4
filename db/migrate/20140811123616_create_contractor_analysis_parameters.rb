class CreateContractorAnalysisParameters < ActiveRecord::Migration
  def change
    create_table :contractor_analysis_parameters do |t|
      t.string :name

      t.timestamps
    end
  end
end
