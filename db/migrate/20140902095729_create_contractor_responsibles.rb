class CreateContractorResponsibles < ActiveRecord::Migration
  def change
    create_table :contractor_responsibles do |t|
      t.string :contractor_id
      t.string :employee_id
      t.datetime :start_date
      t.datetime :end_date
      t.boolean :is_close

      t.timestamps
    end
  end
end
