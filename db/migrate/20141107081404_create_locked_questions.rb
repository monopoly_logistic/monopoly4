class CreateLockedQuestions < ActiveRecord::Migration
  def change
    create_table :locked_questions do |t|
      t.string :question_id
      t.string :user_id

      t.timestamps
    end
  end
end
