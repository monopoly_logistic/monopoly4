class CreateTaskStatuses < ActiveRecord::Migration
  def change
    create_table :task_statuses do |t|
      t.string :name
      t.boolean :is_close

      t.timestamps
    end
  end
end
