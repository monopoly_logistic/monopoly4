class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.string :system_id
      t.string :objecttype_id
      t.datetime :start_date
      t.datetime :end_date
      t.boolean :is_close

      t.timestamps
    end
  end
end
