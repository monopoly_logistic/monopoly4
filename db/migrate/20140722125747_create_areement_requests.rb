class CreateagreementRequests < ActiveRecord::Migration
  def change
    create_table :@agreement_requests do |t|
      t.string :agreement_id
      t.string :initiator_id
      t.text :req_text
      t.datetime :start_date
      t.integer :status
      t.datetime :status_updated_at
      t.string :business_object_id
      t.string :foreign_id
      t.integer :time_use
      t.integer :uses_number
      t.timestamps
    end
  end
end
