class CreateBookingHistories < ActiveRecord::Migration
  def change
    create_table :booking_histories do |t|
      t.string :booking_id
      t.boolean :status
      t.datetime :status_start_date
      t.datetime :status_end_date
      t.string :status_user_id
      t.boolean :is_close

      t.timestamps
    end
  end
end
