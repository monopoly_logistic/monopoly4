class CreateVacations < ActiveRecord::Migration
  def change
    create_table :vacations do |t|
      t.string :employee_id
      t.string :descr
      t.date :start_date
      t.date :end_date
      t.boolean :is_close

      t.timestamps
    end
  end
end
