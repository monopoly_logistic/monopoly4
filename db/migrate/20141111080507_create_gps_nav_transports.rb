class CreateGpsNavTransports < ActiveRecord::Migration
  def change
    create_table :gps_nav_transports do |t|
      t.string :transport_id
      t.string :gps_nav_id
      t.datetime :start_date
      t.datetime :end_date
      t.string :user_id

      t.timestamps
    end
  end
end
