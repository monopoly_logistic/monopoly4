class CreateProcessActionAccesses < ActiveRecord::Migration
  def change
    create_table :process_action_accesses do |t|
      t.string :process_action_id
      t.string :process_template_id
      t.string :employee_id
      t.string :staff_unit_id
      t.datetime :start_date
      t.datetime :end_date
      t.boolean :is_close

      t.timestamps
    end
  end
end
