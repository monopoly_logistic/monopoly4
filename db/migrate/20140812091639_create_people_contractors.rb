class CreatePeopleContractors < ActiveRecord::Migration
  def change
    create_table :people_contractors do |t|
      t.string :person_id
      t.string :conractor_id
      t.string :position

      t.timestamps
    end
  end
end
