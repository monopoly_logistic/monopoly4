class ContractorsPeople < ActiveRecord::Migration
    create_table :contractors_people, id: false do |t|
      t.uuid :contractor_id
      t.uuid :person_id
    end
end
