class CreateDocumentfiles < ActiveRecord::Migration
  def change
    create_table :documentfiles, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :document_id
      t.string :fname
      t.string :descr
      t.boolean :is_close

      t.timestamps
    end
  end
end
