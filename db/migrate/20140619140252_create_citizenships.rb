class CreateCitizenships < ActiveRecord::Migration
  def change
    create_table :citizenships, id: false do |t|
      t.string :id, primary_key: true, null: false
      t.string :name
      t.boolean :is_close

      t.timestamps
    end
  end
end
