class CreateTransportmodel1cs < ActiveRecord::Migration
  def change
    create_table :transportmodel1cs do |t|
      t.string :transportmodel_1c_id
      t.string :transportmodel_1c_name
      t.string :transportmodel_id
      t.boolean :is_close

      t.timestamps
    end
  end
end
