class CreateProcessTypes < ActiveRecord::Migration
  def change
    create_table :process_types do |t|
      t.string :name
      t.string :descr
      t.boolean :is_close

      t.timestamps
    end
  end
end
