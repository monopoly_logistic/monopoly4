class CreateBookingTypes < ActiveRecord::Migration
  def change
    create_table :booking_types do |t|
      t.string :name
      t.integer :release_limit
      t.boolean :is_close

      t.timestamps
    end
  end
end
