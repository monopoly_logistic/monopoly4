class CreateResourceCharacteristics < ActiveRecord::Migration
  def change
    create_table :resource_characteristics do |t|
      t.string :name
      t.string :short_name
      t.integer :value_type
      t.boolean :is_truck
      t.boolean :is_trailer
      t.boolean :is_driver
      t.boolean :is_close

      t.timestamps
    end
  end
end
