class CreateTaskActions < ActiveRecord::Migration
  def change
    create_table :task_actions do |t|
      t.string :name
      t.text :descr
      t.boolean :is_close

      t.timestamps
    end
  end
end
