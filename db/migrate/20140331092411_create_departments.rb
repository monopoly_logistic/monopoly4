class CreateDepartments < ActiveRecord::Migration
  def change
    create_table :departments, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :parent_id
      t.integer :rgt
      t.integer :lft
      t.uuid :contractor_id
      t.string :name
      t.text :descr
      t.boolean :is_close

      t.timestamps
    end
  end
end
