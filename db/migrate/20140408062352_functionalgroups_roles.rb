class CreateFunctionalgroupsRoles < ActiveRecord::Migration
  def change
    create_table :functionalgroups_roles, id: false do |t|
      t.uuid :functionalgroup_id
      t.uuid :role_id

  end
end
end
