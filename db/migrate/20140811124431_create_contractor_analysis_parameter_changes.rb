class CreateContractorAnalysisParameterChanges < ActiveRecord::Migration
  def change
    create_table :contractor_analysis_parameter_changes do |t|
      t.string :contractor_analysis_parameter_id
      t.decimal :parameter_value, precision: 16, scale: 2
      t.datetime :start_date
      t.datetime :end_date
      t.boolean :is_close

      t.timestamps
    end
  end
end
