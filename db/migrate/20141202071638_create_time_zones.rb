class CreateTimeZones < ActiveRecord::Migration
  def change
    create_table :time_zones do |t|
      t.string :area_code
      t.integer :utc
      t.datetime :start_date
      t.datetime :end_date
      t.boolean :is_close

      t.timestamps
    end
  end
end
