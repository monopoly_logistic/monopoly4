class CreateTemplateSteps < ActiveRecord::Migration
  def change
    create_table :template_steps do |t|
      t.string :name
      t.string :process_template_id
      t.string :task_template_id
      t.string :parent_id
      t.string :parent2_id
      t.integer :rgt
      t.integer :lft
      t.boolean :is_close

      t.timestamps
    end
  end
end
