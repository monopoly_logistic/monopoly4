class CreateGpsNavs < ActiveRecord::Migration
  def change
    create_table :gps_navs do |t|
      t.string :opertor_guid
      t.integer :operator_id
      t.string :operator_info
      t.string :phone_info

      t.timestamps
    end
  end
end
