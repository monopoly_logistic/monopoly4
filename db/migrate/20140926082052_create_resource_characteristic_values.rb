class CreateResourceCharacteristicValues < ActiveRecord::Migration
  def change
    create_table :resource_characteristic_values do |t|
      t.string :resource_characteristic_id
      t.string :truck_id
      t.string :trailer_id
      t.string :driver_id
      t.string :name
      t.datetime :start_date
      t.datetime :end_date
      t.boolean :is_close

      t.timestamps
    end
  end
end
