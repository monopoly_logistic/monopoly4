class CreateTaskActionAccesses < ActiveRecord::Migration
  def change
    create_table :task_action_accesses do |t|
      t.string :task_action_id
      t.string :task_template_id
      t.string :employee_id
      t.string :staff_unit_id
      t.datetime :start_date
      t.datetime :end_date
      t.boolean :is_close

      t.timestamps
    end
  end
end
