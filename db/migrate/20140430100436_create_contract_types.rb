class CreateContractTypes < ActiveRecord::Migration
  def change
    create_table :contract_types, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.string :name
      t.text :descr
      t.boolean :is_close
      t.timestamps
    end
  end
end
