class CreateQuestionAnswers < ActiveRecord::Migration
  def change
    create_table :question_answers do |t|
      t.string :agreement_request_id
      t.string :person_id
      t.string :dn
      t.datetime :question_date
      t.datetime :answer_date
      t.string :answer_variant
      t.text :answer_text
      t.boolean :is_close

      t.timestamps
    end
  end
end
