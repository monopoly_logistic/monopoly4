class CreateContractSubjectRoles < ActiveRecord::Migration
  def change
    create_table :contract_kind_roles, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :contract_kind_id
      t.string :name
      t.text :descr
      t.boolean :is_close

      t.timestamps
    end
  end
end
