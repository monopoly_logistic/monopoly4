json.array!(@truck_comments) do |truck_comment|
  json.extract! truck_comment, :id, :truck_id, :comment, :user_id, :is_close
  json.url truck_comment_url(truck_comment, format: :json)
end
