// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
// require jquery
// require jquery-2.1.1.min
// require jquery_ujs
// require jquery.dataTables
// require bootstrap
// require bootstrap-modal
// require chartkick
// require_tree .


//= require jquery
//= require jquery_ujs
//= require jquery-ui-1.10.4.custom.min
//= require jquery.chained
//= require jquery.jgrowl
//= require jquery.maskedinput
//= require jquery.placeholder.min
//= require jquery.ui.draggable
//= require jquery.datetimepicker
//= require nestedsortables.min
//= require bootstrap
//= require jquery.dataTables
//= require jquery.dataTables2datesort
//= require dataTables.fixedColumns
//= require fnReloadAjax
//= require bootstrap-checkbox
//= require checkbox-x
//= require dropdown-submenus
// require jquery.validate.min
// require require
//= require ru
//= require main