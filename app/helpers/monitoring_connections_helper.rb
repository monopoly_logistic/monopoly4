module MonitoringConnectionsHelper
  
  def link_to_comment_con(con)
    link_to image_tag('comment.png'), {action: 'new_comment', order_from_key: con.order_from_key, order_to_key: con.order_to_key, truck: con.truck_num, format: :js} , remote: true
  end
  
  
  def last_comment_con(con)
    comment = UnvComment.unscoped.last_comment(con.order_from_key, con.order_to_key).first
    comment.comment if comment
  end
  
  def comments_count_con(con)
    UnvComment.unscoped.comments_count(con.order_from_key, con.order_to_key).size
  end
  
  def user_ability_con
    current_user.id == 'a2253748-7e9b-45d1-868c-ec7ef707af5d'.upcase or 
    current_user.id == '82eef7ae-5d9b-43f6-94ee-9473059cb8b3'.upcase or 
    current_user.id == 'a58d41fb-5055-4241-9201-147c852df985'.upcase or 
    current_user.admin
  end



  def agree_con(from_key, to_key)
    if user_ability_con
    @agree = Agree.find_agree(from_key, to_key).first
    @from_key, @to_key = from_key, to_key
    render partial: 'monitoring_connections/agree'
    end
  end
  
  
end
