module TariffCalculatorHelper
  
  def market_current_tariff tariff_grid, city_from, city_to
    tariff = tariff_grid.find { |t| t['CityFromCode'] == city_from && t['CityToCode'] == city_to } unless tariff_grid.blank?
    tariff.blank? ? 'Нет' : tariff['TariffValue']
  end
    
    
end
