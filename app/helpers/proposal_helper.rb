module ProposalHelper
  
  def can_delete_works_and_entries?
    current_user.admin || current_user.roles.pluck(:id).map { |r| r.downcase }.include?('7788B822-0B14-46A4-AE35-F650635AEE4A'.downcase)
  end
  
end
