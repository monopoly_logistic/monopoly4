module StatusTsOlHelper
  def link_to_comment_ol(truck_id, truck,sp_ol)
    link_to image_tag('add_comment.png', style: "width: 12px"),  {action: 'new_comment_ol', truck_id: truck_id, truck: truck, sp_ol: sp_ol, format: :js} , remote: true
  end
end
