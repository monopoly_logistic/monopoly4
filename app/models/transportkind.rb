# -*- encoding : utf-8 -*-
class Transportkind < ActiveRecord::Base
#  attr_accessible :descr,  :name, :transporttype_id, :is_close

  #audited

  has_many :transportmodels

  validates :name, presence: true
  belongs_to :transporttype


  scope :visible, lambda { where("transportkinds.is_close != ?", true) }

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end



end
