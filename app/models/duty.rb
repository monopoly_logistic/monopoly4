class Duty < Monosql
self.table_name = "dbo.msSubscriberTemp"

#:EmployeeKey, :IsLog, :IsMan, :MomentFrom, :MomentTo, :created_at, :user_id, :fio


  validates :EmployeeKey, presence: {:message => 'Выберите дежурного'}
  validates :MomentFrom, :MomentTo, presence: {:message => 'Выберите дату и время'}
  validates :IsLog, presence: {:message => 'Обозначьте: логист или менеджер'}, unless: :IsMan

  
  scope :last_month, lambda { where("MomentFrom >= ?", Date.today.at_beginning_of_month).order("MomentFrom desc") }


  #before_validation :parse_date
  

  def self.people
    Monosql.table_name = "dbo.msEmployee"    
    
    Monosql.find_by_sql(["SELECT e.EmployeeKey, e.LastName + ' ' + e.FirstName + ' ' + e.MiddleName FIO
FROM
  [MonopolySun].[dbo].[msEmployee] e
  JOIN [MonopolySun].[dbo].[msEmployeeInternal] ei
    ON e.[EmployeeKey] = ei.EmployeeKey
ORDER BY
  e.LastName + ' ' + e.FirstName + ' ' + e.MiddleName "])#.map {|m| [m.FIO, m.EmployeeKey]}
  end

  def fio
    Monosql.table_name = "dbo.msEmployee"
    #fn = Monosql.find_by_sql(["SELECT top (1) e.LastName + ' ' + e.FirstName + ' ' + e.MiddleName FIO from msEmployee e where e.EmployeeKey = ?", self.EmployeeKey])
    fn = Monosql.where("EmployeeKey = ?", self.EmployeeKey).limit(1)
    fn.empty? ? "-" : [fn.first.LastName, fn.first.FirstName, fn.first.MiddleName].join(' ').strip

  end

private

  def parse_date
    from = self.MomentFrom.split(/\D/)
    to = self.MomentTo.split(/\D/)
    self.MomentFrom = DateTime.new(from[2], from[1], from[0], from[3], from[4]).to_s(:db)
    self.MomentTo = DateTime.new(to[2], to[1], to[0], to[3], to[4]).to_s(:db)
  end


end
