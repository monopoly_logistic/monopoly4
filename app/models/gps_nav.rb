class GpsNav < ActiveRecord::Base
  
  has_many :gps_nav_transports
  has_and_belongs_to_many :transports, through: :gps_nav_transports  
  
  scope :visible, lambda { where("gps_navs.is_close != ? or gps_navs.is_close IS NULL", true) }
  scope :used_gps, lambda { visible.joins(:gps_nav_transports).where("gps_nav_transports.end_date > ? or gps_nav_transports.end_date IS NULL", Time.now) }
  scope :free_gps, lambda { visible - used_gps }
  
end
