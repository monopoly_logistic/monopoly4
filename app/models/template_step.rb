class TemplateStep < ActiveRecord::Base

  belongs_to :process_template
  belongs_to :task_template

  scope :visible, lambda { where("[template_steps].[is_close] != ?", true) }


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
