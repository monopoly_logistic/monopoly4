class ContractKind < ActiveRecord::Base
#  attr_accessible :contract_type_id, :descr, :id, :is_close, :name

  belongs_to :contract_type
  has_many :contract_subkinds
  has_many :contract_kind_roles

  #audited

  validates :contract_type_id, :name, presence: true

  scope :visible, lambda { where("is_close != ?", true) }

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

end
