class PeopleContractor < ActiveRecord::Base
  
  belongs_to :person
  belongs_to :contractor
  has_many :vacations, class_name: "Vacation", foreign_key: :employee_id , primary_key: :employeeId
  
end
