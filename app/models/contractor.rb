class Contractor < ActiveRecord::Base
  #  attr_accessible :contractortype_id, :end_date, :inn, :kpp, :lft, :name, :parent_id, :rgt, :start_date, :is_legal

  validates :contractortype_id, :name, :start_date, presence: true

  acts_as_nested_set

  belongs_to :contractortype
  has_many :departments
  has_many :contractor_representatives
  #has_and_belongs_to_many :people
  has_many :people, through: :people_contractors 
  has_many :bookings
  has_many :transports
  has_many :modulators
  
  has_many :contractor_responsibles, -> {visible}
  has_many :chained_contractor_responsibles, -> { chained }, :class_name => 'ContractorResponsible'
  #has_many :active_chained_contractor_responsibles, -> { active_chained }, :class_name => 'ContractorResponsible'

  scope :visible, lambda { where("contractors.is_close != ?", true).order(:short_name) }
  scope :outside, lambda { visible.joins(:contractortype).where("contractortypes.name = ?", 'Контрагент').order(:short_name) }
  scope :inside, lambda { visible.joins(:contractortype).where("contractortypes.name = ? or inn = ?", 'Организация', '7810071482').order(:short_name) }
  scope :no_responsibles, lambda { visible.includes(:contractor_responsibles).where( contractor_responsibles: { contractor_id: nil } )  }
#  scope :no_responsibles, lambda { visible.includes(:contractor_responsibles).where("contractor_responsibles.contractor_id IS NULL or contractor_responsibles.end_date < ?", Time.now).references(:contractor_responsibles)  }

  def active_chained_contractor_responsibles
    chained_contractor_responsibles.map(&:active_descendents).flatten.compact.uniq
  end
  
  def staff_units
    @staff_units = []
    departments.each { |d| @staff_units += d.staff_units  }
    @staff_units.compact
  end

  def inside?
    contractortype.name == 'Организация' || inn == '7810071482' if contractortype
  end
  
  
  def employees
    tmp = []
    staff_units.each{|su| tmp += su.employees }
    tmp.flatten.compact
  end

  def shortname_inn_kpp
    [short_name, "ИНН:#{inn}", "КПП:#{kpp}"].compact.join(', ').strip
  end
  

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
