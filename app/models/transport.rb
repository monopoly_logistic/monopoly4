class Transport < ActiveRecord::Base

  before_validation :ensure_uuid
  validates :reg_num, presence: true
  validates :year,  presence: true,
                    numericality: true,
                    length: {minimum: 4, maximum: 4},
                    allow_blank: true 
  validates :reg_num, uniqueness: true #, on: :update
  validates :vin, uniqueness: true, allow_blank: true 

  has_and_belongs_to_many :gps_navs, through: :gps_nav_transports
  has_and_belongs_to_many :enginemodels
  has_and_belongs_to_many :documents
  
  has_many :tsloadings
  has_many :tstanks

  
  belongs_to :transportexploitation
  belongs_to :transportcolor
  belongs_to :transportmodel
  belongs_to :contractor
  
  has_many :coupler_trucks, -> { visible }, :class_name => 'Coupler', :foreign_key => 'truck_id'
  has_many :coupler_trailers, -> { visible }, :class_name => 'Coupler', :foreign_key => 'trailer_id'
  has_many :chained_coupler_trucks, -> { chained }, :class_name => 'Coupler', :foreign_key => 'truck_id'
  has_many :chained_coupler_trailers, -> { chained }, :class_name => 'Coupler', :foreign_key => 'trailer_id'

  has_many :truck_drivers, -> { visible }
  has_many :chained_truck_drivers, -> { chained }, :class_name => 'TruckDriver'

  has_many :gps_nav_transports, -> { visible }
  has_many :chained_gps_nav_transports, -> { chained }, :class_name => 'GpsNavTransport'
  

  default_scope lambda { order(:reg_num) }  
  scope :visible, lambda { where("transports.is_close != ?", true) }
  scope :trucks, lambda {visible.joins(transportmodel: [ transportkind: :transporttype ] ).where("[transporttypes].id = ?", 'eee53245-a100-42d3-8b36-c4b891dca119').order(:reg_num)}
  scope :trailers, lambda {visible.joins(transportmodel: [ transportkind: :transporttype ])
            .where("[transporttypes].id = ? or [transporttypes].id = ? or [transporttypes].id = ? or [transporttypes].id = ? ",
              '1e475762-b77b-427b-99de-e9350b455b13' , 'f8902d5a-762b-4188-b698-b893fbc0553c', '1d7b592f-8d12-4c4d-bc5a-71484efc1fff', '2c505e83-7074-46b1-ad55-5254a7067476').order(:reg_num)}
          
  scope :used_trucks, lambda { visible.joins(:coupler_trucks).where("couplers.end_date > ? or couplers.end_date IS NULL", Time.now) }
  scope :used_trailers, lambda { visible.joins(:coupler_trailers).where("couplers.end_date > ? or couplers.end_date IS NULL", Time.now) }
  scope :free_trucks, lambda { trucks.includes(:chained_coupler_trucks, :chained_truck_drivers) - used_trucks }
  scope :free_trailers, lambda { trailers.includes(:chained_truck_drivers) - used_trailers.includes(:chained_truck_drivers) }
  scope :without_driver, lambda { trucks.includes(:chained_coupler_trucks, :chained_truck_drivers) - trucks.joins(:chained_truck_drivers) }
  scope :without_gps, lambda { trucks.includes(chained_gps_nav_transports: [:gps_nav, :user]) - trucks.joins(chained_gps_nav_transports: [:gps_nav, :user]) }
#  scope :without_driver, lambda { trucks.joins('LEFT OUTER JOIN truck_drivers ON truck_drivers.transport_id = transports.id').where("(truck_drivers.transport_id IS NULL) OR (NOT(truck_drivers.end_date IS NULL) OR (truck_drivers.end_date < ?))", Time.now) }
#  scope :without_driver, lambda { (trucks.includes(:truck_drivers).where(truck_drivers: { id: nil })).joins(:truck_drivers).where("truck_drivers.end_date < ?", Time.now) }
  #scope :without_driver, lambda { trucks.includes(:truck_drivers).where("(truck_drivers.transport_id IS NULL) OR truck_drivers.end_date < ?", Time.now) }

  
  def truck?
    transportmodel.transportkind_id == '27f61ba9-8d2d-489c-b890-f16423b4b935' if transportmodel
  end

  def trailer?
    !truck?
  end
  
  # --------------------------------------------------------------------------------------------
  def coupler_trailer_num
    chained_coupler_trucks.any? ? chained_coupler_trucks.map { |c| c.trailer_num }.compact.join(', ') : ""
  end  
  
  def coupler_start_date
    chained_coupler_trucks.any? ? chained_coupler_trucks.pluck(:start_date).compact.join(', ') : ""
  end
  
  def coupler_end_date
    chained_coupler_trucks.any? ? chained_coupler_trucks.pluck(:end_date).compact.join(', ') : ""
  end

  def driver_name
    chained_truck_drivers.any? ? chained_truck_drivers.map { |d| d.person_info }.compact.join(', ') : ""
  end

  def driver_start_date
    chained_truck_drivers.any? ? chained_truck_drivers.pluck(:start_date).compact.join(', ') : ""
  end

  def driver_end_date
    chained_truck_drivers.any? ? chained_truck_drivers.pluck(:end_date).compact.join(', ') : ""
  end
  # --------------------------------------------------------------------------------------------

  def gps_nav_name
    chained_gps_nav_transports.any? ? chained_gps_nav_transports.map { |g| g.gps_nav.name if g.gps_nav }.compact.join(', ') : ""
  end

  def gps_start_date
    chained_gps_nav_transports.any? ? chained_gps_nav_transports.pluck(:start_date).compact.join(', ') : ""
  end

  def gps_end_date
    chained_gps_nav_transports.any? ? chained_gps_nav_transports.pluck(:end_date).compact.join(', ') : ""
  end
  
  def editor_name
    chained_gps_nav_transports.any? ? chained_gps_nav_transports.map { |g| g.user.fullname if g.user }.compact.join(', ') : ""
  end
  # --------------------------------------------------------------------------------------------

  def inside_contractor?
    contractor.inside? if contractor
  end
  
  def document
    documents.any? ? documents.first : false
  end

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

  def transporttype
    self.transportmodel && self.transportmodel.transportkinds.first.any?  && self.transportmodel.transportkinds.first.transporttype ? self.transportmodel.transportkinds.first.transporttype : false
  end


  def enginemodel
    enginemodels.any? ? enginemodels.first : false
  end


 private 

 def ensure_uuid
    self.id ||= SecureRandom.uuid
 end


end
