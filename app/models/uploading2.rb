class Uploading2 < Monosql
  self.table_name = "dbo.msOrder_OLOP_Get_Table"
  self.time_zone_aware_attributes = false
  
  #default_scope {lock('WITH (NOLOCK)')}
  
  belongs_to :booking, -> { where("is_close != ? and status =?", true, true) }, class_name: "Booking", foreign_key: "_TruckKey", primary_key: :truck_id
  has_many :char_val_truck, class_name: "ResourceCharacteristicValue", primary_key: "_TruckKey",foreign_key: :truck_id
  has_many :char_val_trailer, class_name: "ResourceCharacteristicValue", primary_key: "_TrailerKey",foreign_key: :trailer_id
  has_many :char_val_driver, class_name: "ResourceCharacteristicValue", primary_key: "_DriverKey",foreign_key: :driver_id
  
  def char_vals
    @out = {}
    truck = char_val_truck unless self._TruckKey.blank?
    trailer = char_val_trailer unless self._TrailerKey.blank?
    driver = char_val_driver unless self._DriverKey.blank?
    
    @char_vals = [truck, trailer, driver].compact.flatten
    
    @char_vals.each do |char|
      @out[char.resource_characteristic.short_name] = char.name
    end if @char_vals
    @out
    
  end
  
  
end
