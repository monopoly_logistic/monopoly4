class ExtraSpending < ActiveRecord::Base
    
  validates :extra_spending_type_id, :person_id, :spending_sum, presence: true
  
  belongs_to :extra_spending_type
  belongs_to :user
  belongs_to :person
  belongs_to :transport
  belongs_to :user
  
  default_scope {visible} 
  scope :visible, lambda { where("is_close != ? OR is_close IS NULL", true) }

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
end
