class TruckDriver < ActiveRecord::Base
#  attr_accessible :employee_id, :end_date, :id, :is_close, :start_date, :transport_id

  validate :not_create_many_chained_drivers
  validates  :start_date, :employee_id, :transport_id, presence: true

  belongs_to :transport
  belongs_to :employee

  #audited

  scope :visible, lambda { where("[truck_drivers].[is_close] != ?", true) }
  scope :chained, lambda {visible.where("[truck_drivers].[end_date] > ? or [truck_drivers].[end_date] IS NULL", Time.now)}
  scope :unchained, lambda {visible.where("[truck_drivers].[end_date] < ?", Time.now)}
  scope :trucks, lambda { select("distinct transport_id").includes(:transport).order("transports.reg_num") } 


  def chained?
    end_date.nil? or end_date > Time.now 
  end

  def unchained?
    end_date < Time.now
  end
  
  
  def reg_num
    self.transport.reg_num if self.transport
  end 
  
  def person_fullname
    employee.person.fullname if employee.person
  end
  
  def person_info
    employee.person.fullname + " #{employee.person.birthdate}" if employee.person
  end

  def close
    self.update_attribute(:is_close, true)
  end

  def unchain_truck_driver
    self.update_attribute(:end_date, Time.now )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  
  private
   
  def not_create_many_chained_drivers
    @other_chained_driver = TruckDriver.visible.where(employee_id: employee_id).where("end_date IS NULL or end_date > ?", Time.now).limit 1 
    if @other_chained_driver.any? and @other_chained_driver.first.employee_id != employee_id and (end_date.blank? or end_date > Time.now)
        driver_error(@other_chained_driver.first.person_fullname) 
    end
  end

  def driver_error(driver)
    errors.add(:"Ошибка назначения связки:", "Действующая связка уже существует. Водитель: #{driver}")
  end
  

end
