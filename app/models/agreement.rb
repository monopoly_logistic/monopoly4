class Agreement < ActiveRecord::Base

  has_many :agreement_accesses
  has_many :agreement_requests

  scope :visible, lambda {where("[agreements].[is_close] != ?", true)}


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
