class ProcessTemplate < ActiveRecord::Base

  belongs_to :process_type
  has_many :template_steps
  has_many :process_action_accesses

  scope :visible, lambda { where("[process_templates].[is_close] != ?", true) }


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
end
