# -*- encoding : utf-8 -*-
class StaffUnit < ActiveRecord::Base
  #  attr_accessible :department_id, :descr, :head, :id, :position_id, :is_close, :rgt, :lft, :parent_id

  belongs_to :department
  belongs_to :position
  has_many :employees
  has_many :process_action_accesses
  has_many :task_action_accesses
  has_many :agreement_accesses

  #audited

  #acts_as_nested_set

  before_validation :ensure_uuid #, :delete_empty_parameters

  validates :department_id, :descr, :position_id, presence: true

  scope :visible, lambda { where("is_close != ?", true) }
  scope :heads, lambda { where("head = ?", true).visible }


  #  def employee #переопределение
  #    self.is_close ? nil : super
  #  end

#  def employees #переопределение
#    super.visible
#  end

  def employees_persons
    self.employees.map { |p| p.person  }
  end

  def contractor
    self.department and self.department.contractor ? self.department.contractor : false
  end

  def cdp #contractor + department + position
    contractor and position ?  "Контрагент: #{department.contractor.name}. Подразделение:#{department.name}. Должность: #{position.name}." : nil
  end

  def position_name #contractor + department + position
   position ?  position.name : nil
  end

  #  def self.free_units
  #    self.visible.find_all{|f| !f.employee}.sort_by(&:cdp).compact
  #  end


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


  private


  def ensure_uuid
    self.id ||= SecureRandom.uuid
  end

  def delete_empty_parameters
    Department.accessible_attributes.each do |c|
      self[c.to_sym] = nil if self[c.to_sym].blank? and  c!= 'is_close'
    end
  end


end
