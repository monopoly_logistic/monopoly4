class ResourseType < ActiveRecord::Base
#  attr_accessible :descr, :id, :name

  has_many :unv_types

  validates :name, presence: true
  
  scope :visible, lambda { where("[resourse_types].[is_close] != ?", true) }


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  

end
