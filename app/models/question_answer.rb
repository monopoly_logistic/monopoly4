class QuestionAnswer < ActiveRecord::Base

  belongs_to :agreement_request
  belongs_to :person


  scope :visible, lambda {where("[question_answers].[is_close] != ?", true)}

  validates :staff_unit_id, :agreement_id, presence: true


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

end
