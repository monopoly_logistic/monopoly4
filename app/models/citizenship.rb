class Citizenship < ActiveRecord::Base

  has_many :documentkinds

  scope :visible, lambda {where("[citizenships].[is_close] != ?", true)}


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
