class ContractKindRole < ActiveRecord::Base
#  attr_accessible :contract_kind_id, :descr, :id, :is_close, :name


  belongs_to :contract_kind
  has_many :contract_relationships

  #audited

  validates :contract_kind_id, :name, presence: true

  scope :visible, lambda { where("contract_kind_roles.is_close != ?", true) }
  scope :invisible, lambda { where("contract_kind_roles.is_close = ?", true) }

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

end
