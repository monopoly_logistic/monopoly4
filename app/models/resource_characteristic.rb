class ResourceCharacteristic < ActiveRecord::Base
  
  scope :visible, lambda { where("[is_close] != ?", true) }
  
  has_many :resource_characteristic_values



  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end  
  
end
