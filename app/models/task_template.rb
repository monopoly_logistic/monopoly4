class TaskTemplate < ActiveRecord::Base

  belongs_to :task_type
  has_many :template_steps
  has_many :task_action_accesses

  acts_as_nested_set

  scope :visible, lambda { where("[task_templates].[is_close] != ?", true) }


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
