class Transportcategory < ActiveRecord::Base
#  attr_accessible  :name, :descr, :is_close

  #audited


  has_many :transporttypes

  validates :name, presence: true

  scope :visible, lambda { where("is_close != ?", true) }

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end



end
