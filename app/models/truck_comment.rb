class TruckComment < ActiveRecord::Base
  
  
  scope :visible, lambda {where("is_close != ?", true)}
  
  scope :last_comment, ->(truck_key) { where("truck_id = ?", truck_key).order('created_at desc').limit(1).select(:comment) }

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  
end
