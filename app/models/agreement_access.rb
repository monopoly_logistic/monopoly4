class AgreementAccess < ActiveRecord::Base

  belongs_to :staff_unit
  belongs_to :agreement

  scope :visible, lambda {where("[agreement_accesses].[is_close] != ?", true)}

  validates :staff_unit_id, :agreement_id, presence: true


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
end
