class GpsNavTransport < ActiveRecord::Base
  
  validate :not_create_many_chained_gps
  validates  :start_date, :gps_nav_id, :transport_id, presence: true
  
  
  belongs_to :transport
  belongs_to :gps_nav
  belongs_to :user
  
  #default_scope lambda { visible }
  scope :visible, lambda { where("gps_nav_transports.is_close != ?", true) }
  scope :chained, lambda {visible.where("[gps_nav_transports].[end_date] > ? or [gps_nav_transports].[end_date] IS NULL", Time.now)}

 
  def unchain_gps_nav_transport
    self.update_attribute(:end_date, Time.now )
  end

  def chained?
    end_date.nil? or end_date > Time.now 
  end
  
  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

  private
  
  def not_create_many_chained_gps
    @other_chained_gps = GpsNavTransport.visible.where(gps_nav_id: gps_nav_id).where("end_date IS NULL or end_date > ?", Time.now).limit 1 
    if @other_chained_gps.any? and @other_chained_gps.first.gps_nav_id != gps_nav_id and (end_date.blank? or end_date > Time.now)
        gps_error(@other_chained_gps.first.gps_nav.name) 
    end
  end

  def gps_error(gps)
    errors.add(:"Ошибка назначения связки:", "Действующая связка уже существует. GPS: #{gps}")
  end
  
  
  
end
