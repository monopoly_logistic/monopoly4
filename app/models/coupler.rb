class Coupler < ActiveRecord::Base
#  attr_accessible :end_date, :id, :start_date, :trailer_id, :truck_id, :is_close



  before_validation :not_create_many_chained_couplers
  validates  :start_date, :trailer_id, :truck_id, presence: true
  

  belongs_to :truck, class_name: "Transport", foreign_key: "truck_id"
  belongs_to :trailer, class_name: "Transport", foreign_key: "trailer_id"

  scope :visible, lambda { where("[couplers].[is_close] != ?", true) }
  scope :chained, lambda { visible.where("[couplers].[end_date] > ? or [couplers].[end_date] IS NULL", Time.now) }
  scope :unchained, lambda { visible.where("[couplers].[end_date] < ?", Time.now) }

  
  def locked_to_date?
    !locked_to_date.nil? && locked_to_date > Time.now
  end

  def chained?
    end_date.nil? or end_date > Time.now 
  end

  def unchained?
    end_date < Time.now
  end
  
  def trailer_num
    trailer.reg_num
  end
  
  
  def close
    self.update_attribute( :is_close, true )
  end

  def close_coupler
    self.update_attribute(:end_date, Time.now)
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  
  private
  
  def not_create_many_chained_couplers
    @other_chained_truck = Coupler.visible.where("truck_id = ? and (end_date IS NULL or end_date > ?)", truck_id, Time.now).limit 1 
    if @other_chained_truck.any? and @other_chained_truck.first.trailer_id != trailer_id and (end_date.blank? or end_date > Time.now)
      coupler_error(@other_chained_truck.first.trailer.reg_num) 
    end
  end

  def coupler_error(trailer)
    errors.add(:"Ошибка назначения сцепки:", "Действующая сцепка уже существует. Прицеп: #{trailer}")
  end
end
