class TaskActionAccess < ActiveRecord::Base

  belongs_to :task_action
  belongs_to :task_template
  belongs_to :employee
  belongs_to :staff_unit


  scope :visible,  lambda { where("[task_action_accesses].[is_close] != ?", true) }


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
end
