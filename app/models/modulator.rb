class Modulator < ActiveRecord::Base
  
  attr_accessor :price, :mileage
  
  validates :contractor_id, :traffic, presence: true
  
  belongs_to :contractor 
  
  default_scope {visible}
  scope :visible, lambda { where("is_close != ? OR is_close IS NULL", true) }

  
  def self.get_contractors(from, to)
    url = "http://esbsrv01.monopoly.su/ESB/ST/Read/GetContragentList.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body =  "{'MomentFrom':'#{from}', 'MomentTo':'#{to}'}"
    response = http.request(request)
  
    @resp = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
    @resp = @resp["Data"].sort_by { |c| c.second  }
  end
  
  

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  
end
