# -*- encoding : utf-8 -*-
class System < Monopolydata

#  attr_accessible :descr, :end, :id, :is_close, :name, :start

  has_many :objecttypes
  has_many :properties

  validates :name,  presence: true

  scope :visible, lambda { where("[systems].[is_close] != ?", true) }


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

end
