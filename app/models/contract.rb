class Contract < ActiveRecord::Base
#  attr_accessible :contract_number, :contract_subkind_id, :end_date, :id, :is_close, :start_date

  belongs_to :contract_subkind

  #audited

  validates :contract_subkind_id, :contract_number, :start_date, presence: true

  scope :visible, lambda { where("is_close != ?", true) }

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end




end
