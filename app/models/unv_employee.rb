class UnvEmployee < ActiveRecord::Base
#  attr_accessible :actual_end, :actual_start, :employee_id, :id, :planned_end, :planned_start, :unv_type_id
  
  validates :unv_type_id, presence: true
  validates :planned_start, :planned_end, presence: {:message => 'Выберите дату начала и окончания периода'}
  validates :employee_id, presence: {:message => 'Выберите водителя'}
  validate :finish_cannot_be_earlier_than_start
  
  default_scope lambda { visible }
  scope :visible, lambda { where("unv_employees.is_close != 1") }

  belongs_to :employee
  belongs_to :unv_type
  has_one :person, through: :employee
  
  
  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  
  
  private
  
  def finish_cannot_be_earlier_than_start
    unless planned_start.nil? || planned_end.nil?
      time_error if planned_end < planned_start
    end
  end

  def time_error
    errors.add(:"Ошибка задания диапазона дат.", 'Фундаментальные законы природы не позволяют путешествия в прошлое')
  end
  
  
end
