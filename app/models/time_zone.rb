class TimeZone < ActiveRecord::Base
  
  default_scope lambda { visible }
  scope :visible, lambda { where("is_close != ?", true) }

  def self.get_regions
      url = "http://esbsrv01.monopoly.su/ESB/ST/Read/GetRegions.ashx"
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Post.new(uri.request_uri)
      response = http.request(request)
      
      out = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
      out["Ok"] ? out["Data"].sort_by! { |a| [ a.second ] } : false
  end

  def region_name
    regions = TimeZone.get_regions
    if regions
      regions.find { |r| r.first.downcase == self.area_code.to_s.downcase}.try(:second) || self.area_code
    end
  end
  
  
  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

  
#  
end
