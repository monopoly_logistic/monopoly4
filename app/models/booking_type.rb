class BookingType < ActiveRecord::Base
  
  has_many :bookings
    
  validates :name, presence: true

  scope :visible, lambda { where("is_close != ?", true).order(:contractor_id) }


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  
end
