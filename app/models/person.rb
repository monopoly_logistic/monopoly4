class Person < ActiveRecord::Base
  
  #  attr_accessible :birthdate, :last_name, :name, :surname, :gender, :start_date, :end_date,  :mail, :is_close, :id

  has_many :phoneuses
  has_many :phones, through: :phoneuses
  has_many :users
  has_many :employees
  has_many :contractor_representatives
  has_many :question_answers
  has_and_belongs_to_many :documents
  has_many :people_contractors 
  has_many :contractors, through: :people_contractors 
  has_many :contacts
  has_many :unv_employees, through: :employees
  has_many :vacations, through: :people_contractors

  
  #audited
  before_validation :ensure_uuid

  validates :birthdate, :last_name, :name, :surname, :id, presence: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :mail, allow_blank: true, format: { with: VALID_EMAIL_REGEX , message: "Неверный формат электронной почты." }


  default_scope lambda { visible.order('people.last_name, people.name, people.surname') }
  
  scope :visible, lambda { where("[people].[is_close] != ?", true).order('people.last_name, people.name, people.surname') }  
  scope :drivers, lambda { visible.joins(employees: {staff_unit: :position}).where("positions.name = ?", "Водитель").includes(:contractors, :people_contractors, documents: :documentkind, contacts: :contact_type ) | 
                           visible.joins(:contractor_representatives).includes(:contractors, :people_contractors, documents: :documentkind, contacts: :contact_type ) }  
  scope :drivers_without_snils, lambda { drivers.find_all { |d| d.snils == nil } }  
  scope :without_snils, lambda { where("snils IS NULL") }  
  scope :working_drivers, lambda { visible.joins(employees: {staff_unit: :position}).where("positions.name = ?", "Водитель").where.not("snils IS NULL").where("id_zup IS NOT NULL or id_zup_logistik IS NOT NULL or id_zup_mtk IS NOT NULL") }  

  
  def dismissed?
    id_zup.nil? && id_zup_logistik.nil? && id_zup_mtk.nil?
  end
  
  def truck(date)
    truck = TruckByPerson.get_truck(date.to_time.to_i, self.id).first
    truck.nil? ? '' : truck["RegNumber"].to_s
  end
  
  def vacations_from_to(from, to)
    (vacations_from_to_start(from, to) + vacations_from_to_end(from, to) + vacations_from_to_around(from, to)).uniq
  end
  
  def unv_employees_from_to(from, to)
    (unv_employees_from_to_start(from, to) + unv_employees_from_to_end(from, to) + unv_employees_from_to_around(from, to)).uniq
  end
  
  
  def vacations_from_to_around(from, to)
    vacations.where("start_date <= ? and end_date >= ?", from.to_date, to.to_date)
  end
  
  
  def vacations_from_to_start(from, to)
    vacations.where(:"vacations.start_date" => from.to_date..to.to_date)
  end
  
  def vacations_from_to_end(from, to)
    vacations.where(:"vacations.end_date" => from.to_date..to.to_date)
  end
  
  def unv_employees_from_to_around(from, to)
    unv_employees.where(unv_type_id: 'CE58D7F2-E69A-4F8C-8A36-92E9066B3D5E').where("planned_start <= ? and planned_end >= ?", from.to_date, to.to_date)
  end
  
  def unv_employees_from_to_start(from, to)
    unv_employees.where(unv_type_id: 'CE58D7F2-E69A-4F8C-8A36-92E9066B3D5E').where(:"unv_employees.planned_start" => from.to_date..to.to_date)
  end
  
  def unv_employees_from_to_end(from, to)
    unv_employees.where(unv_type_id: 'CE58D7F2-E69A-4F8C-8A36-92E9066B3D5E').where(:"unv_employees.planned_end" => from.to_date..to.to_date)
  end
  
  def employees?
    employees.any?
  end

  def edit_allowed?#Предикат для определиния права редактирования
    snils.blank?
  end
  
  def employees_cdp #contractor + department + position
    if employees?
      employees.map { |e| e.staff_unit.cdp if e.staff_unit }.join('; ')
    end
  end


  
  def document
    documents.any? ? documents.first : false
  end

  def passport
    documents.joins(:documentkind).where("documentkinds.name = ?", 'Паспорт гражданина Российской Федерации').map { |p| ["#{p.series} #{p.number}", p.issued_by, p.issued_date].join(', ')}.join('; ')
  end

  def passport_num
    documents.joins(:documentkind).where("documentkinds.name = ?", 'Паспорт гражданина Российской Федерации').map { |p| "Паспорт:#{p.series} №:#{p.number}"}.join('; ')
  end

  def foreign_passport
    documents.joins(:documentkind).where("documentkinds.name = ?", 'Паспорт иностранного гражданина').map { |p| ["#{p.series} #{p.number}", p.issued_by, p.issued_date].join(', ')}.join('; ')
  end

  def drivers_license
    documents.joins(:documentkind).where("documentkinds.name = ?", 'Водительское удостоверение').map { |p| ["#{p.series} #{p.number}", p.issued_date].join(', ')}.compact.join('; ')
  end
  
  def contact_phone
    contacts.joins(:contact_type).where('contact_types.name = ?', 'Номер телефона').map(&:name).join(', ')
  end
  
  def fullname
    [last_name, name, surname].compact.join(' ').strip
  end

  def fullname_g_b
    [last_name, name, surname, gender ? "(М)" : "(Ж)", birthdate].compact.join(' ').strip
  end

  def fullname_passport
    [last_name, name, surname, passport_num].compact.join(' ').strip
  end


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

  def ensure_uuid; self.id ||= SecureRandom.uuid end
  
  private
  
  class TruckByPerson < Monosql  
    def self.get_truck(date, person)
      connection.select_all("exec [MonopolySunTemp].[dbo].[ms_TruckByPerson_Get] @person_id = '#{person}', @Moment = #{date}").to_a
    end
  end
  

end
