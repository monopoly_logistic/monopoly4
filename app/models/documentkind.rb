# -*- encoding : utf-8 -*-
class Documentkind < ActiveRecord::Base
  #  attr_accessible :descr, :documenttype_id, :is_close, :is_needed, :name, :id

  #audited

  has_one :documentattr, dependent: :destroy
  accepts_nested_attributes_for :documentattr
  belongs_to :documenttype
  belongs_to :citizenship
  has_many :documents

  scope :visible, lambda { where("is_close != ?", true).order(:name) }
  scope :for_person, lambda { visible.where("documenttype_id = ?",'bd591fd6-35f8-405b-9372-5120e47eb260').order(:name) }
  scope :for_transport, lambda { visible.where("documenttype_id = ?", '38e1a84f-f58d-4f37-8ba7-d0435add8873').order(:name)}



  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end



end
