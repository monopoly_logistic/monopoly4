class MonitoringResourceUnavailable < Monosql

	def self.check_availability(moment_from, moment_to)
		connection.select_all("exec [MonopolySunTemp].[dbo].[ms_Inaccessibility_Get_ForPortal] @MomentFrom = #{moment_from}, @MomentTo = #{moment_to}").to_a
	end

end
