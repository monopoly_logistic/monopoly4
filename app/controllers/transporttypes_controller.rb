class TransporttypesController < ApplicationController

  def index
    @transporttypes = Transporttype.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end


  def new
    @transporttype = Transporttype.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  def edit
    @transporttype = Transporttype.find(params[:id])
  end


  def create
    @transporttype = Transporttype.new(transporttype_params)
    respond_to do |format|
      if @transporttype.save
        format.html { redirect_to transporttypes_path, notice: 'Тип ТС создан' }
      else
        format.html { render action: "new" }
      end
    end
  end


  def update
    @transporttype = Transporttype.find(params[:id])

    respond_to do |format|
      if @transporttype.update_attributes(transporttype_params)
        format.html { redirect_to transporttypes_path, notice: 'Тип ТС сохранен' }
      else
        format.html { render action: "edit" }
      end
    end
  end


  def destroy
    @transporttype = Transporttype.find(params[:id])
    @transporttype.close

    respond_to do |format|
      format.html { redirect_to transporttypes_url }
    end
  end


 private

 def transporttype_params
   params.require(:transporttype).permit(:name, :transportcategory_id , :descr, :is_close)
 end


end
