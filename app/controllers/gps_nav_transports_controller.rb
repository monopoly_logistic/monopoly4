class GpsNavTransportsController < ApplicationController
  before_action :set_gps_nav_transport, only: [:edit, :update, :destroy]
  respond_to :html, :js

  def index
    user_session[:gps_nav_transports] = params[:vis] unless params[:vis].blank?
    
    @trucks =
      case user_session[:gps_nav_transports]
      when 'without_gps'
        Transport.without_gps
      else
        Transport.trucks.includes(chained_gps_nav_transports: [:gps_nav, :user])
      end

  end

  def show_gps_nav
    @truck = Transport.find(params[:id])
    @gps_nav_transports = @truck.gps_nav_transports.order("created_at desc")
    respond_to do |format|
      format.js {  }
    end
  end
  
  def unchain_gps_nav
    @gps_nav_transport = GpsNavTransport.find(params[:id])
    @gps_nav_transport.unchain_gps_nav_transport
    index 
    @truck = Transport.find(@gps_nav_transport.transport_id)
    @gps_nav_transports = @truck.gps_nav_transports.order("created_at desc")

    respond_to do |format|
      format.js do 
        flash[:notice] = 'GPS отвязан'
        render :index
      end
    end
    
  end
  
  
  def new
    @truck = Transport.find(params[:truck])
    @gps_nav_transport = GpsNavTransport.new
    @gps_nav_transport.transport_id = @truck.id
    @gps = GpsNav.free_gps
  end

  def edit
    @gps_nav_transport = GpsNavTransport.find(params[:id])
    @truck = @gps_nav_transport.transport
    @gps = GpsNav.free_gps
    render :new
  end

  def create
    @gps_nav_transport = GpsNavTransport.new(gps_nav_transport_params)
    index
    @truck = @gps_nav_transport.transport
    @gps_nav_transports = @truck.gps_nav_transports.order("created_at desc")
    if @gps_nav_transport.save
      flash[:notice] = 'Связка сохранена' 
      render :index
    else
      @gps = GpsNav.free_gps
      render :new 
    end
  end  


  def update
    index
    @truck = @gps_nav_transport.transport
    @gps_nav_transports = @truck.gps_nav_transports.order("created_at desc")
    if  @gps_nav_transport.update(gps_nav_transport_params)
      flash[:notice] = 'Связка сохранена' 
      render :index
    else
      @gps = GpsNav.free_gps
      render :new 
    end
  end

  def destroy
    @gps_nav_transport.close
    respond_with(@gps_nav_transport)
  end

  private
    def set_gps_nav_transport
      @gps_nav_transport = GpsNavTransport.find(params[:id])
    end

    def gps_nav_transport_params
      params[:gps_nav_transport][:user_id] = current_user.id
      params.require(:gps_nav_transport).permit(:transport_id, :gps_nav_id, :start_date, :end_date, :user_id, :is_close)
    end
end
