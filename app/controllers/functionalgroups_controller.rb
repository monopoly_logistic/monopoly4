class FunctionalgroupsController < ApplicationController


  def index
    @functionalgroups = Functionalgroup.all.sort_by(&:lft)

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  def new
    routes
    @functionalgroup = Functionalgroup.new
    @functionalgroup.parent_id = params[:id] unless params[:id].blank?

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @functionalgroup }
    end
  end


  def edit
    routes
    @functionalgroup = Functionalgroup.find(params[:id])
  end


  def create
    @functionalgroup = Functionalgroup.new(functionalgroup_params)
    @functionalgroup.id = SecureRandom.uuid
    respond_to do |format|
      if @functionalgroup.save
        Functionalgroup.rebuild!
        format.html { redirect_to functionalgroups_url, notice: 'Функционал добавлен' }
        format.js {}
      else
        format.html { render action: "new" }
        format.js {  }
      end
    end
  end


  def update
    @functionalgroup = Functionalgroup.find(params[:id])

    respond_to do |format|
      if @functionalgroup.update_attributes(functionalgroup_params)
        Functionalgroup.rebuild!
        format.html { redirect_to functionalgroups_url, notice: 'Функционал сохранен' }
        format.js {  }
      else
        format.html { render action: "edit" }
        format.js {  }
      end
    end
  end


  def destroy
    @functionalgroup = Functionalgroup.find(params[:id])
    @functionalgroup.close

    respond_to do |format|
      format.html { redirect_to functionalgroups_url }
    end
  end


  def savesort

    neworder = JSON.parse(params[:set])
    prev_item = nil
    neworder.each do |item|
      dbitem = Functionalgroup.find(item['id'])
      prev_item.nil? ? dbitem.move_to_root : dbitem.move_to_right_of(prev_item)
      sort_children(item, dbitem) unless item['children'].nil?
      prev_item = dbitem
    end
    Functionalgroup.rebuild!
    @functionalgroups = Functionalgroup.order(:lft)
    render :update_menu
  end

  def sort_children(element,dbitem)
    prevchild = nil
    element['children'].each do |child|
      childitem = Functionalgroup.find(child['id'])
      prevchild.nil? ? childitem.move_to_child_of(dbitem) : childitem.move_to_right_of(prevchild)
      sort_children(child, childitem) unless child['children'].nil?
      prevchild = childitem
    end
  end

  def update_menu
  end

  private
  
  def routes
  
    routes = Rails.application.routes.routes.map{ |route| {controller: route.defaults[:controller], action: route.defaults[:action]} }
#    routes =Rails.application.routes.routes.map{ |route| route.constraints[:request_method] }.second
    @routes = {}
    routes.each{|m| @routes[m[:controller]] = [] unless m[:controller].nil? }
    routes.each{|m| @routes[m[:controller]] += [m[:action]] unless m[:controller].nil? }
    @ctrl = @routes.keys.uniq.sort
    @act = []
    @routes.each { |k, v| v.each { |a| @act << [k, a] unless ['update', 'create', 'destroy'].include?(a)  } }
    @act.uniq!.sort!
  end
  
  

  def functionalgroup_params
    params[:functionalgroup].delete(:parent_id) if params[:functionalgroup][:parent_id].blank?
    params[:functionalgroup][:link] = '/' + params[:functionalgroup][:controller] + (params[:act] == 'index' ? '' : '/' + params[:act]) if params[:functionalgroup][:link].blank?
    params.require(:functionalgroup).permit(:descr, :link, :controller, :name, :part, :is_close , :rgt, :lft, :parent_id, :id)
  end


end
