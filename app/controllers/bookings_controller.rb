require 'unv'

class BookingsController < ApplicationController
  B_MENU = 
    [ 
      {name: 'Модуль планирования', id: 0, part: nil, referrer: 'get_bookings'},
      {name: 'Транспорт для планирования', id: 1, part: '_tr_planning', referrer: 'transport_planning'},
      {name: 'Открытые заявки', id: 2, part: '_open_orders', referrer: 'open_orders'},
      {name: 'Результат планирования', id: 3, part: nil, referrer: 'planned'}
    ]
  
  
  before_action :set_booking, only: [:show, :edit, :update, :destroy]
  before_action :all_params, only: [:select_filter, :get_column_values, :show_filter_and_search]
  before_action :get_ref
  
  def get_bookings 
    
    @b_menu = BookingsController::B_MENU.find { |b| b[:referrer] == 'get_bookings'} unless request.xhr?
    @ref = @b_menu[:referrer]
    get_bookings_data_query
    respond_to do |format|
      format.html 
      format.js {} 
    end
  end

  def transport_planning
    @b_menu = BookingsController::B_MENU.find { |b| b[:referrer] == 'transport_planning'}
    @ref = @b_menu[:referrer]
    get_bookings_data_query
    render :get_bookings 
  end  

  def open_orders
    @b_menu = BookingsController::B_MENU.find { |b| b[:referrer] == 'open_orders'}
    @ref = @b_menu[:referrer]
    get_bookings_data_query
    render :get_bookings 
  end  
  
  def planned
    @b_menu = BookingsController::B_MENU.find { |b| b[:referrer] == 'planned'}
    @ref = @b_menu[:referrer]
    get_bookings_data_query
    render :get_bookings 
  end
  
  def save_param
    user_session["b_filter_#{@ref}"] ||= []
    unless params[:sort_filters].blank?
      s_f = params[:sort_filters].split(',')
      new_b_filter = []
      s_f.each { |f| new_b_filter << user_session["b_filter_#{@ref}"][f.to_i]}
      user_session["b_filter_#{@ref}"] = new_b_filter
    end
    user_session["b_per_page_#{@ref}"] = params[:per_page] unless params[:per_page].blank?
    unless params[:search].blank?
      params[:search].split(',').each do |search|
        user_session["b_filter_#{@ref}"] << [ user_session["b_col_name_#{@ref}"],  search.strip, 'ИЛИ' ] unless search.strip.blank?
        user_session["b_filter_#{@ref}"].uniq!
      end
    end
    unless params[:filter].blank?
      user_session["b_filter_#{@ref}"] << [ params[:col_name],  params[:filter], user_session["b_filter_#{@ref}"].blank? ? 'ИЛИ' : (params[:logic_op].blank? ? 'ИЛИ' : params[:logic_op]) ]
      user_session["b_filter_#{@ref}"].uniq!
    end
    user_session["b_sort_id_#{@ref}"] = params[:sort_ind] unless params[:sort_ind].blank?
    user_session["b_sort_dir_#{@ref}"] = params[:sort_dir] unless params[:sort_dir].blank?
    user_session["b_col_name_#{@ref}"] = params[:col_name] unless params[:col_name].blank?
    user_session["b_page_#{@ref}"] = params[:page] unless params[:page].blank?
  
    if request.delete?
      user_session["b_filter_#{@ref}"].delete_at(params[:id].to_i) 
    end  
    user_session["b_filter_#{@ref}"].first[2] = 'ИЛИ' unless user_session["b_filter_#{@ref}"].blank?
   
    clear_session_params if params[:clear] && params[:page].blank?
    get_bookings_data_query
    render :get_bookings
  end
  
  
  def show_filter_and_search
    @title = "Фильтрация данных"
    @render = 'filter_and_search'
    render :show_popup
  end
  
  def get_column_values
    @current_column_values = Booking.get_column_values(params[:col_name])
    @ind = @col_names.index(params[:col_name])
    respond_to { |format|  format.js {} }
  end
  
  
  def select_filter
    @for_filter = Booking.get_column_values(params[:col_name])
    user_session["b_col_name_#{@ref}"] = params[:col_name]
    all_params
  end
  
  def columns; end
    
  def save_xls
    get_bookings_data_query
    @invisible_cols = []
    @col_names.each_with_index { |cn,ind | @invisible_cols << ind if cn.index('#') }
    @col_names = @col_names.find_all { |u| !u.index('#') }

    @get_bookings.map! { |con| con.instance_values.values }
    @get_bookings.each { |con| con.delete_if.with_index { |c, ind| @invisible_cols.include?(ind) }  }

    respond_to do |format|
      format.xls {
        connections = Spreadsheet::Workbook.new
        list = connections.create_worksheet name: 'get_bookings'
        list.row(0).concat @col_names
        @get_bookings.each_with_index { |con, i| list.row(i+1).push *con  }
        header_format = Spreadsheet::Format.new color: :green, weight: :bold
        list.row(0).default_format = header_format
        #output to blob object
        blob = StringIO.new("")
        connections.write blob
        #respond with blob object as a file
        send_data blob.string, type: :xls, filename: "Брони_#{Time.now.to_i.to_s}.xls"
        }
    end
  end

  
  def characteristic_val
    @info = params[:info]
    @resource_characteristic_value = 
      if params[:truck_id]  
        ResourceCharacteristicValue.where(truck_id: params[:truck_id], resource_characteristic_id: get_resource_char(params[:char]).id).limit(1).first
      elsif params[:trailer_id]  
        ResourceCharacteristicValue.where(trailer_id: params[:trailer_id], resource_characteristic_id: get_resource_char(params[:char]).id).limit(1).first
      elsif params[:driver_id]  
        ResourceCharacteristicValue.where(driver_id: params[:driver_id], resource_characteristic_id: get_resource_char(params[:char]).id).limit(1).first
      end || ResourceCharacteristicValue.new
      
    if @resource_characteristic_value.new_record?  
      @resource_characteristic = get_resource_char(params[:char]) if get_resource_char(params[:char])
      @resource_characteristic_value.resource_characteristic_id = @resource_characteristic.id
      @resource_characteristic_value.truck_id = params[:truck_id]
      @resource_characteristic_value.trailer_id = params[:trailer_id]
      @resource_characteristic_value.driver_id = params[:driver_id]
    else
      @resource_characteristic = @resource_characteristic_value.resource_characteristic
    end
    @title = "Характеристики"
    @render = 'bookings/charact_form'
    render :show_popup
  end
  
  def select_truck
    @trucks = Booking.get_trucks
    if @trucks['Ok']
      active_booking_trucks = Booking.active.where("user_id != ?", current_user.id).pluck(:truck_id).map { |a| a.downcase }
      @trucks = @trucks['Data']
      @trucks.delete_if { |t| active_booking_trucks.include?(t.first.downcase) } 
      @trucks.unshift ["", "ТС отсутствует"]
    else
      @error = @trucks['ErrorMessage']
    end  
    @order_key = params[:order_key]
    @next_entry_num = params[:next_entry_num]
    @title = "Назначение ресурса"
    @render = 'select_truck'
    render :show_popup
  end
  
  def save_truck
    @order_key = params[:order_key]
    @truck = params[:truck]
    @resp = Booking.set_truck(@order_key, @truck, current_user.id)
    unless @resp['Ok']
      @error_save = @resp['ErrorMessage']
      select_truck
      render :select_truck
    else
      @popup_close = true
      get_bookings
      render :get_bookings
    end  
  end
  
  def empty_mileage
    @regions = Booking.get_regions
    @contractors = get_contractors
    t = params[:time]
    t.insert(6, "20")
    @time =       
      if params[:status] != 'Выгружен' && params[:status] != 'Готов' 
        t[0..t.index('(')-2]
      else
        Time.now + (t.index('+') ? t[t.index('+')+1..t.index(')')-1].to_i.hours : 0.hours)
      end
    
    @truck_num = params[:truck_num]
    @truck_key = params[:truck_key]
    @order_key = params[:order_key]
    @loading_date_time = params[:loading_date_time]
    if @regions['Ok']
      @regions_to = @regions['Data'].map { |r| [r.first, "#{r.second} (#{r.third})"]}.sort_by { |a| a.second  }
      @regions_from = @regions['Data'].find_all { |r| r.first == params[:citykey_release].downcase}.map { |r| [r.first, "#{r.second} (#{r.third})"]}.sort_by { |a| a.second  }
      @selected_city = @regions_from.first
      if @selected_city
        locations = Booking.get_city_locations(@selected_city.first) 
        @city_locations = locations['Data'].sort_by { |a| a.second  } if locations && locations['Ok']
      else
        @regions_from = @regions_to
      end  
    else
      @error = @regions_to['ErrorMessage']
    end  
    @title = "Назначение пустого пробега"
    @render = 'empty_mileage'
    render :show_popup
  end
  
  def get_city_locations
    @city_locations = Booking.get_city_locations(params[:city_key])
#    render js: "alert('#{@city_locations}')"
    if @city_locations['Ok']
      @city_locations = @city_locations['Data'].sort_by { |a| a.second  }
    else
      @error = @city_locations['ErrorMessage']
    end 
  end
  
  
  def save_empty_mileage
    @resp = Booking.set_empty_mileage params[:order_key], params[:truck_key], current_user.id, params[:from_location], params[:to_location], 
                  params[:departure_time].to_time.to_i, params[:arrival_time].to_time.to_i, params[:contractor], params[:loading_date_time].blank? ? nil : params[:loading_date_time].to_time.to_i
    if @resp['Ok']
      render js: "$('#popup').hide(); $('#popup_content').html('')"
    else
      @error = @resp['ErrorMessage']
      @title = "Назначение пустого пробега"
      @render = 'empty_mileage'
      render :show_popup
    end 
  end
  
  def show_popup
    
  end
  
  # ++++++++++++++++++++++++++++++++++++++++++++ Bookings model
  
  
  def index
    @bookings = Booking.all
  end
  
  def show; end

  def new
    respond_to do |format|
      format.html {@booking = Booking.new}
      format.js do
        @truck_num = params[:truck_num]
        @booking ||= Booking.new(
        truck_id: params[:truck_key], 
        trailer_id:   params[:trailer_key], 
        driver_id:   params[:driver_key], 
        source_id: nil, 
        target_id: nil,
        booking_type_id: BookingType.all.where(name: "Под заявку").first.id,
        status: true,
        start_date: Time.now,
        end_date: Time.now + 2.hours
      )
        @title = "Новая бронь"
        @render = 'js_form'
        render :show_popup
      end
    end    
  end

  def edit
    respond_to do |format|
      format.html {}
      format.js do
        @title = "Редактирование брони"
        @render = 'js_form'
        render :show_popup
      end      
    end
  end

  def create
    @booking = Booking.new(booking_params)
    respond_to do |format|
      format.html do
        @booking.save ? redirect_to( contacts_url, notice: 'Бронь добавлена') : render(:new)
      end  
      format.js do 
        @booking.id = SecureRandom.uuid
        @booking.user_id = current_user.id
        @booking.status_user_id = current_user.id
        @booking.source_id = nil 
        @booking.target_id = nil
        @booking.booking_type_id = BookingType.all.where(name: "Под заявку").first.id
        @booking.status = true
        @booking.start_date = Time.now + 3.hours
        @booking.end_date = Time.now + 5.hours
        if @booking.save
          Booking.push_booking
          flash.now[:notice] = 'Бронь добавлена'
        else
          @title = "Редактирование брони"
          @render = 'js_form'
          render :show_popup
        end
      end
    end
  end

  def update
    respond_to do |format|
      format.html do
        @booking.update(booking_params) ? redirect_to( @booking, notice: 'Бронь сохранена') : render(:edit)
      end  
      format.js do 
        @booking.attributes = booking_params
        @booking.user_id = current_user.id
        @booking.status_user_id = current_user.id
        @booking.source_id = nil 
        @booking.target_id = nil
        @booking.booking_type_id = BookingType.all.where(name: "Под заявку").first.id
        @booking.status = true
        if @booking.save
          Booking.push_booking
          flash.now[:notice] = 'Бронь сохранена'
          render :create
        else
          render(:edit)
        end
      end
    end

  end

  def destroy
    @booking.close
    @booking.update_attributes(status: false, status_user_id: current_user.id)
    respond_to do |format|
      format.html { redirect_to bookings_url, notice: 'Бронь удалена' }  
      format.js { Booking.push_booking; flash.now[:notice] = 'Бронь удалена'; render :create} 
    end
  end  

  # ==============================================================================================================
  private
  
  def get_bookings_data_query
    all_params
    @get_bookings = Booking.get_booking current_user, @filter, @b_menu.blank? ? nil : @b_menu[:id]
    
    if @filter.any? and @get_bookings
      @tmp = []
      @filter.each do |filter|
        ind = @col_names.index(filter[0])
        attr_name = @keys[ind]
        
        if filter[2].blank? or filter[2] == 'ИЛИ'
          if filter[1] == 'Не пусто'
            @tmp += @get_bookings.find_all { |u| !u.send(attr_name).to_s.strip.blank? }
          elsif filter[1] == 'Пусто'
            @tmp += @get_bookings.find_all { |u| u.send(attr_name).to_s.strip.blank? }
          else
            @tmp += @get_bookings.find_all { |u| u.send(attr_name).to_s.strip == filter[1].to_s.strip }
          end
        elsif filter[2] == 'И'
          if filter[1] == 'Не пусто'
            @tmp = @tmp.find_all { |u| !u.send(attr_name).to_s.strip.blank? }
          elsif filter[1] == 'Пусто'
            @tmp = @tmp.find_all { |u| u.send(attr_name).to_s.strip.blank? }
          else
            @tmp = @tmp.find_all { |u| u.send(attr_name).to_s.strip == filter[1].to_s.strip }
          end          
        elsif filter[2] == 'И НЕ'
          if filter[1] == 'Не пусто'
            @tmp = @tmp.find_all { |u| u.send(attr_name).to_s.strip.blank? }
          elsif filter[1] == 'Пусто'
            @tmp = @tmp.find_all { |u| !u.send(attr_name).to_s.strip.blank? }
          else
            @tmp = @tmp.find_all { |u| u.send(attr_name).to_s.strip != filter[1].to_s.strip }
          end          
        end  
      end 
      @get_bookings = @tmp
    end     
              
    @col_values = []    
    @keys.each do |k|
    @col_values <<  @get_bookings.uniq { |b| b.send(k) }.map { |b| b.send(k).to_s.strip.delete("'") unless b.send(k).to_s.blank? }.compact.sort
      if k == 'release_date' || k == 'loading_date'
        @col_values.last.sort! { |a, b| is_date(a) <=> is_date(b) }
      end
      @col_values.last << 'Не пусто' unless @col_values.last.blank?
      @col_values.last << 'Пусто'
    end if @keys
         
    #@get_bookings.uniq!
    
    if @sort_ind and @sort_dir and @get_bookings
      @attr_name = @keys[@sort_ind.to_i]
      if @sort_dir == 'up'
        if ['release_date', 'loading_date'].include? @attr_name
          @get_bookings.sort! { |a, b| is_date(a.send(@attr_name)) <=> is_date(b.send(@attr_name)) }
        else
          @get_bookings.sort! { |a, b| a.send(@attr_name).to_s <=> b.send(@attr_name).to_s }
        end  
      else
        if ['release_date', 'loading_date'].include? @attr_name
          @get_bookings.sort! { |a, b| is_date(b.send(@attr_name)) <=> is_date(a.send(@attr_name)) }  
        else
          @get_bookings.sort! { |a, b| b.send(@attr_name).to_s <=> a.send(@attr_name).to_s }
        end            
      end  
    end
    @get_bookings_count = @get_bookings.size
    
    tmp =
      if @per_page == 'Все'
        user_session["b_page"] = 1
        @page = 1
        @get_bookings.paginate(page: 1, per_page: @get_bookings_count)
      else  
        @get_bookings.paginate(page: @page, per_page: @per_page)
      end  
    
    @get_bookings =
      if tmp.total_pages < @page.to_i
        user_session["b_page"] = 1
        @page = 1
        @get_bookings.paginate(page: 1, per_page: @per_page ) 
      else  
        tmp
      end  
  end
  
  def get_ref
    @ref = Rails.application.routes.recognize_path(request.referrer)[:action]
    @b_menu ||= BookingsController::B_MENU.find { |b| b[:referrer] == @ref}
  end
  
  def all_params  
    @logic_ops = Booking::FILTER_LOGICAL_OPS
    @col_names = Booking::COLUMN_NAMES
    @keys = Booking::COLUMN_KEYS
    
    @sort_ind = user_session["b_sort_id_#{@ref}"]
    @sort_dir = user_session["b_sort_dir_#{@ref}"]
    @col_name = user_session["b_col_name_#{@ref}"] # столбец фильтрации 
    @filter = user_session["b_filter_#{@ref}"] || []
    @page = user_session["b_page_#{@ref}"] || 1 
    @per_page = user_session["b_per_page_#{@ref}"] || (@ref == 'get_bookings' ? '20' : '50')
  end
  
  def clear_session_params
    user_session.keys.each { |s| user_session[s] = nil if s.to_s.index(@ref) } if @ref
    all_params
  end
  
  def is_date(str)
    begin
      DateTime.strptime(str, '%d.%m.%y').to_i.to_s
    rescue # ArgumentError
      str.to_s
    end
  end
  
  def get_resource_char(char)
    ResourceCharacteristic.where(short_name: char).limit(1).first || false
  end
  
  
  def set_booking
    @booking = Booking.find(params[:id])
  end

  def booking_params
    params[:booking][:driver_id] = nil if params[:booking][:driver_id].blank?
    params[:booking][:truck_id] = nil if params[:booking][:truck_id].blank?
    params[:booking][:trailer_id] = nil if params[:booking][:trailer_id].blank?
    params[:booking][:contractor_id] = nil if params[:booking][:contractor_id].blank?
    params.require(:booking).permit(:booking_type_id, :truck_id, :trailer_id, :driver_id, :user_id, :comment, :status, :status_user_id, :source_id, :target_id, :id, :contractor_id)
  end

  def get_contractors
    url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetContractors.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
  
  
end
