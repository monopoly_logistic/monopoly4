class ContractorRepresentativesController < ApplicationController


  def index
    #   @contractor_representatives = ContractorRepresentative.all
    @contractor_representatives_count = ContractorRepresentative.count
    @contractor_representatives = ContractorRepresentative.all#.paginate(page: params[:page], per_page: 20)
    @contractor_representative = ContractorRepresentative.new
    respond_to do |format|
      format.html # index.html.erb
    end
  end


  def show
    @contractor_representative = ContractorRepresentative.find(params[:id])

    respond_to do |format|
      format.js { }
    end
  end


  def new
    @contractor_representative = ContractorRepresentative.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end


  def edit
    @contractor_representative = ContractorRepresentative.find(params[:id])
  end


  def create
    @contractor_representative = ContractorRepresentative.new(contractor_representative_params)
    @contractor_representatives_count = ContractorRepresentative.count
    @contractor_representatives = ContractorRepresentative.all
    respond_to do |format|
      if @contractor_representative.save
        format.html { redirect_to contractor_representatives_url, notice: 'Представитель контрагента добавлен.' }
        format.js { @contractor_representative = ContractorRepresentative.new; flash[:notice] = 'Представитель контрагента сохранен' }
      else
        format.html { render action: "new" }
        format.js { render :update}
      end
    end
  end


  def update
    @contractor_representative = ContractorRepresentative.find(params[:id])

    respond_to do |format|
      if @contractor_representative.update_attributes(contractor_representative_params)
        format.html { redirect_to contractor_representatives_url, notice: 'Представитель контрагента сохранен.' }
        format.js { @contractor_representative = ContractorRepresentative.new; flash[:notice] = 'Представитель контрагента сохранен'; render :create }
      else
        format.html { render action: "edit" }
        format.js { }
      end
    end
  end


  def destroy
    @contractor_representative = ContractorRepresentative.find(params[:id])
    @contractor_representative.close
    #@contractor_representative.destroy

    respond_to do |format|
      format.html { redirect_to contractor_representatives_url }
      format.json { head :no_content }
    end
  end


  private

  def contractor_representative_params
    params[:contractor_representative][:id] = SecureRandom.uuid
    params.require(:contractor_representative).permit(:contractor_id, :end_date, :id, :is_close, :person_id, :position_id, :start_date, :id)
  end


end

