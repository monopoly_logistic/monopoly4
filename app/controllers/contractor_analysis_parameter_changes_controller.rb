class ContractorAnalysisParameterChangesController < ApplicationController
  before_action :set_contractor_analysis_parameter_change, only: [:show, :edit, :update, :destroy]


  def index
    @contractor_analysis_parameter_changes = ContractorAnalysisParameterChange.all
  end

  def show
  end

  def new
    @contractor_analysis_parameter_change = ContractorAnalysisParameterChange.new
  end

  def edit
  end

  def create
    @contractor_analysis_parameter_change = ContractorAnalysisParameterChange.new(contractor_analysis_parameter_change_params)

    if @contractor_analysis_parameter_change.save
      redirect_to contractor_analysis_parameter_changes_url, notice: 'добавлен'
    else
      render :new
    end
  end

  def update
    if @contractor_analysis_parameter_change.update(contractor_analysis_parameter_change_params)
      redirect_to contractor_analysis_parameter_changes_url, notice: 'сохранен'
    else
      render :edit
    end
  end

  def destroy
    @contractor_analysis_parameter_change.destroy
    redirect_to contractor_analysis_parameter_changes_url, notice: 'удален'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contractor_analysis_parameter_change
      @contractor_analysis_parameter_change = ContractorAnalysisParameterChange.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def contractor_analysis_parameter_change_params
      params.require(:contractor_analysis_parameter_change).permit(:contractor_analysis_parameter_id, :parameter_value, :start_date, :end_date, :is_close)
    end
end
