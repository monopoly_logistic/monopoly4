# -*- encoding : utf-8 -*-
class TaskTemplatesController < ApplicationController
  before_action :set_task_template, only: [:show, :edit, :update, :destroy]


  def index
    @task_templates = TaskTemplate.all
  end

  def show
  end

  def new
    @task_template = TaskTemplate.new
  end

  def edit
  end

  def create
    @task_template = TaskTemplate.new(task_template_params)

    if @task_template.save
      redirect_to @task_template, notice: 'Task template добавлен'
    else
      render :new
    end
  end

  def update
    if @task_template.update(task_template_params)
      redirect_to @task_template, notice: 'Task template сохранен'
    else
      render :edit
    end
  end

  def destroy
    @task_template.destroy
    redirect_to task_templates_url, notice: 'Task template удален'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task_template
      @task_template = TaskTemplate.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def task_template_params
      params.require(:task_template).permit(:task_type_id, :name, :descr, :objective, :normative_runtime, :prioryty)
    end
end
