require 'sse'
require 'timeout'

class StreamController < ApplicationController
  
  include ActionController::Live
  
  def uploading   
    response.headers['Content-Type'] = 'text/event-stream'
    sse = ServerSide::SSE.new(response.stream)     
   
    begin
      loop do
        @data = Unavailable.uploading2
        sse.write ({ :message => @data.to_json })
#        sleep 5  
      end     
#      sse.write "event: finished\n"
#      sse.write "data: Buy!\n\n"
    rescue IOError 
      
    ensure  
      sse.close
    end  
    render nothing: true
  end
  
  def monitoring_connections   
    response.headers['Content-Type'] = 'text/event-stream'
      sse = ServerSide::SSE.new(response.stream)   
      loop do
      sleep 10  
  #      @data = Unavailable.connection_request2(nil, nil)          
  #      sse.write ({ :message => @data.to_json}) unless @data.blank?
        sse.write ({ :message => 'ok'})
      end     
#    rescue IOError 
      
    ensure  
    sse.close
    render nothing: true
  end

  
end
