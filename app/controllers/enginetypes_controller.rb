class EnginetypesController < ApplicationController

  def index
    @enginetypes = Enginetype.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end


  def new
    @enginetype = Enginetype.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end


  def edit
    @enginetype = Enginetype.find(params[:id])
  end


  def create
    @enginetype = Enginetype.new(enginetype_params)

    respond_to do |format|
      if @enginetype.save
        format.html { redirect_to enginetypes_path, notice: 'Тип двигателя добавлен' }
      else
        format.html { render action: "new" }
      end
    end
  end


  def update
    @enginetype = Enginetype.find(params[:id])

    respond_to do |format|
      if @enginetype.update_attributes(enginetype_params)
        format.html { redirect_to enginetypes_path, notice: 'Тип двигателя сохранен' }
      else
        format.html { render action: "edit" }
      end
    end
  end


  def destroy
    @enginetype = Enginetype.find(params[:id])
    @enginetype.close

    respond_to do |format|
      format.html { redirect_to enginetypes_url }
    end
  end


 private

 def enginetype_params
   params.require(:enginetype).permit(:name, :is_close)
 end

end
