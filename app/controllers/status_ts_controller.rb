class StatusTsController < ApplicationController
  
  before_action :set_vars
  
  def index
    clear_vars unless params[:clear].blank?
    sort_and_filter
    @ts_count = @status_ts.size
    @status_ts = @status_ts.paginate(page: @page || 1, per_page: @per_page || 20)
    
    respond_to do |format|
      format.html 
      format.js 
    end
  end
  
  def show_filter_and_search
    @filter_col_names = @col_names[0..-2]
    @title = 'Фильтрация и поиск'
    @render = 'filter_and_search'
    render :show_popup
  end
  
  def select_filter
    @ind = @col_names.index(params[:col_name])
    @key = @keys[@ind]
    @for_filter = @status_ts.map { |s| s.send(@key).class.to_s == 'Hash' ? s.send(@key)[:num] : s.send(@key).strip unless s.send(@key).blank? }.compact.uniq.sort
    @for_search = @for_filter
    @for_filter.unshift('Не пусто', 'Пусто')
  end
  
  
  def repair_request
    @repair_request = Unavailable.repair_request(params[:id], params[:chain].blank? ? 0 : 1)
    @col_names =  @repair_request[0].keys
    @repair_request.map!{|u| u.values}
    @title = params[:ts]
    @render = 'repair_request'
    render :show_popup
  end

  def save_xls
    sort_and_filter
    @status_ts_xls = []
    @status_ts.each do |st|
      @tmp = []
      @keys.each do |key|
        @tmp << st.send(key)
      end
      @status_ts_xls += [@tmp]
    end
    respond_to do |format|
      format.xls {
        status_ts = Spreadsheet::Workbook.new
        list = status_ts.create_worksheet name: 'status_ts'
        list.row(0).concat @col_names
        @status_ts_xls.each_with_index { |status, i| list.row(i+1).push *status  }
        header_format = Spreadsheet::Format.new color: :green, weight: :bold
        list.row(0).default_format = header_format
        blob = StringIO.new("")
        status_ts.write blob
        send_data blob.string, type: :xls, filename: "Cоcтояние_ТС_#{Time.now.to_i.to_s}.xls"
      }
      format.html { render text: @status_ts_xls}
    end
  end
  
  
  
  private
  
  def show_popup; end
  
  
  def is_date(str)
    begin
      Date.parse(str).to_time.to_i.to_s
    rescue # ArgumentError
      str.try(:to_s)
    end
  end
  
  def sort_and_filter

    user_session[:search_by_column] = nil unless request.xhr?    
    if @search_by_column
      @status_ts = @status_ts.find_all{ |s| s.send(@keys[@sort_ind.to_i]).index(@search_by_column[:search].strip) }.compact 
    end  
    
    if @search and @col_ind
      @search_arr = @search[0..-2].split(',').compact
      @tmp = []
      @key = @keys[@col_ind.to_i]
      @search_arr.each do |search|
        @tmp += @status_ts.find_all{ |s| s.send(@key) == search.strip }.compact
      end
      @status_ts = @tmp.uniq
    elsif @filter and @col_ind
      @ind = @col_ind.to_i
      if @filter == 'Не пусто'
        @status_ts = @status_ts.find_all{ |s| !s.send(@keys[@ind]).blank? }.compact
      elsif @filter == 'Пусто'
        @status_ts = @status_ts.find_all{ |s| s.send(@keys[@ind]).blank? }.compact
      else
        @status_ts = @status_ts.find_all{ |s| s.send(@keys[@ind]).class.to_s == 'Hash' ? s.send(@keys[@ind])[:num] == @filter : s.send(@keys[@ind]).to_s.strip == @filter.to_s.strip }.compact
      end
    end
    
    @status_ts.sort! do |a,b| 
      @dir == 'up' ?  (is_date(a.send(@keys[@sort_ind.to_i])) <=> is_date(b.send(@keys[@sort_ind.to_i]))) : (is_date(b.send(@keys[@sort_ind.to_i])) <=> is_date(a.send(@keys[@sort_ind.to_i])))
    end if @sort_ind and @dir
    @all_st_ts = @status_ts.size
  end
  
  
  def set_vars
    @status_ts = StatusTsData.get_status_ts
    @col_names = StatusTsData::COLUMN_NAMES
    @keys = @status_ts.first.attr_keys
    user_session[:st_ts_per_page] = params[:per_page] unless params[:per_page].blank?
    user_session[:st_ts_page] = params[:page] unless params[:page].blank?
    user_session[:st_ts_sort_dir] = params[:dir] unless params[:dir].blank?
    user_session[:st_ts_sort_ind] = params[:sort_ind] unless params[:sort_ind].blank?
    user_session[:st_ts_col_ind] = params[:col_ind] unless params[:col_ind].blank?
    user_session[:st_ts_filter] = params[:filter] unless params[:filter].blank?
    user_session[:st_ts_search] = params[:search] unless params[:search].blank?
    user_session[:search_by_column] = {search: params[:search_by_column], ind: params[:col_ind] } unless params[:search_by_column].blank?
    
    if request.delete? && params[:clear]
      case params[:clear]
      when 'search'
        user_session[:search_by_column] = nil
      when 'filter'
        user_session[:st_ts_filter] = nil
      end
    end
    
    @page = user_session[:st_ts_page]
    @per_page = user_session[:st_ts_per_page]
    @dir = user_session[:st_ts_sort_dir]
    @sort_ind = user_session[:st_ts_sort_ind]
    @col_ind = user_session[:st_ts_col_ind]
    @filter = user_session[:st_ts_filter]
    @search = user_session[:st_ts_search]
    @search_by_column = user_session[:search_by_column]
    
  
    
  end
  
  def clear_vars
    user_session[:st_ts_per_page] = nil
    user_session[:st_ts_page] = nil
    user_session[:st_ts_sort_dir] = nil
    user_session[:st_ts_sort_ind] = nil
    user_session[:st_ts_col_ind] = nil
    user_session[:st_ts_filter] = nil
    user_session[:st_ts_search] = nil
    user_session[:search_by_column] = nil
    set_vars
  end

end  
  
class StatusTsData < Object  
  COLUMN_NAMES = ['Тягач', 'Прицеп', 'Водитель', 'Ответственный логист', 'Предыдущая заявка', 
                    'Клиент текущей заявки', 'Текущая заявка откуда', 'Текущая заявка куда', 
                    'Время высвобождения по текущей заявке', 'Следующая заявка', 'Недоступность', 'Заявки на ремонт']
  attr_accessor :truck_num, :trailer_num, :driver_name, :logist, :prev_entry, 
                :current_contractor, :current_entry_from, :current_entry_to,
                :release_date_current_entry, :next_entry, :unavailable, :repair_order, :unavailable_color
  
  def self.get_status_ts
    data = Unavailable.getdata
    @out = []
    data.each do |item|
      unv = item['Недоступность']
      rep_truck = item['Заявки на ремонт']
      rep_trailer = item['#Заявки на ремонт']
      rep_man = item['#Заявки на водителя']
      
      status_ts = StatusTsData.new

      status_ts.truck_num = item['Тягач'] || ''
      status_ts.trailer_num = item['Прицеп'] || ''
      status_ts.driver_name = item['Водитель'] || ''
      status_ts.logist = item['Ответственный логист'] || ''
      status_ts.prev_entry = (item['Предыдущая заявка'] || '').delete("'")
      status_ts.current_contractor = (item['Клиент текущей заявки'] || '').delete("'")
      status_ts.current_entry_from = item['Текущая заявка Откуда'] || ''
      status_ts.current_entry_to = item['Текущая заявка Куда'] || ''
      status_ts.release_date_current_entry = item['Время высвобождения по текущей заявке'] || ''
      status_ts.next_entry = (item['Следующая заявка'] || '').delete("'")
      status_ts.unavailable = unv && unv.index('#') ? {num: unv[0..unv.index('#')-1], link: unv[unv.index('#')+1..-1]} : '' 
            
      status_ts.repair_order = []
      status_ts.repair_order << { num: rep_truck[0..rep_truck.index('#')-1], link: rep_truck[rep_truck.index('#')+1..-1], img: 'tg_1.png' } if rep_truck && rep_truck.index('#')
      status_ts.repair_order << { num: rep_trailer[0..rep_trailer.index('#')-1], link: rep_trailer[rep_trailer.index('#')+1..-1], img: 'pr_1.png' } if rep_trailer && rep_trailer.index('#')
      status_ts.repair_order << { num: rep_man[0..rep_man.index('#')-1], link: rep_man[rep_man.index('#')+1..-1], img: 'man.png' } if rep_man && rep_man.index('#')
      
      status_ts.unavailable_color = item['#Недоступность']
      @out << status_ts
    end  
    @out
  end
  
  def attr_values
    self.instance_values.values
  end
  
  def attr_keys
    self.instance_values.keys
  end
  
end     
  
