class CouplersController < ApplicationController


  def index
    
    @trucks_count = Transport.trucks.count
    @trailers_count = Transport.trailers.count
    
    @free_trucks_count = Transport.free_trucks.count
    @used_trucks_count = Transport.used_trucks.count
    @free_trailers_count = Transport.free_trailers.count
    @used_trailers_count = Transport.used_trailers.count
    
    case params[:vis]
    when 'chain'
      @couplers = Coupler.chained.order('start_date desc')
    when 'unchain'
      @couplers = Coupler.unchained.order('start_date desc')
    else
      @couplers = Coupler.order('start_date desc')
    end
    #@couplers =  @couplers.paginate(page: params[:page], per_page: 20)
    
    respond_to do |format|
      format.html # index.html.erb
    end
  end


  def show
    @coupler = Coupler.find(params[:id])

    respond_to do |format|
      format.js { }
    end
  end


  def new
    @coupler = Coupler.new
    @free_trucks = Transport.free_trucks
    @free_trailers = Transport.free_trailers

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @coupler }
    end
  end


  def edit
    @coupler = Coupler.find(params[:id])
    @free_trucks = Transport.free_trucks
    @free_trailers = Transport.free_trailers
  end


  def create
    @coupler = Coupler.new(coupler_params)

    respond_to do |format|
      if @coupler.save
        format.html { redirect_to couplers_url, notice: 'Сцепка добавлена' }
        format.json { render json: @coupler, status: :created, location: @coupler }
      else
        format.html { render action: "new" }
        format.json { render json: @coupler.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @coupler = Coupler.find(params[:id])

    respond_to do |format|
      if @coupler.update_attributes(coupler_params)
        format.html { redirect_to couplers_url, notice: 'Сцепка сохранена' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @coupler.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @coupler = Coupler.find(params[:id])
    @coupler.close
    #@coupler.destroy

    respond_to do |format|
      format.html { redirect_to couplers_url }
    end
  end

  def unchain
    @coupler = Coupler.find(params[:id])
    @coupler.close_coupler

    respond_to do |format|
      format.html { redirect_to couplers_url }
    end
  end
  
  
 private

 def coupler_params
   params.require(:coupler).permit(:end_date, :id, :start_date, :trailer_id, :truck_id, :is_close)
 end


end

