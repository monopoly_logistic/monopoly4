class TransportcategoriesController < ApplicationController


  def index

    @transportcategories = Transportcategory.all
    respond_to do |format|
      format.html # index.html.erb
    end
  end


  def new
    @transportcategory = Transportcategory.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end


  def edit
    @transportcategory = Transportcategory.find(params[:id])
  end

  def create
    @transportcategory = Transportcategory.new(transportcategory_params)

    respond_to do |format|
      if @transportcategory.save
        format.html { redirect_to transportcategories_path, notice: 'Категория ТС добавлена' }
      else
        format.html { render action: "new" }
      end
    end
  end


  def update
    @transportcategory = Transportcategory.find(params[:id])

    respond_to do |format|
      if @transportcategory.update_attributes(transportcategory_params)
        format.html { redirect_to transportcategories_path, notice: 'Категория ТС сохранена' }
      else
        format.html { render action: "edit" }
      end
    end
  end


  def destroy
    @transportcategory = Transportcategory.find(params[:id])
    @transportcategory.close

    respond_to do |format|
      format.html { redirect_to transportcategories_url }
    end
  end


 private

 def transportcategory_params
   params.require(:transportcategory).permit(:name, :descr, :is_close)
 end


end
