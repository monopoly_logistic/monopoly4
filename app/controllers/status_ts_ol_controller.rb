class StatusTsOlController < ApplicationController
  
  before_action :set_vars
  
  def index
    clear_vars unless params[:clear].blank?
    sort_and_filter
    @ts_ol_count = @status_ts_ol.size
    @status_ts_ol = @status_ts_ol.paginate(page: @page || 1, per_page: @per_page || 20)
    
    respond_to do |format|
      format.html 
      format.js 
    end
  end
  
  def show_filter_and_search
    @filter_col_names = @col_names
    @title = 'Фильтрация и поиск'
    @render = 'filter_and_search'
    render :show_popup
  end
  
  def select_filter
    @ind = @col_names.index(params[:col_name])
    @key = @keys[@ind]
    @for_filter = 
      @status_ts_ol.map do |s| 
        if s.send(@key).class.to_s == 'Hash' 
          s.send(@key)[:num] 
        elsif s.send(@key).class.to_s == 'Array' 
        else
          s.send(@key).strip unless s.send(@key).blank? 
        end
      end.compact.uniq.sort
    @for_search = @for_filter
    @for_filter.unshift('Не пусто', 'Пусто')
  end
  
  
  def repair_request
    @repair_request = Unavailable.repair_request(params[:id], params[:chain].blank? ? 0 : 1)
    @col_names =  @repair_request[0].keys
    @repair_request.map!{|u| u.values}
    @title = params[:ts]
    @render = 'repair_request'
    render :show_popup
  end

  def new_comment_ol
    @truck_id = params[:truck_id]
    @last_comment = TruckComment.last_comment(@truck_id).first.try(:comment) unless @truck_id.blank?
    @comment = TruckComment.new
    @sp_ol = params[:sp_ol]
    @title = "Комментарии для #{params[:truck]}"
    @render = 'comment_ol_form'
    render :show_popup
  end
  
  def create_comment_ol
   @truck_commment = TruckComment.create({user_id: current_user.id, truck_id: params[:truck_id] , comment: params[:comment]})
   @sp_ol = params[:sp_ol]
  end

  def get_document1
    @document = Unavailable.get_document1_ol(params[:id])
    unless @document.blank?
      @col_names =  @document[0].keys
      @document.map!{|u| u.values}
    end
    @title = "Документы для #{params[:ts]}"
    @render = 'get_document'
    render :show_popup
  end

  def get_document2
    @document = Unavailable.get_document2_ol(params[:id])
    unless @document.blank?
      @col_names =  @document[0].keys
      @document.map!{|u| u.values}
    end
    @title = "Документы для #{params[:ts]}"
    @render = 'get_document'
    render :show_popup
  end

  def get_document3
    @document = Unavailable.get_document3_ol(params[:id])
    unless @document.blank?
      @col_names =  @document[0].keys
      @document.map!{|u| u.values}
    end
    @title = "Документы для #{params[:ts]}"
    @render = 'get_document'
    render :show_popup
  end
  
  
  def save_xls
    sort_and_filter
    @status_ts_ol_xls = []
    @status_ts_ol.each do |st|
      @tmp = []
      @keys.each do |key|
        @tmp << st.send(key)
      end
      @status_ts_ol_xls += [@tmp]
    end
    respond_to do |format|
      format.xls {
        status_ts_ol = Spreadsheet::Workbook.new
        list = status_ts_ol.create_worksheet name: 'status_ts_ol'
        list.row(0).concat @col_names
        @status_ts_ol_xls.each_with_index { |status, i| list.row(i+1).push *status[0..-2] }
        header_format = Spreadsheet::Format.new color: :green, weight: :bold
        list.row(0).default_format = header_format
        blob = StringIO.new("")
        status_ts_ol.write blob
        send_data blob.string, type: :xls, filename: "Cоcтояние_ТС_#{Time.now.to_i.to_s}.xls"
      }
      format.html { render text: @status_ts_ol_xls}
    end
  end
  
  
  
  private
  
  def show_popup; end
  
  
  def is_date(str)
    begin
      Date.parse(str).to_time.to_i.to_s
    rescue # ArgumentError
      str.try(:to_s)
    end
  end
  
  def sort_and_filter

    user_session[:search_by_column_ol] = nil unless request.xhr?    
    if @search_by_column_ol
      @status_ts_ol = @status_ts_ol.find_all{ |s| s.send(@keys[@sort_ind.to_i]).index(@search_by_column_ol[:search].strip) }.compact 
    end  
    
    if @search and @col_ind
      @search_arr = @search[0..-2].split(',').compact
      @tmp = []
      @key = @keys[@col_ind.to_i]
      @search_arr.each do |search|
        @tmp += @status_ts_ol.find_all{ |s| s.send(@key) == search.strip }.compact
      end
      @status_ts_ol = @tmp.uniq
    elsif @filter and @col_ind
      @ind = @col_ind.to_i
      if @filter == 'Не пусто'
        @status_ts_ol = @status_ts_ol.find_all{ |s| !s.send(@keys[@ind]).blank? }.compact
      elsif @filter == 'Пусто'
        @status_ts_ol = @status_ts_ol.find_all{ |s| s.send(@keys[@ind]).blank? }.compact
      else
        @status_ts_ol = @status_ts_ol.find_all{ |s| s.send(@keys[@ind]).class.to_s == 'Hash' ? s.send(@keys[@ind])[:num] == @filter : s.send(@keys[@ind]).to_s.strip == @filter.to_s.strip }.compact
      end
    end
    
    @status_ts_ol.sort! do |a,b| 
      @dir == 'up' ?  (is_date(a.send(@keys[@sort_ind.to_i])) <=> is_date(b.send(@keys[@sort_ind.to_i]))) : (is_date(b.send(@keys[@sort_ind.to_i])) <=> is_date(a.send(@keys[@sort_ind.to_i])))
    end if @sort_ind and @dir
    
    @all_st_ts = @status_ts_ol.size
  end
  
  
  def set_vars
    @status_ts_ol = StatusTsOlData.get_status_ts_ol
    @col_names = StatusTsOlData::COLUMN_NAMES
    @keys = @status_ts_ol.first.attr_keys
    
    user_session[:st_ts_ol_per_page] = params[:per_page] unless params[:per_page].blank?
    user_session[:st_ts_ol_page] = params[:page] unless params[:page].blank?
    user_session[:st_ts_ol_sort_dir] = params[:dir] unless params[:dir].blank?
    user_session[:st_ts_ol_sort_ind] = params[:sort_ind] unless params[:sort_ind].blank?
    user_session[:st_ts_ol_col_ind] = params[:col_ind] unless params[:col_ind].blank?
    user_session[:st_ts_ol_filter] = params[:filter] unless params[:filter].blank?
    user_session[:st_ts_ol_search] = params[:search] unless params[:search].blank?
    user_session[:search_by_column_ol] = {search: params[:search_by_column_ol], ind: params[:col_ind] } unless params[:search_by_column_ol].blank?
    
    if request.delete? && params[:clear]
      case params[:clear]
      when 'search'
        user_session[:search_by_column_ol] = nil
      when 'filter'
        user_session[:st_ts_ol_filter] = nil
      end
    end
    @per_page = user_session[:st_ts_ol_per_page]
    @page = user_session[:st_ts_ol_page]
    @dir = user_session[:st_ts_ol_sort_dir]
    @sort_ind = user_session[:st_ts_ol_sort_ind]
    @col_ind = user_session[:st_ts_ol_col_ind]
    @filter = user_session[:st_ts_ol_filter]
    @search = user_session[:st_ts_ol_search]
    @search_by_column_ol = user_session[:search_by_column_ol]
    
  end
  
  def clear_vars
    user_session[:st_ts_ol_per_page] = nil
    user_session[:st_ts_ol_page] = nil
    user_session[:st_ts_ol_sort_dir] = nil
    user_session[:st_ts_ol_sort_ind] = nil
    user_session[:st_ts_ol_col_ind] = nil
    user_session[:st_ts_ol_filter] = nil
    user_session[:st_ts_ol_search] = nil
    user_session[:search_by_column_ol] = nil
    set_vars
  end

end  
  
class StatusTsOlData < Object  

  COLUMN_NAMES = ['Тягач', 'Прицеп', 'Водитель', 'Комментарий логиста', 'Предыдущая заявка', 
                    'Заявки на ремонт', 'Документы', 'Недоступность', 'Регион высвобождения',
                    'Город загрузки текущей заявки', 'План загрузки текущей заявки', 'Факт загрузки текущей заявки', 
                    'Город выгрузки текущей заявки', 'План выгрузки текущей заявки', 'Последний комментарий',
                    'Следующая заявка: загрузка', 'Следующая заявка: выгрузка', 'Ответственный логист']
                  

  attr_accessor :truck_key, :truck_num, :trailer_num, :driver_name, :logist_comment, :prev_entry, :repair_order, :docs, :unavailable,
                :location, :current_city_loading, :current_plan_loading, :current_fact_loading, 
                :current_city_unloading, :current_plan_unloading, :last_comment, :next_loading, :next_unloading,
                :logist, :unavailable_color

  def self.get_status_ts_ol
    data = Unavailable.getdata_ol
    @out = []
    data.each do |item|
      unv = item['Недоступность']
      rep_truck = item['Заявки на ремонт']
      rep_trailer = item['#Заявки на ремонт']
      rep_man = item['#Заявки на водителя']
      pr_doc1 = item['#Проблемы с документами1']
      pr_doc2 = item['#Проблемы с документами2']
      pr_doc3 = item['#Проблемы с документами3']
      doc1 = item['Документы'][item['Документы'].index('#')+1..-1] if item['Документы'] && item['Документы'].index('#')
      doc2 = item['#Документы2'][item['#Документы2'].index('#')+1..-1] if item['#Документы2'] && item['#Документы2'].index('#')
      doc3 = item['#Документы3'][item['#Документы3'].index('#')+1..-1] if item['#Документы3'] && item['#Документы3'].index('#')

      status_ts_ol = StatusTsOlData.new

      status_ts_ol.truck_num = item['Тягач'] || ''
      status_ts_ol.trailer_num = item['Прицеп'] || ''
      status_ts_ol.driver_name = item['Водитель'] || ''
      status_ts_ol.logist_comment = item['#TruckComment']
      status_ts_ol.prev_entry = (item['Предыдущая заявка'] || '').delete("'")
      
      status_ts_ol.repair_order = []
      status_ts_ol.repair_order << { num: rep_truck[0..rep_truck.index('#')-1], link: rep_truck[rep_truck.index('#')+1..-1], img: 'tg_1.png' } if rep_truck && rep_truck.index('#')
      status_ts_ol.repair_order << { num: rep_trailer[0..rep_trailer.index('#')-1], link: rep_trailer[rep_trailer.index('#')+1..-1], img: 'pr_1.png' } if rep_trailer && rep_trailer.index('#')
      status_ts_ol.repair_order << { num: rep_man[0..rep_man.index('#')-1], link: rep_man[rep_man.index('#')+1..-1], img: 'man.png' } if rep_man && rep_man.index('#')
      
      status_ts_ol.docs = []
      if doc1
        img1 =
          case pr_doc1
            when 1; 'green_med1.png'
            when 2; 'yellow_med1.png'
            when 3; 'red_med1.png'
          end    
        status_ts_ol.docs << {link: doc1, img: img1, num: 1}
      end
      if doc2
        img2 =
          case pr_doc2
            when 1; 'green_ol.png'
            when 2; 'yellow_ol.png'
            when 3; 'red_ol.png'
          end    
        status_ts_ol.docs << {link: doc2, img: img2, num: 2}
      end
      if doc3
        img3 =
          case pr_doc3
            when 1; 'green_pass.png'
            when 2; 'yellow_pass.png'
            when 3; 'red_pass.png'
          end    
        status_ts_ol.docs << {link: doc3, img: img3, num: 3}
      end
      
      status_ts_ol.unavailable = unv && unv.index('#') ? {num: unv[0..unv.index('#')-1], link: unv[unv.index('#')+1..-1]} : '' 

      status_ts_ol.location = item['Местоположение'] || ''
      status_ts_ol.current_city_loading = item['Город загрузки текущей заявки'] || ''
      status_ts_ol.current_plan_loading = item['План загрузки текущей заявки'] || ''
      status_ts_ol.current_fact_loading = item['Факт загрузки текущей заявки'] || ''
      status_ts_ol.current_city_unloading = item['Город выгрузки текущей заявки'] || ''
      status_ts_ol.current_plan_unloading = item['План выгрузки текущей заявки'] || ''
      status_ts_ol.last_comment = item['Последний комментарий'] || ''
      status_ts_ol.next_loading = item['Следующая заявка: загрузка'] || ''
      status_ts_ol.next_unloading = item['Следующая заявка: выгрузка'] || ''       
      status_ts_ol.logist = item['Ответственный логист'] || ''       
      
      status_ts_ol.unavailable_color = item['#Недоступность']
      status_ts_ol.truck_key = item['#TruckKey']
      @out << status_ts_ol
    end  
    @out
  end
  
  def attr_values
    self.instance_values.values
  end
  
  def attr_keys
    self.instance_values.keys
  end
  
end     
  