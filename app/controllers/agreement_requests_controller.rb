class AgreementRequestsController < ApplicationController
  before_action :set_agreement_request, only: [:show, :edit, :update, :destroy]


  def index
    user_session[:agr_from] = params[:from] unless params[:from].blank?
    user_session[:agr_to] = params[:to] unless params[:to].blank?
    user_session[:agr_type] = params[:id] unless params[:id].blank?
    
    @from = user_session[:agr_from] || Date.today
    @to = user_session[:agr_to] || Date.tomorrow
    @agreements = Agreement.visible.select(:id, :name)
    @agreement = user_session[:agr_type].blank? ? @agreements.first : Agreement.find(user_session[:agr_type])
    @agreement_requests = @agreement.agreement_requests.where(created_at: @from.to_date..@to.to_date )
        .includes(:question_answers_yes, :question_answers_no, agreement: :agreement_accesses)
  end

  def show
    
  end

  def new
    @agreement_request = AgreementRequest.new
  end

  def edit
  end

  def create
    @agreement_request = AgreementRequest.new(agreement_request_params)

    if @agreement_request.save
      redirect_to @agreement_request, notice: 'agreement request добавлен'
    else
      render :new
    end
  end

  def update
    if @agreement_request.update(agreement_request_params)
      redirect_to @agreement_request, notice: 'agreement request сохранен'
    else
      render :edit
    end
  end

  def destroy
    @agreement_request.destroy
    redirect_to agreement_requests_url, notice: 'agreement request удален'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_agreement_request
    @agreement_request = AgreementRequest.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def agreement_request_params
    params.require(:agreement_request).permit(:agreement_id, :initiator_id, :req_text, :start_date, :status, :status_updated_at, :business_object_id, :uses_number, :time_use)
  end
end
