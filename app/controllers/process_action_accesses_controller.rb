class ProcessActionAccessesController < ApplicationController
  before_action :set_process_action_access, only: [:show, :edit, :update, :destroy]


  def index
    @process_action_accesses = ProcessActionAccess.all
  end

  def show
  end

  def new
    @process_action_access = ProcessActionAccess.new
  end

  def edit
  end

  def create
    @process_action_access = ProcessActionAccess.new(process_action_access_params)

    if @process_action_access.save
      redirect_to @process_action_access, notice: 'Доступ к действиям с шаблонами процессов добавлен'
    else
      render :new
    end
  end

  def update
    if @process_action_access.update(process_action_access_params)
      redirect_to @process_action_access, notice: 'Доступ к действиям с шаблонами процессов сохранен'
    else
      render :edit
    end
  end

  def destroy
    @process_action_access.destroy
    redirect_to process_action_accesses_url, notice: 'Доступ к действиям с шаблонами процессов удален'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_process_action_access
    @process_action_access = ProcessActionAccess.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def process_action_access_params
    params.require(:process_action_access).permit(:process_action_id, :process_template_id, :employee_id, :staff_unit_id, :start_date, :end_date, :is_close)
  end
end
