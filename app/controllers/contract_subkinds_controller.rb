# -*- encoding : utf-8 -*-

class ContractSubkindsController < ApplicationController


  def index
 #   @contract_subkinds = ContractSubkind.all
    @contract_subkinds_count = ContractSubkind.count
    @contract_subkinds = ContractSubkind.try(params[:show_del].blank? ? :visible : :invisible)
        .joins(:contract_kind)
        .order("contract_kinds.name")
        #.paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contract_subkinds }
    end
  end


  def show
    @contract_subkind = ContractSubkind.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @contract_subkind }
    end
  end


  def new
    @contract_subkind = ContractSubkind.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contract_subkind }
    end
  end


  def edit
    @contract_subkind = ContractSubkind.find(params[:id])
  end


  def create
    @contract_subkind = ContractSubkind.new(contract_subkind_params)

    respond_to do |format|
      if @contract_subkind.save
        format.html { redirect_to contract_subkinds_url, notice: 'Подвид договора добавлен.' }
        format.json { render json: @contract_subkind, status: :created, location: @contract_subkind }
      else
        format.html { render action: "new" }
        format.json { render json: @contract_subkind.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @contract_subkind = ContractSubkind.find(params[:id])

    respond_to do |format|
      if @contract_subkind.update_attributes(contract_subkind_params)
        format.html { redirect_to contract_subkinds_url, notice: 'Подвид договорa сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contract_subkind.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @contract_subkind = ContractSubkind.find(params[:id])
    @contract_subkind.close
    #@contract_subkind.destroy

    respond_to do |format|
      format.html { redirect_to contract_subkinds_url }
      format.json { head :no_content }
    end
  end


 private

 def contract_subkind_params
   params.require(:contract_subkind).permit(:contract_kind_id, :descr, :id, :is_close, :name)
 end


end

