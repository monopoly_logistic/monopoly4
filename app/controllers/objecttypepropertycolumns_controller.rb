# -*- encoding : utf-8 -*-

class ObjecttypepropertycolumnsController < ApplicationController


  def index
    @objecttypepropertycolumns = Objecttypepropertycolumn.joins(:objecttypeproperty).order('objecttypeproperties.name')#.paginate(page: params[:page], per_page: 30)


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @objecttypepropertycolumns }
    end
  end


  def show
    @objecttypepropertycolumn = Objecttypepropertycolumn.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @objecttypepropertycolumn }
    end
  end


  def new
    @objecttypepropertycolumn = Objecttypepropertycolumn.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @objecttypepropertycolumn }
    end
  end


  def edit
    @objecttypepropertycolumn = Objecttypepropertycolumn.find(params[:id])
  end


  def create
    @objecttypepropertycolumn = Objecttypepropertycolumn.new(objecttypepropertycolumn_params)

    respond_to do |format|
      if @objecttypepropertycolumn.save
        format.html { redirect_to objecttypepropertycolumns_url, notice: 'Objecttypepropertycolumn добавлен.' }
        format.json { render json: @objecttypepropertycolumn, status: :created, location: @objecttypepropertycolumn }
      else
        format.html { render action: "new" }
        format.json { render json: @objecttypepropertycolumn.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @objecttypepropertycolumn = Objecttypepropertycolumn.find(params[:id])

    respond_to do |format|
      if @objecttypepropertycolumn.update_attributes(objecttypepropertycolumn_params)
        format.html { redirect_to objecttypepropertycolumns_url, notice: 'Objecttypepropertycolumn сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @objecttypepropertycolumn.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @objecttypepropertycolumn = Objecttypepropertycolumn.find(params[:id])
    @objecttypepropertycolumn.close

    respond_to do |format|
      format.html { redirect_to objecttypepropertycolumns_url }
      format.json { head :no_content }
    end
  end


 private

 def objecttypepropertycolumn_params
   params.require(:objecttypepropertycolumn).permit(:column_id, :end, :id, :is_close, :objecttypeproperty_id, :start)
 end

end

