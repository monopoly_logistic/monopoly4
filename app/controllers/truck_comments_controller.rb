class TruckCommentsController < ApplicationController
  before_action :set_truck_comment, only: [:show, :edit, :update, :destroy]


  def index
    @truck_comments = TruckComment.all
  end

  def show
  end

  def new
    @truck_comment = TruckComment.new
  end

  def edit
  end

  def create
    @truck_comment = TruckComment.new(truck_comment_params)

    if @truck_comment.save
      redirect_to @truck_comment, notice: 'Truck comment добавлен'
    else
      render :new
    end
  end

  def update
    if @truck_comment.update(truck_comment_params)
      redirect_to @truck_comment, notice: 'Truck comment сохранен'
    else
      render :edit
    end
  end

  def destroy
    @truck_comment.destroy
    redirect_to truck_comments_url, notice: 'Truck comment удален'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_truck_comment
      @truck_comment = TruckComment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def truck_comment_params
      params.require(:truck_comment).permit(:truck_id, :comment, :user_id, :is_close)
    end
end
