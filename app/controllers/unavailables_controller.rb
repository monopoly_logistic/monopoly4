class UnavailablesController < ApplicationController
  $INTERVAL = 10000
  $INTERVAL_OL = 10000

  #before_action :unv_params


  def index
    redirect_to "/status_ts"

#    @status_ts.sort! do |a,b| 
#      @dir == 'up' ?  (is_date(a.send(@keys[@sort_ind.to_i])) <=> is_date(b.send(@keys[@sort_ind.to_i]))) : (is_date(b.send(@keys[@sort_ind.to_i])) <=> is_date(a.send(@keys[@sort_ind.to_i])))
#    end if @sort_ind and @dir
#
#    user_session[:search_by_column] = nil unless request.xhr?    
#    if @search_by_column
#      @status_ts = @status_ts.find_all{ |s| s.send(@keys[@sort_ind.to_i]).index(@search_by_column[:search].strip) }.compact 
#    end  
#    
#    if @search and @col_ind
#      @search_arr = @search[0..-2].split(',').compact
#      @tmp = []
#      @key = @keys[@col_ind.to_i]
#      @search_arr.each do |search|
#        @tmp += @status_ts.find_all{ |s| s.send(@key) == search.strip }.compact
#      end
#      @status_ts = @tmp
#    elsif @filter and @col_ind
#      @ind = @col_ind.to_i
#      if @filter == 'Не пусто'
#        @status_ts = @status_ts.find_all{ |s| !s.send(@keys[@ind]).blank? }.compact
#      else
#        @status_ts = @status_ts.find_all{ |s| s.send(@keys[@ind]).blank? }.compact
#      end
#    end
#
#    @un_count = @status_ts.size
#    @status_ts = @status_ts.paginate(page: @page ? @page : 1, per_page: 20)

  end

  def ol
    redirect_to "/status_ts_ol"
#    @unavailables = Unavailable.getdata_ol
#    @col_names =  @unavailables[0].keys
#    
#    @unavailables.map!{ |u| u.values.map!{|u2| u2.to_s} }
#    
#    user_session[:search_by_column] = nil unless request.xhr?    
#    @col_names.insert(7, "Комментарий логиста")
#    @unavailables.each{ |u| u.insert(7, TruckComment.last_comment(u[3]).first.nil? ? '' : TruckComment.last_comment(u[3]).first.comment )  } 
#    @unavailables.sort!{|a,b| @dir == 'up' ?  is_date(a[@sort_ind.to_i]) <=> is_date(b[@sort_ind.to_i]) : is_date(b[@sort_ind.to_i]) <=> is_date(a[@sort_ind.to_i]) } if @sort_ind and @dir
#    
#    if @search_by_column
#      @unavailables = @unavailables.find_all{ |u| u[@search_by_column[:ind].to_i].index(@search_by_column[:search].strip) }.compact 
#    end    
#    ##добавление комментариев
#
#
#
#    if @search and @col_ind
#      @search_arr = @search[0..-2].split(',').compact
#      @tmp = []
#      @search_arr.each do |s|
#        @tmp += @unavailables.find_all{ |u| u[@col_ind.to_i] == s.strip.delete("'") }.compact
#      end
#      @unavailables = @tmp
#      @un_count = @unavailables.size
#    elsif @filter and @col_ind
#      @ind = @col_ind.to_i
#      if @filter == 'Не пусто'
#        if @ind == 9
#          @tmp = @unavailables
#          @unavailables = @tmp.find_all{ |u| !( !u[@ind].blank? and u[@ind].index('#') ? u[@ind][0..u[@ind].index('#')-1].strip.delete("'")  : u[@ind].strip.delete("'") ).blank? }.compact
#          @unavailables += @tmp.find_all{ |u| !( !u[10].blank? and u[10].index('#') ? u[10][0..u[10].index('#')-1].strip.delete("'")  : u[10].strip.delete("'") ).blank? }.compact
#          @unavailables.uniq!
#        else
#          @unavailables = @unavailables.find_all{ |u| !( !u[@ind].blank? and u[@ind].index('#') ? u[@ind][0..u[@ind].index('#')-1].strip.delete("'") : u[@ind].strip.delete("'") ).blank? }.compact
#        end
#      else
#        @unavailables = @unavailables.find_all{ |u| ( !u[@ind].blank? and u[@ind].index('#') ? u[@ind][0..u[@ind].index('#')-1].strip.delete("'") : u[@ind].strip.delete("'") ) == params[:filter]}.compact
#      end
#      @un_count = @unavailables.size
#    else
#      @un_count = @unavailables.size
#    end
#
#    @unavailables = @unavailables.paginate(page: @page ? @page : 1, per_page: @filter ? @unavailables.size : 20)
#    respond_to do |format|
#      format.html # index.html.erb
#      format.js { }
#    end

  end


  def new_comment_ol
    @truck_id = params[:truck_id]
    @truck = params[:truck]
    @last_comment = TruckComment.last_comment(@truck_id).first.nil? ? '' : TruckComment.last_comment(@truck_id).first.comment
    @comment = TruckComment.new
    @sp_ol = params[:sp_ol]
    respond_to do |format|
      format.js {}
    end
  end


  def create_comment_ol
   @truck_commment = TruckComment.create({user_id: current_user.id, truck_id: params[:truck_id] , comment: params[:comment]})
   @sp_ol = params[:sp_ol]
  end

  def save_xls

    @invisible_cols = []
    @col_names =  JSON.parse(params[:col_names]).to_a
    @col_names.each_with_index { |cn,ind | @invisible_cols << ind if cn.index('#') }
    @col_names = @col_names.find_all { |u| !u.index('#') }

    @unavailables = JSON.parse(params[:unavailables]).to_a
    @unavailables.each { |unv| @invisible_cols.each { |inv| unv.delete_at(inv) } }
    @unavailables.each { |unv| unv.map! { |u| u && u.to_s.index('#') ? u[0..u.to_s.index('#')-1] : u  } }

    respond_to do |format|
      format.xls {
        unavailables = Spreadsheet::Workbook.new
        list = unavailables.create_worksheet name: 'unavailables'
        list.row(0).concat @col_names
        @unavailables.each_with_index { |unv, i| list.row(i+1).push *unv  }
        header_format = Spreadsheet::Format.new color: :green, weight: :bold
        list.row(0).default_format = header_format
        #output to blob object
        blob = StringIO.new("")
        unavailables.write blob
        #respond with blob object as a file
        send_data blob.string, type: :xls, filename: "Cоcтояние_ТС_#{Time.now.to_i.to_s}.xls"
      }
    end

  end

  def show_filter_and_search
    respond_to do |format|
      format.js   {}
    end
    @params = params[:par].to_hash
    @unavailables = dataset_query
    @col_names =  @unavailables[0].keys.find_all{|u| u unless u.index('#')}
    @unavailables.map!{|u| u.values.map!{|u2| u2.to_s}}
  end


  def select_filter
    respond_to do |format|
      format.js   {}
    end
    @unavailables = dataset_query

    @col_names =  @unavailables[0].keys
    @unavailables.map!{|u| u.values.map!{|u2| u2.to_s}}
    if @referrer['/ol']
      @col_names.insert(7, 'Комментарий логиста')
      @unavailables.each{ |u| u.insert(7, TruckComment.last_comment(u[3]).first.nil? ? '' : TruckComment.last_comment(u[3]).first.comment )  } 
    end
    @ind = @col_names.index(params[:col_name])
    @agreed = params[:par][:agreed]
    @unavailables = @unavailables.find_all { |u| @agreed.blank? ? visible_connection?(u) : !visible_connection?(u) }.compact if request.referrer['connections']
    @for_filter = @unavailables.map { |u| ( u[@ind].index('#') ? (u[@ind].index('#') == 0 ? '' : u[@ind][0..u[@ind].index('#')-1]) : u[@ind] ).strip.delete("'")  if  u[@ind] and !u[@ind].strip.blank? }.compact.uniq.sort if @ind
    @for_filter.unshift('Не пусто')
    @for_search = @unavailables.map { |u| u[@ind].strip.delete("'") if  u[@ind] and !u[@ind].strip.blank? }.compact.uniq.sort if @ind
  end


  def repair_request
    respond_to do |format|
      format.js   {}
    end
    @repair_request = Unavailable.repair_request(params[:id], params[:chain].blank? ? 0 : 1)
    @col_names =  @repair_request[0].keys
    @repair_request.map!{|u| u.values}
    @popup_head = params[:ts]
  end


  def relocate_request
    respond_to do |format|
      format.js   {}
    end
    @relocate_request = Unavailable.relocate_request(params[:id])
    @col_names =  @relocate_request[0].keys
    @relocate_request.map!{|u| u.values}
    @popup_head = params[:ts]
  end

  def get_document
    respond_to do |format|
      format.js   {}
    end
  end

  def get_document1
    @document = Unavailable.get_document1_ol(params[:id])
    unless @document.blank?
      @col_names =  @document[0].keys
      @document.map!{|u| u.values}
    end
    @popup_head = params[:ts]
    render :get_document
  end

  def get_document2
    @document = Unavailable.get_document2_ol(params[:id])
    unless @document.blank?
      @col_names =  @document[0].keys
      @document.map!{|u| u.values}
    end
    @popup_head = params[:ts]
    render :get_document
  end

  def get_document3
    @document = Unavailable.get_document3_ol(params[:id])
    unless @document.blank?
      @col_names =  @document[0].keys
      @document.map!{|u| u.values}
    end
    @popup_head = params[:ts]
    render :get_document
  end


  private

  def unv_params
    @search = params[:search] unless params[:search].blank?
    @col_ind = params[:col_ind] unless params[:col_ind].blank?
    @filter = params[:filter] unless params[:filter].blank?
    @auto = params[:auto] unless params[:auto].blank?
    @sort_ind = params[:sort_ind] unless params[:sort_ind].blank?
    @dir = params[:dir] unless params[:dir].blank?
    @page = params[:page] unless params[:page].blank?
    @agreed = params[:agreed] unless params[:agreed].blank?
    user_session[:search_by_column] = {search: params[:search_by_column], ind: params[:col_ind] } unless params[:search_by_column].blank?
    @search_by_column = user_session[:search_by_column]


    @params = {page: @page || '', col_ind: @col_ind || '', search: @search || '', filter: @filter || '', auto: 1, from: @from || '', to: @to || '', agreed: @agreed || '', sort_ind: @sort_ind || '', dir: @dir || ''}
    
    @status_ts = StatusTs.get_status_ts
    @col_names = StatusTs::COLUMN_NAMES
    @keys = @status_ts.first.attr_keys
    
  end


  def is_date(str)
    begin
      Date.parse(str).to_time.to_i.to_s
    rescue # ArgumentError
      str.try(:to_s)
    end

  end


  def dataset_query
    @referrer = request.referrer
    if @referrer['/ol']
      Unavailable.getdata_ol
    else Unavailable.getdata
    end
  end

  

class StatusTs < Object  
  COLUMN_NAMES = ['Тягач', 'Прицеп', 'Водитель', 'Ответственный логист', 'Предыдущая заявка', 
                    'Клиент текущей заявки', 'Текущая заявка откуда', 'Текущая заявка куда', 
                    'Время высвобождения по текущей заявке', 'Следующая заявка', 'Недоступность', 'Заявки на ремонт']
  attr_accessor :truck_num, :trailer_num, :driver_name, :logist, :prev_entry, 
                :current_contractor, :current_entry_from, :current_entry_to,
                :release_date_current_entry, :next_entry, :unavailable, :repair_order, :unavailable_color
  
  def self.get_status_ts
    data = Unavailable.getdata
    @out = []
    data.each do |item|
      unv = item['Недоступность']
      rep_truck = item['Заявки на ремонт']
      rep_trailer = item['#Заявки на ремонт']
      rep_man = item['#Заявки на водителя']
      
      status_ts = StatusTs.new

      status_ts.truck_num = item['Тягач'] || ''
      status_ts.trailer_num = item['Прицеп'] || ''
      status_ts.driver_name = item['Водитель'] || ''
      status_ts.logist = item['Ответственный логист'] || ''
      status_ts.prev_entry = (item['Предыдущая заявка'] || '').delete("'")
      status_ts.current_contractor = (item['Клиент текущей заявки'] || '').delete("'")
      status_ts.current_entry_from = item['Текущая заявка Откуда'] || ''
      status_ts.current_entry_to = item['Текущая заявка Куда'] || ''
      status_ts.release_date_current_entry = item['Время высвобождения по текущей заявке'] || ''
      status_ts.next_entry = (item['Следующая заявка'] || '').delete("'")
      status_ts.unavailable = unv && unv.index('#') ? {num: unv[0..unv.index('#')-1], link: unv[unv.index('#')+1..-1]} : '' 
            
      status_ts.repair_order = []
      status_ts.repair_order << { num: rep_truck[0..rep_truck.index('#')-1], link: rep_truck[rep_truck.index('#')+1..-1], img: 'tg_1.png' } if rep_truck && rep_truck.index('#')
      status_ts.repair_order << { num: rep_trailer[0..rep_trailer.index('#')-1], link: rep_trailer[rep_trailer.index('#')+1..-1], img: 'pr_1.png' } if rep_trailer && rep_trailer.index('#')
      status_ts.repair_order << { num: rep_man[0..rep_man.index('#')-1], link: rep_man[rep_man.index('#')+1..-1], img: 'man.png' } if rep_man && rep_man.index('#')
      
      status_ts.unavailable_color = item['#Недоступность']
      @out << status_ts
    end  
    @out
  end
  
  def attr_values
    self.instance_values.values
  end
  
  def attr_keys
    self.instance_values.keys
  end
  
end    


end