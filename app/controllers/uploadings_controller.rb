require 'unv'

class UploadingsController < ApplicationController
  
  #skip_before_action :authenticate_user!, :only => [:index, :newdata]
  before_action :save_param, :olny => [:select_filter]
  before_action :all_params, :olny => [:index, :show_filter_and_search, :select_filter]
  
  
  def index
    clear_session_params unless request.xhr?
    
    @data = params[:message] || Unavailable.uploading2
    if params[:message]
      @data = JSON.parse(@data)
      @col_names = @data.first
      @upl = []
      @data[1].each { | item | @upl << Uploading.new(item)}
    else  
      @col_names, @upl = Unavailable.uploading2
    end
    
    user_session[:col_names] = @col_names
      
    if @sort_ind and @sort_dir
      attr_name = @upl.first.instance_values.keys[@sort_ind]
      if @sort_dir == 'up'
        @upl.sort! { |a, b| a.send(attr_name) <=> b.send(attr_name) }
      else
        @upl.sort! { |a, b| b.send(attr_name) <=> a.send(attr_name) }        
      end  
    end
    
    if @search and @col_name
      ind = @col_names.index(@col_name)
      attr_name = @upl.first.instance_values.keys[ind]
      @tmp = []
      for search in @search.split(',')
        @tmp += @upl.find_all { |u| u.send(attr_name) == search.strip.delete("'") }.compact
      end
      @upl = @tmp
    elsif @filter and @col_name
      ind = @col_names.index(@col_name)
      attr_name = @upl.first.instance_values.keys[ind]
      if @filter == 'Не пусто'
        @upl = @upl.find_all { |u| !u.send(attr_name).to_s.strip.blank?  }.compact
      else
        @upl = @upl.find_all { |u| u.send(attr_name) == @filter }.compact
      end
    end
    @upl_count = @upl.size
    
    @upl = @upl.paginate(page: @page || 1, per_page: 20)
    @upl = @upl.paginate(page: 1, per_page: 20) if @upl.size < 1
    
    
    respond_to do |format|
      format.html # index.html.erb
      format.js { }
    end
  end

  
  def save_param
    if params[:sort_ind] or params[:sort_dir] or params[:page] or params[:col_name] or params[:filter] or params[:search]
      user_session[:search] = params[:search] unless params[:search].blank?
      user_session[:sort_id] = params[:sort_ind] unless params[:sort_ind].blank?
      user_session[:sort_dir] = params[:sort_dir] unless params[:sort_dir].blank?
      user_session[:col_name] = params[:col_name] unless params[:col_name].blank?
      user_session[:filter] = params[:filter] unless params[:filter].blank?
      user_session[:page] = params[:page] unless params[:page].blank?
    end  
    #render text: params[:search]
    #render nothing: true
  end
  
  def show_filter_and_search
    respond_to do |format|
      format.js   {}
    end
  end
  
  def select_filter
    @col_names, @upl = Unavailable.uploading2
    ind = @col_names.index(@col_name)
    attr_name = @upl.first.instance_values.keys[ind]

    @for_filter = @upl.map { |u| u.send(attr_name).strip.delete("'")  if u.send(attr_name) and !u.send(attr_name).blank? }.compact.uniq
    @for_search = @for_filter.sort
    @for_filter.unshift('Не пусто')
    
    respond_to do |format|
      format.js   {}
    end
  end
  

  private
  

  def all_params
    @sort_ind = user_session[:sort_id].to_i
    @sort_dir = user_session[:sort_dir]
    @col_names = user_session[:col_names] 
    @col_name = user_session[:col_name] # столбец фильтрации 
    @filter = user_session[:filter]
    @search = user_session[:search]
    @page = user_session[:page] || 1 
  end
  
  def clear_session_params
    user_session[:sort_id] = nil
    user_session[:sort_dir] = nil
    user_session[:page] = nil
    user_session[:col_name] = nil
    user_session[:filter] = nil
    user_session[:search] = nil
    
    @sort_ind = nil
    @sort_dir = nil
    @page = nil
    @col_names = nil
    @col_name = nil
    @filter = nil
    @search = nil
    
    
  end
  
end
