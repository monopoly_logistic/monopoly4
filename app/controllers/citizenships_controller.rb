class CitizenshipsController < ApplicationController
  before_action :set_citizenship, only: [:show, :edit, :update, :destroy]


  def index
    @citizenships = Citizenship.all
  end

  def show
  end

  def new
    @citizenship = Citizenship.new
  end

  def edit
  end

  def create
    @citizenship = Citizenship.new(citizenship_params)

    if @citizenship.save
      redirect_to citizenships_url, notice: 'Гражданство добавлено'
    else
      render :new
    end
  end

  def update
    if @citizenship.update(citizenship_params)
      redirect_to citizenships_url, notice: 'Гражданство сохранено'
    else
      render :edit
    end
  end

  def destroy
    @citizenship.destroy
    redirect_to citizenships_url, notice: 'Гражданство удалено'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_citizenship
    @citizenship = Citizenship.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def citizenship_params
    params.require(:citizenship).permit(:name, :is_close)
  end
end
