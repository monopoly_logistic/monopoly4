# -*- encoding : utf-8 -*-


class EmployeeHistoriesController < ApplicationController


  def index
 #   @employee_histories = EmployeeHistory.all
    @employee_histories_count = EmployeeHistory.count
    @employee_histories = EmployeeHistory.order(:person_id)#.paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @employee_histories }
    end
  end


  def show
    @employee_history = EmployeeHistory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @employee_history }
    end
  end


  def new
    @employee_history = EmployeeHistory.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @employee_history }
    end
  end


  def edit
    @employee_history = EmployeeHistory.find(params[:id])
  end


  def create
    @employee_history = EmployeeHistory.new(employee_history_params)

    respond_to do |format|
      if @employee_history.save
        format.html { redirect_to employee_histories_url, notice: 'Employee history добавлен.' }
        format.json { render json: @employee_history, status: :created, location: @employee_history }
      else
        format.html { render action: "new" }
        format.json { render json: @employee_history.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @employee_history = EmployeeHistory.find(params[:id])

    respond_to do |format|
      if @employee_history.update_attributes(employee_history_params)
        format.html { redirect_to employee_histories_url, notice: 'Employee history сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @employee_history.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @employee_history = EmployeeHistory.find(params[:id])
    @employee_history.close
    #@employee_history.destroy

    respond_to do |format|
      format.html { redirect_to employee_histories_url }
      format.json { head :no_content }
    end
  end


 private

 def employee_history_params
   params.require(:employee_history).permit(:contractor_id, :contractor_name, :department_id, :department_name, :employee_id, :employment_date, :end_date, :id,
                  :leaving_date, :person_id, :position_id, :position_name, :staff_unit_id, :start_date)
 end

end

