class PeopleController < ApplicationController

  def index

    @letters =  Person.pluck(:last_name).map{|t| t[0] if t}.compact.uniq.sort
    @letter = params[:letter] || @letters.first

    if @letter == 'all'
      @people = Person.all
    else
      @people = Person.where("last_name LIKE ?" , "#{@letter}%")
    end

    respond_to do |format|
      format.html 
    end
  end

  def drivers

    @people_all = Person.drivers
    @contractor_representative = ContractorRepresentative.new
    
    @letters = @people_all.map{|t| t.last_name[0] if t.last_name}.compact.uniq.sort
    @letter = params[:letter] || @letters.first

    if @letter == 'all'
      @people = @people_all.sort_by(&:fullname)
    else
      @people = @people_all.find_all {|f| f.last_name[0] == @letter}
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @people }
    end
  end


  def show
    redirect_to people_url(letter: params[:letter])
  end

  def new_person
    @person = Person.new
    @person.gender = true
    @document = Document.new
    respond_to do |format|
      format.js { }
    end
  end


  def new
    @person = Person.new
    @person.gender = true
    @phone = Phone.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @person }
    end
  end


  def edit
    @person = Person.find(params[:id])
    @phone = Phone.new
  end


  def create
    @person = Person.new(person_params)

    respond_to do |format|
      format.html do 
        if @person.save
          redirect_to edit_person_path(@person), notice: 'Физ. лицо добавлено' 
        else
          render action: "new"
        end  
      end
      
      format.js do
        @document = Document.new(document_params)
        @document.documentkind_id = Documentkind.where("documentkinds.name = ?", 'Паспорт гражданина Российской Федерации').first.id
        if @person.save and @document.valid?
          @document.save!
          @person.documents << @document
          flash[:notice] = "Физ. лицо добавлено"
          render action: "#{referrer_controller}"
        else
          render action: "new_person"
        end
      end
    end

  end


  def update
    @person = Person.find(params[:id])

    respond_to do |format|
      format.html do 
        if @person.
          redirect_to edit_person_path(@person), notice: 'Физ. лицо сохранено.' 
        else
          render action: "edit"
        end  
      end
      
      format.js do
        @document = Document.new(document_params)
        @document.documentkind_id = Documentkind.where("documentkinds.name = ?", 'Паспорт гражданина Российской Федерации').first.id
        if @person.update_attributes(person_params) and @document.valid?
          @document.save!
          @person.documents << @document
          flash[:notice] = "Физ. лицо добавлено"
          render action: "#{referrer_controller}"
        else
          render action: "new_person"
        end
      end
    end    

  end


  def destroy
    @person = Person.find(params[:id])
    current_user.admin ? @person.destroy : @person.close

    respond_to do |format|
      format.html { redirect_to people_url }
      format.json { head :no_content }
    end
  end

  def remove_doc
    @document = Document.find(params[:id])
    tmp = @document
    tmp_p = @document.person
    current_user.admin ? @document.destroy : @document.close

    respond_to do |format|
      format.html { redirect_to people_url, notice: "Документ #{tmp.documentkind.name} для #{tmp_p.fullname} удален" }
      format.json { head :no_content }
    end
  end

  def show_part
    respond_to do |format|
      format.js   {}
    end
    @part = params[:part]
    @person = Person.find(params[:id]) if params[:id]
  end

  def phone_person

    if params[:id]
      @person = Person.find(params[:id])
      par = params[:phone]
      id = SecureRandom.uuid
      @phone = Phone.create(id: id, phone: par[:phone], start_date: par[:start_date], end_date: par[:end_date])
      @person.phoneuses.create(phone_id: id, id: SecureRandom.uuid, issue_date: Time.now )
      #@person.reload
      flash[:notice] = "Номер #{par[:phone]} добавлен"
    elsif params[:remove]
      @phone = Phone.find(params[:remove])
      @person = Person.find(params[:person])
      Phoneuse.where({phone_id: @phone.id, person_id: @person.id}).delete_all
      flash[:notice] = "Номер удален"
    end

    respond_to do |format|
      format.js   {render action: "complete"}
    end

    #render text: params[:phone]
  end

  def contacts
    @person ||= Person.find(params[:id])
    @contacts ||= @person.contacts
    @contact ||= Contact.new
  end

  
  
  private


  def complete; end

  def hide; end

  def contractor_representatives
    @contractor_representative = ContractorRepresentative.new
  end
  
  def people
    
  end

  def document_params
    params.require(:document).permit(:series, :number, :issued_date, :issued_by)
  end

  def person_params
    params.require(:person).permit(:birthdate, :last_name, :name, :surname, :gender, :start_date, :end_date,  :mail, :is_close, :id)
  end



end
