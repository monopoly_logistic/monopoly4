class ReportsController < ApplicationController
  require "net/http"
  def ts_spb
    @table_data = get_table_data('http://Esbsrv01.monopoly.su/ESB/Reports/TrucksAtBase.ashx')
  end
  
  def operation_log
    header = { "ACCEPT" => "text/json" }
    @table_data = get_table_data('http://esbsrv01.monopoly.su:5555/api/SmartTruckReports/OperationsLog', header)
    #render text: @table_data#['Data']['Head']
  end
  
  
  private
  
  def get_table_data(str, header = nil)
    url = str 
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri, header)
    resp = http.request(request)
    JSON.parse(resp.body.to_s.force_encoding(Encoding::UTF_8))
  end

  
end
