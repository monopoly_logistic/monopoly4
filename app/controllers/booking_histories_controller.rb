class BookingHistoriesController < ApplicationController
  before_action :set_booking_history, only: [:show, :edit, :update, :destroy]


  def index
    @booking_histories = BookingHistory.all
  end

  def show
  end

  def new
    @booking_history = BookingHistory.new
  end

  def edit
  end

  def create
    @booking_history = BookingHistory.new(booking_history_params)

    if @booking_history.save
      redirect_to @booking_history, notice: 'Booking history добавлен'
    else
      render :new
    end
  end

  def update
    if @booking_history.update(booking_history_params)
      redirect_to @booking_history, notice: 'Booking history сохранен'
    else
      render :edit
    end
  end

  def destroy
    @booking_history.destroy
    redirect_to booking_histories_url, notice: 'Booking history удален'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_booking_history
      @booking_history = BookingHistory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def booking_history_params
      params.require(:booking_history).permit(:booking_id, :status, :status_start_date, :status_end_date, :status_user_id, :is_close)
    end
end
