class MonitoringConnectionsController < ApplicationController
  
  before_action :all_params, :olny => [:show_filter_and_search, :select_filter]
  before_action :connection_data_query, :olny => [ :index, :save_xls, :quick_agrees]

  
  def index 

    #render text: @data
  end

  
  def save_param
      if !params[:previous_entry].blank? and !params[:next_entry].blank?
        entry = [ params[:previous_entry].to_s.strip, params[:next_entry].to_s.strip ]
        if user_session[:conn_entrys] && user_session[:conn_entrys].include?(entry) 
          user_session[:conn_entrys].delete(entry)
        else
          user_session[:conn_entrys] ||= [] 
          user_session[:conn_entrys] << entry
        end  
      end
      
      unless params[:all].blank?
        user_session[:conn_show_all] = params[:all] 
      end
      
      unless params[:agreed].blank?
        user_session[:conn_agreed] = params[:agreed] == 'false' ? nil : params[:agreed]
      end
      unless params[:search].blank?
        params[:search].split(',').each do |search|
          user_session[:filter]  << [ params[:col_name],  search.strip ] unless search.strip.blank?
          user_session[:filter].uniq!
        end
      end
      unless params[:filter].blank?
        user_session[:filter] ||= []
        #user_session[:filter].delete_if { |f| f[0] ==  user_session[:col_name] } if user_session[:filter]
        user_session[:filter] << [ params[:col_name],  params[:filter] ]
        user_session[:filter].uniq!
      end
      user_session[:sort_ind] = params[:sort_ind] unless params[:sort_ind].blank?
      user_session[:sort_dir] = params[:sort_dir] unless params[:sort_dir].blank?
      user_session[:col_name] = params[:col_name] unless params[:col_name].blank?
      user_session[:page] = params[:page] unless params[:page].blank?

    user_session[:filter].delete_at(params[:id].to_i) if request.delete?
    clear_session_params if params[:clear]
    
    connection_data_query
    respond_to { |format| format.js {} }
  end
  
  def show_filter_and_search
    respond_to { |format| format.js {} }
  end
  
  def select_filter
    @data = Unavailable.connection_request2(@from, @to)
    @col_names, @connections =  @data
    @connections = @connections.find_all { |u| user_session[:conn_agreed] != 'true' ? visible_connection?(u) : !visible_connection?(u)}
    
    ind = @col_names.index(params[:col_name])
    user_session[:col_name] = params[:col_name]
    attr_name = @connections.first.instance_values.keys[ind]
    @col_name = params[:col_name]
    @for_filter = @connections.map { |u| u.send(attr_name).strip.delete("'")  if u.send(attr_name) and !u.send(attr_name).blank? }.compact.uniq
    @for_filter.unshift('Не пусто')
    
    respond_to { |format|  format.js {} }
  end
  
  def connections_unv
    respond_to do |format|
      format.js   {}
    end
    @conn_unv = Unavailable.connections_unv(params[:id])
    @col_names =  @conn_unv[0].keys
    @conn_unv.map!{|u| u.values}
    @popup_head = params[:ts]
  end
  
  
  def new_comment
    @order_from_key = params[:order_from_key]
    @order_to_key = params[:order_to_key]
    @truck = params[:truck]
    @comments = UnvComment.find_comments(@order_from_key, @order_to_key)
    render :comments
  end
  
  def create_comment
    UnvComment.create({user_id: current_user.id, OrderFromKey: params[:OrderFromKey], OrderToKey: params[:OrderToKey], comment: params[:comment]})
    @order_from_key = params[:OrderFromKey]
    @order_to_key = params[:OrderToKey]
    @truck = params[:truck]
    @comments = UnvComment.find_comments(@order_from_key, @order_to_key)

    render :comments
  end
  
  
  def quick_agrees
    user_session[:conn_entrys].each do |ent|
      agree = @connections.find { |c| c.previous_entry == ent.first && c.next_entry == ent.second }
      Agree.create(user_id: current_user.id, OrderFromKey: agree.order_from_key, OrderToKey: agree.order_to_key) if agree
    end  
    user_session[:conn_entrys] = []
    connection_data_query
    render :index
  end
  
  def agree
    Agree.create({user_id: current_user.id, OrderFromKey: params[:OrderFromKey], OrderToKey: params[:OrderToKey], note: params[:note]})
    @order_from_key = params[:OrderFromKey]
    @order_to_key = params[:OrderToKey]
    @truck = params[:truck]
    @comments = UnvComment.find_comments(@order_from_key, @order_to_key)
    render :comments
  end
  
  def disagree
    @agree = Agree.find_agree(params[:OrderFromKey], params[:OrderToKey])
    @agree.first.destroy if @agree.any?
    @order_from_key = params[:OrderFromKey]
    @order_to_key = params[:OrderToKey]
    @truck = params[:truck]
    @comments = UnvComment.find_comments(@order_from_key, @order_to_key)
    render :comments
  end
  
  def quick_disagree
    @agree = Agree.find_agree(params[:OrderFromKey], params[:OrderToKey])
    @agree.first.destroy if @agree.any?
    connection_data_query
    render :index
  end
  
  def comments
    respond_to { |format|  format.js {} }
  end
  
  def save_xls

    @invisible_cols = []
    @col_names.each_with_index { |cn,ind | @invisible_cols << ind if cn.index('#') }
    @col_names = @col_names.find_all { |u| !u.index('#') }

    @connections.map! { |con| con.instance_values.values }
    @connections.each { |con| con.delete_if.with_index { |c, ind| @invisible_cols.include?(ind) }  }



    respond_to do |format|
      format.xls {
        connections = Spreadsheet::Workbook.new
        list = connections.create_worksheet name: 'connections'
        list.row(0).concat @col_names
        @connections.each_with_index { |con, i| list.row(i+1).push *con[0..-2]  }
        header_format = Spreadsheet::Format.new color: :green, weight: :bold
        list.row(0).default_format = header_format
        #output to blob object
        blob = StringIO.new("")
        connections.write blob
        #respond with blob object as a file
        send_data blob.string, type: :xls, filename: "Cоcтояние_ТС_#{Time.now.to_i.to_s}.xls"
        }
    end
  end

  private
  
  def connection_data_query
    all_params
    
    @data = Unavailable.connection_request2(@from, @to)
    @col_names, @connections =  @data

    @connections = @connections.find_all { |u| user_session[:conn_agreed] != 'true' ? visible_connection?(u) : !visible_connection?(u)}
    
    user_session[:col_names] = @col_names

   
    unless @filter.blank?
      @keys = @connections.first.instance_values.keys
      @tmp = []
      @filter.each_with_index do |filter, ind|
        ind = @col_names.index(filter.first)
        attr_name = @keys[ind]
        if filter.second == 'Не пусто'
          @tmp = @connections.find_all { |u| !u.send(attr_name).to_s.strip.blank?  }.compact
        else
          if ind > 0 && @filter[(ind - 1)] && @filter[ind -1].first == filter.first
            @tmp += @connections.find_all { |u| u.send(attr_name) == filter.second }.compact
          else
            @connections = @connections.find_all { |u| u.send(attr_name) == filter.second }.compact
          end  
        end
      end 
      @connections &= @tmp unless @tmp.blank?
    end    
        
    if @sort_ind and @sort_dir
      @attr_name = @connections.first.instance_values.keys[@sort_ind]
      if @sort_dir == 'up'
        @connections.sort! { |a, b| is_date(a.send(@attr_name)) <=> is_date(b.send(@attr_name)) }
      else
        @connections.sort! { |a, b| is_date(b.send(@attr_name)) <=> is_date(a.send(@attr_name)) }        
      end  
    end
    
    
    @connections.each do |connection|
      con_entries = [ connection.previous_entry.to_s.strip, connection.next_entry.to_s.strip ]
      connection.is_checked = user_session[:conn_entrys].include?(con_entries) if user_session[:conn_entrys]
    end  
    
    @connections_count = @connections.size
    unless @show_all
      @connections = @connections.paginate(page: @page || 1, per_page: 20)
      @connections = @connections.paginate(page: 1, per_page: 20) if @connections.size < 1
    else
      @connections = @connections.paginate(page: 1, per_page: 1000)
    end  
    
    
    
  end
  

  def all_params
    @sort_ind = user_session[:sort_ind].to_i
    @sort_dir = user_session[:sort_dir]
    @col_names = user_session[:col_names] 
    @col_name = user_session[:col_name] # столбец фильтрации 
    @filter = user_session[:filter] || []
    @page = user_session[:page] || 1 
    @show_all = user_session[:conn_show_all]
  end
  
  def clear_session_params
    user_session[:sort_ind] = nil
    user_session[:sort_dir] = nil
    user_session[:page] = nil
    user_session[:col_name] = nil
    user_session[:filter] = []
    user_session[:conn_entrys] = []
    user_session[:conn_agreed] = nil
    user_session[:conn_show_all] = nil
    
    @sort_ind = nil
    @sort_dir = nil
    @page = 1
    @col_names = nil
    @col_name = nil
    @filter = nil
    @search = nil
    @show_all = nil
  end
  
  def is_date(str)
    begin
      DateTime.strptime(str, '%d.%m.%y %k:%M').to_time.to_i.to_s
    rescue # ArgumentError
      str.to_s
    end
  end
  
  def agree?(con)
    Agree.find_agree(con.order_from_key, con.order_to_key).any?
  end

  def visible_connection?(con)
    #str[12].to_s == '1' or ( str[13].blank? or str[14].blank? ? false : UnvComment.find_comments(str[13],str[14]).any? ) or agree?(str)
    !agree?(con)
  end
  
  
end
