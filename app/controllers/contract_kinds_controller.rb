# -*- encoding : utf-8 -*-

class ContractKindsController < ApplicationController


  def index
 #   @contract_kinds = ContractKind.all
    @contract_kinds_count = ContractKind.count
    @contract_kinds = ContractKind
          .joins(:contract_type)
          .order("contract_types.name")
          #.paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contract_kinds }
    end
  end


  def new
    @contract_kind = ContractKind.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contract_kind }
    end
  end


  def edit
    @contract_kind = ContractKind.find(params[:id])
  end


  def create
    @contract_kind = ContractKind.new(contract_kind_params)

    respond_to do |format|
      if @contract_kind.save
        format.html { redirect_to contract_kinds_url, notice: 'Вид договора добавлен.' }
        format.json { render json: @contract_kind, status: :created, location: @contract_kind }
      else
        format.html { render action: "new" }
        format.json { render json: @contract_kind.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @contract_kind = ContractKind.find(params[:id])

    respond_to do |format|
      if @contract_kind.update_attributes(contract_kind_params)
        format.html { redirect_to contract_kinds_url, notice: 'Вид договора сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contract_kind.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @contract_kind = ContractKind.find(params[:id])
    @contract_kind.close
    #@contract_kind.destroy

    respond_to do |format|
      format.html { redirect_to contract_kinds_url }
      format.json { head :no_content }
    end
  end


 private

 def contract_kind_params
   params.require(:contract_kind).permit(:contract_type_id, :descr, :id, :is_close, :name)
 end


end

