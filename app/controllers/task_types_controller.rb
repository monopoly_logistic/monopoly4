class TaskTypesController < ApplicationController
  before_action :set_task_type, only: [:show, :edit, :update, :destroy]


  def index
    @task_types = TaskType.all
  end

  def show
  end

  def new
    @task_type = TaskType.new
  end

  def edit
  end

  def create
    @task_type = TaskType.new(task_type_params)

    if @task_type.save
      redirect_to task_types_url, notice: 'Тип задач добавлен'
    else
      render :new
    end
  end

  def update
    if @task_type.update(task_type_params)
      redirect_to task_types_url, notice: 'Тип задач сохранен'
    else
      render :edit
    end
  end

  def destroy
    @task_type.destroy
    redirect_to task_types_url, notice: 'Тип задач удален'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task_type
      @task_type = TaskType.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def task_type_params
      params.require(:task_type).permit(:task_type, :descr)
    end
end
