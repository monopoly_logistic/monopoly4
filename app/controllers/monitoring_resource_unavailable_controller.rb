class MonitoringResourceUnavailableController < ApplicationController

  before_action :set_columns
  
  def processes
    @from = params[:from].blank? ? 3.days.ago : params[:from].to_time
    @to = params[:to].blank? ? 3.days.since : params[:to].to_time
    
    @from_par = DateTime.new(@from.year, @from.month, @from.day, 0, 0, 0).to_time
    @to_par = DateTime.new(@to.year, @to.month, @to.day, 23, 59, 59).to_time
    
    @from, @to = @to, @from if @from > @to
    
    @all_processes = Inaccessibility.get_data @from_par.to_i, @to_par.to_i
    #@reg_nums = @all_processes.map { |r| r["Рег. № тягача"] }.compact.uniq.sort
    @truck_keys = @all_processes.map { |r| r["TruckKey"] }.compact.uniq.sort
    #--------
    # Все процессы
    @all_processes_title = @all_processes.first.keys.take 4
    @processes = @all_processes.find_all { |p| p['IsInaccessibility'] == 1 }.map { |p| p.values.map { |v| v.to_s }.take 4}.sort_by { |a| [a[0], a[1]]}
    
    #--------
    # Все процессы без вложений
    @tmp_without_nesting = []
    @truck_keys.each do |truck_key|
      @res = @all_processes.find_all{ |a| a["TruckKey"] == truck_key }
      @res.each do |res|
        @res.delete_if { |r| (r['Момент начала'] >= res['Момент начала']) && (r['Момент окончания'] <= res['Момент окончания']) && r != res }
      end
      @tmp_without_nesting += @res
    end
    @processes_without_nesting = @tmp_without_nesting.uniq.find_all { |p| p['IsInaccessibility'] == 1 }.sort_by { |a| [a["Рег. № тягача"], a['Момент начала']]}.map { |p| p.values.map { |v| v.to_s }.take 4 }
    
    #--------
    #Все процессы без наложений
    @tmp_without_overlap = []
    @truck_keys.each do |truck_key|
      @res = @tmp_without_nesting.uniq.sort_by { |a| [a["Рег. № тягача"], a['Момент начала']]}.find_all{ |a| a["TruckKey"] == truck_key }
      @res.each_cons(2) do |res|
        if res[0]['Момент окончания'] >= res[1]['Момент начала']
          res[0]['Момент окончания'] = res[1]['Момент начала']
        end
      end
      @tmp_without_overlap += @res
    end
    @processes_without_overlap = @tmp_without_overlap.find_all { |p| p['IsInaccessibility'] == 1 }.sort_by { |a| [a["Рег. № тягача"], a['Момент начала']]}.map { |p| p.values.map { |v| v.to_s }.take 4 }
    
    #--------
    #Все процессы без наложений
    @tmp_with_waiting = []
    @truck_keys.each do |truck_key|
      @res = @tmp_without_overlap.uniq.find_all{ |a| a["TruckKey"] == truck_key }.sort_by { |a| a['Момент начала'] }
      @res.each_cons(2) do |res|
        if res.second['IsInaccessibility'] == 1 && res.second['Момент начала'] > res.first['Момент окончания']
          @tmp_with_waiting += 
            [{"Рег. № тягача" => res.second['Рег. № тягача'], "Момент начала"=> res.first['Момент окончания'], 
              "Момент окончания" => res.second['Момент начала'], "Тип сущности" => "Ожидание недоступности: " + res.second['Тип сущности'], 
              "Прицеп и водитель" => "", "Стыковка" => 0, "TruckKey" => truck_key, "IsInaccessibility" => 1}]
        end
      end
    end
    @processes_with_waiting_all = (@tmp_without_overlap + @tmp_with_waiting).find_all { |p| p['IsInaccessibility'] == 1 }.sort_by { |a| [a["Рег. № тягача"], a['Момент начала']]}
    @processes_with_waiting = @processes_with_waiting_all.map { |p| p.values.map { |v| v.to_s }.take 4 }
      
    #--------
    #Все процессы - группировка по тягачам
    @group_by_truck_title = ['Рег № тягача', 'Кол-во мин', 'Кол-во часов']
    @group_by_truck = []
    @truck_keys.each do |truck_key|
      @tmp_group_by_truck = @processes_with_waiting_all.find_all{ |a| a["TruckKey"] == truck_key }
      @group_by_truck << [@tmp_group_by_truck.first['Рег. № тягача'], 
                          @tmp_group_by_truck.sum { |s| ((s['Момент окончания'] - s['Момент начала']) / 1.minute).round },
                          @tmp_group_by_truck.sum { |s| ((s['Момент окончания'] - s['Момент начала']) / 1.hour).round }
                         ] unless @tmp_group_by_truck.blank?
    end
      
    #--------
    #Все процессы - группировка по типу недоступности
    @group_by_inaccess_title = ['Тип недоступности', 'Кол-во мин', 'Кол-во часов']
    @group_by_inaccess = []
    @processes_with_waiting_all.map { |r| r["Тип сущности"] }.compact.uniq.sort.each do |inaccess_type|
      @tmp_group_by_inaccess = @processes_with_waiting_all.find_all{ |a| a["Тип сущности"].strip == inaccess_type.strip }
      @group_by_inaccess << [inaccess_type, 
                             @tmp_group_by_inaccess.sum { |s| ((s['Момент окончания'] - s['Момент начала']) / 1.minute).round },
                             @tmp_group_by_inaccess.sum { |s| ((s['Момент окончания'] - s['Момент начала']) / 1.hour).round }
                            ] unless @tmp_group_by_inaccess.blank?
    end
      
    #--------
    #Все процессы - группировка по тягачу и типу недоступности
    @group_by_truck_and_inaccess_title = ['Рег № тягача', 'Тип недоступности', 'Кол-во мин']
    @group_by_truck_and_inaccess = []
    @truck_keys.each do |truck_key|
      @processes_with_waiting_truck = @processes_with_waiting_all.find_all{ |a| a["TruckKey"] == truck_key }
      @processes_with_waiting_truck.map { |r| r["Тип сущности"] }.compact.uniq.sort.each do |inaccess_type|
        @tmp_group_by_truck_and_inaccess = @processes_with_waiting_truck.find_all{ |a| a["Тип сущности"].strip == inaccess_type.strip }
        @group_by_truck_and_inaccess << [@processes_with_waiting_truck.first['Рег. № тягача'], inaccess_type, @tmp_group_by_truck_and_inaccess.sum { |s| ((s['Момент окончания'] - s['Момент начала']) / 1.minute).round }] unless @tmp_group_by_truck_and_inaccess.blank?
      end
    end
  end
  

  def index
    clear_session_params if params[:clear]
    
    @from = Date.today.at_beginning_of_month
    @to = Date.today
    if params[:moment_from] and params[:moment_to]
      @from = params[:moment_from]
      @to = params[:moment_to]
    end
    @unavailables = MonitoringResourceUnavailable.check_availability(@from.to_time.to_i, @to.to_time.to_i)

    if user_session[:mr_search] and user_session[:mr_col_name]
      ind = @col_names.index(user_session[:mr_col_name])
      attr_name = @unavailables.first.keys[ind]
      @tmp = []
      for search in user_session[:mr_search].split(',')
        @tmp += @unavailables.find_all { |u| u[attr_name] == search.strip.delete("'") }.compact
      end
    elsif user_session[:mr_filter] and user_session[:mr_col_name]
      ind = @col_names.index(user_session[:mr_col_name])
      attr_name = @unavailables.first.keys[ind]
      if @filter == 'Не пусто'
        @tmp = @unavailables.find_all { |u| !u[attr_name].to_s.strip.blank?  }.compact
      else
        @tmp = @unavailables.find_all { |u| u[attr_name] == user_session[:mr_filter] }.compact
      end
      @unavailables = @tmp
      
    end 

    respond_to do |format|
      format.html # index.html.erb
      format.js { }
    end
  end

  def show_filter_and_search
    respond_to do |format|
      format.js   {}
    end
  end

  def select_filter
    @unavailables = MonitoringResourceUnavailable.check_availability(params[:from].to_time.to_i, params[:to].to_time.to_i)
    ind = @col_names.index(params[:col_name])
    user_session[:mr_col_name] = params[:col_name]
    attr_name = @unavailables.first.keys[ind]

    @for_filter = @unavailables.map { |u| u[attr_name] }.compact.uniq
    @for_search = @for_filter.sort
    @for_filter.unshift('Не пусто')
    
    respond_to do |format|
      format.js {}
    end
  end

  def save_param
    if params[:filter] or params[:search]
      unless params[:search].blank?
        user_session[:mr_search] = params[:search]
        user_session[:mr_filter] = nil
      end
      unless params[:filter].blank?
        user_session[:mr_filter] = params[:filter] 
        user_session[:mr_search] = nil
      end
    end
    index
  end

  def save_xls

    @unavailables = MonitoringResourceUnavailable.check_availability(params[:from].to_time.to_i, params[:to].to_time.to_i)

    respond_to do |format|
      format.xls {
        connections = Spreadsheet::Workbook.new
        list = connections.create_worksheet name: 'unavailable_resources'
        list.row(0).concat @col_names
        @unavailables.each_with_index { |unv, i| list.row(i+1).push *unv.values  }
        header_format = Spreadsheet::Format.new color: :green, weight: :bold
        list.row(0).default_format = header_format
        #output to blob object
        blob = StringIO.new("")
        connections.write blob
        #respond with blob object as a file
        send_data blob.string, type: :xls, filename: "Мониторинг_недоступности_ресурсов_#{Time.now.to_i.to_s}.xls"
        }
    end
  end

  def set_columns
    @col_names = ['Рег. № тягача', 'Момент начала недоступности', 'Момент окончания недоступности', 'Тип недоступности', 'Прицеп и водитель', 'Стыковка']
  end

  private
  
  
  
  def monitoring_resource_unavailable_params
    params.require(:monitoring_resource_unavailable).permit(:moment_from, :moment_to)
  end
  
  def clear_session_params
    user_session[:mr_col_name] = nil
    user_session[:mr_filter] = nil
    user_session[:mr_search] = nil
  end

    
  class Inaccessibility < Monosql  
    def self.get_data from, to
      connection.select_all("exec [MonopolySunTemp].[dbo].[ms_Inaccessibility_Get_ForPortal] @MomentFrom = #{from}, @MomentTo = #{to}").to_a
    end
  end
   
   
end
