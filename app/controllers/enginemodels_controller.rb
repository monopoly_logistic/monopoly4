class EnginemodelsController < ApplicationController


  def index

    @enginemodels = Enginemodel.visible

    respond_to do |format|
      format.html # index.html.erb
    end
  end


  def new
    @enginemodel = Enginemodel.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end


  def edit
    @enginemodel = Enginemodel.find(params[:id])
  end


  def create
      @enginemodel = Enginemodel.new(enginemodel_params)


    respond_to do |format|
      if @enginemodel.save
        format.html { redirect_to enginemodels_url, notice: 'Модель двигателя ТС добавлена' }
      else
        format.html { render action: "new" }
      end
    end
  end


  def update
    @enginemodel = Enginemodel.find(params[:id])
    respond_to do |format|
      if @enginemodel.update_attributes(enginemodel_params)
        format.html { redirect_to enginemodels_url, notice: 'Модель двигателя ТС сохранена' }
      else
        format.html { render action: "edit" }
      end
    end
  end


  def destroy
    @enginemodel = Enginemodel.find(params[:id])
     @enginemodel.close

    respond_to do |format|
      format.html { redirect_to enginemodels_url( type: enginetype) }
    end
  end


 private

 def enginemodel_params
   params.require(:enginemodel).permit(:displacement, :enginetype_id, :name, :power, :power_kvt, :is_close)
 end

end
