class TariffCalculatorController < ApplicationController
  
  before_action :set_market_params, only: [:market_get_tariff_grid, :edit_current_market_tariff, :save_current_market_tariff, :destroy_current_market_tariff]
  
  def all_tariffs
    @regions = get_regions
    @transport_kinds = get_transport_kinds
    unless @error
      @regions.sort_by! { |r| r.second } 
      @transport_kinds.sort_by! { |t| t['Name']} 
    end
    if request.post?
      @transport_kind = params[:transport_kind]
      @from_city = params[:from_city]
      @to_city = params[:to_city]
      @city = params[:city]
      @market_tariff = get_tariff_grid_history(@from_city, @to_city, @transport_kind).first
      @average_tariff = get_active_orders @transport_kind, @from_city, @to_city
      
      @zero_tariff = get_zero_tariff @city, @from_city, @to_city
      unless @zero_tariff.blank?
        @zero_distance_between = @zero_tariff['distanceBetweenTwoTargetsKM']
        @zero_res = @zero_tariff['res']
        @zero_to_target = @zero_tariff['toTarget']
        @zero_from_target = @zero_tariff['fromTarget']
        @zero_to_target_data = @zero_tariff['ToTargetData']
        @zero_from_target_data = @zero_tariff['FromTargetData']
        @zero_docking_from_target = @zero_tariff['DockingFromTarget']
        @zero_docking_to_target = @zero_tariff['DockingToTarget']
      end  
    end
    
  end
  
  def get_cities
    @param_name = :from_city if params[:from_region]
    @param_name = :to_city if params[:to_region]
    @param_name = :city if params[:region]

    if params[:from_region] || params[:to_region] || params[:region]
      @cities = get_cities_by_region(params[:from_region] || params[:to_region] || params[:region])
      @cities.sort_by! { |c| c['CityName'] } unless @error
    end
  end
  
  def market
    @regions = get_regions
    @transport_kinds = get_transport_kinds
    unless @error
      @regions.sort_by! { |r| r.second } 
      @transport_kinds.sort_by! { |r| r['Name']} 
    end
 
  end
  
  def market_get_tariff_grid
    @market_tariff_grid = get_tariff_grid @region_from, @region_to, @transport_kind
    @cities_from = get_cities_by_region @region_from
    @cities_to = get_cities_by_region @region_to
    unless @error
      @cities_from.sort_by! { |c| c['CityName'] }
      @cities_to.sort_by! { |c| c['CityName'] }
      flash[:notice] = 'Тарифная сетка построена'
    end
    render :market
  end
  
  def edit_current_market_tariff
   @tariff_grid_history = get_tariff_grid_history @city_from, @city_to, @transport_kind    
   @city_from_name = get_cities_by_region(@region_from).find { |r| r['CityKey'] == @city_from }['CityName']
   @city_to_name = get_cities_by_region(@region_to).find { |r| r['CityKey'] == @city_to }['CityName']
   @num = params[:num]
  end
  
  def save_current_market_tariff
    @set_tariff_grid = set_tariff_grid @id, @transport_kind, @city_from, @city_to, @tariff, @start_date, @end_date
    
    if @error
      edit_current_market_tariff
      render :edit_current_market_tariff
    else
      if @id
        flash[:notice] = "Тариф изменен" 
      else  
        flash[:notice] = "Тариф сохранен"
      end  
      market_get_tariff_grid
    end  
  end
  
    def destroy_current_market_tariff
      set_tariff_grid @id, nil, nil, nil, nil, nil, nil, true
      @id = nil
      flash[:notice] = "Тариф удален" unless @error
      edit_current_market_tariff
          @market_tariff_grid = get_tariff_grid @region_from, @region_to, @transport_kind
      @cities_from = get_cities_by_region @region_from
      @cities_to = get_cities_by_region @region_to
      unless @error
        @cities_from.sort_by! { |c| c['CityName'] }
        @cities_to.sort_by! { |c| c['CityName'] }
      end
      #render js: "$('##{@id}').hide('slow'); notice('Удалено #{del}')"
    end
  
  
  # ============================================================================
  

  def average
    @seasons = get_seasons  
    
    @transport_kinds = get_transport_kinds
  end

  def average_active_regions
    @regons = get_active_regions params[:transport_kind]
    @regons.sort_by! { |r| r.second } unless @error
  end

  def average_active_cities
    @cities = get_active_cities params[:transport_kind], params[:region]
  end
  
  
  def set_seasons
    @seasons = get_seasons
    if request.post?
      @start_date = params[:start_date]
      @end_date = params[:end_date]
      @season_type = params[:season_type]
      set_season @start_date, @end_date, @season_type
      average
      render :average unless @error
    end
  end
  
  
  
  def zero
  end
  
  private
  

  #Возвращение перечня типов подвижного состава
  def get_transport_kinds
    url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetTransportKinds.ashx?TrailersOnly=true"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
  
    # --------------------- MARKET ----------------------------
  def set_market_params
    @id = params[:id]
    @transport_kind = params[:transport_kind]
    @region_from = params[:region_from]
    @region_to = params[:region_to]
    @city_from = params[:city_from]
    @city_to = params[:city_to]
    @tariff = params[:tariff]
    @start_date = params[:start_date]
    @end_date = params[:end_date]
  end
  
  def get_regions
    url = "http://esbsrv01.monopoly.su/ESB/ST/Read/GetRegions.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    #request.body =  "{'Secret':12345, 'UserLogin':'#{user_login}'}"
    response = http.request(request)
    get_esb_result response
  end
  
  def get_cities_by_region region_id
    url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetCities.ashx?RegionKey=#{region_id}"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
  
  #По заданному региону Откуда, региону Куда, типу подвижного состава 
  #возвращение массива данных по тарифам, актуальным на текущий момент времени: 
  #город откуда, город куда, значение тарифа
  def get_tariff_grid region_from, region_to, transport_kind
    url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetTariffGrid.ashx?RegionFrom=#{region_from}&RegionTo=#{region_to}&TransportKindId=#{transport_kind}"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
  
  #По заданному городу Откуда, городу Куда, типу подвижного состава 
  #возвращение истории изменения тарифа перевозки из города Откуда в город Куда 
  #(значение тарифа, период действия)
  def get_tariff_grid_history city_from, city_to, transport_kind
    url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetTariffGridHistory.ashx?CityFrom=#{city_from}&CityTo=#{city_to}&TransportKindId=#{transport_kind}"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end

  #Запись значения тарифа перевозки из города Откуда в город Куда с заданным типом подвижного состава с момента времени
  def set_tariff_grid id, transport_kind, city_from, city_to, tariff, start_date, end_date, is_close = false
    id_str = "'Id':'#{id}'," unless id.blank?
    start_date = (start_date.to_time + 3.hours).to_i unless start_date.blank?
    end_date = (end_date.to_time + 3.hours).to_i unless end_date.blank?
    url = "http://esbsrv01.monopoly.su/ESB/MS/Write/SetTariffGrid.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body = "{#{id_str if id_str} 'TransportKindId':'#{transport_kind}', 'CityFromCode':'#{city_from}', 'CityToCode':'#{city_to}', 'TariffValue':'#{tariff}', 'StartDate':#{start_date}, 'EndDate':#{end_date}, 'UserId': '#{current_user.id}', 'IsClose': #{is_close}}"
    response = http.request(request)
    @resp = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
    if @resp['Ok']
      @resp['TariffGridId']
    else  
      @error = @resp['ErrorMessage'] + request.body
    end
  end
  
  # --------------------- AVERAGE ----------------------------
  
  def get_seasons
    url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetSeasons.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
  
  def set_season start_date, end_date, season_type = 0
    start_date = start_date.to_date unless start_date.blank?
    end_date = end_date.to_date unless end_date.blank?
    url = "http://esbsrv01.monopoly.su/ESB/MS/Write/SetSeason.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body = "{'StartDate':'#{start_date}', 'EndDate':'#{end_date}', 'SeasonType': #{season_type}}"
    response = http.request(request)
    @resp = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
    if @resp['Ok']
      @resp['SeasonId']
    else  
      @error = @resp['ErrorMessage']
    end
  end
  
  
  #Возвращение перечня регионов, по городам которых осуществлялись перевозки в последний год в текущем сезоне с заданным типом подвижного состава
  def get_active_regions transport_kind
    url = "http://esbsrv01.monopoly.su/ESB/ST/Read/GetActiveRegions.ashx?TransportKindId=#{transport_kind}"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
  
  #Возвращение по заданному региону перечня городов, по которым осуществлялись перевозки в последний год в текущем сезоне с заданным типом подвижного состава
  def get_active_cities transport_kind, region
    url = "http://esbsrv01.monopoly.su/ESB/ST/Read/GetActiveCities.ashx?TransportKindId=#{transport_kind}&RegionKey=#{region}"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
  
  #Возвращение перечня перевозок (пока: город откуда, город куда, тариф), начинающихся в одном городе, 
  #заканчивающихся в другом городе в рамках периода и сезона с заданным типом подвижного состава
  def get_active_orders transport_kind, city_from, city_to
    url = "http://esbsrv01.monopoly.su/ESB/ST/Read/GetActiveOrders.ashx?TransportKindId=#{transport_kind}&CityFrom=#{city_from}&CityTo=#{city_to}"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end

  #============================== ZERO ====================================
  
  #
  
  def get_zero_tariff city_key, city_to, city_from
    url = "http://esbsrv01.monopoly.su/ESB/ST/Read/test.ashx?KnownCityKey=#{city_key}&TargetCityToKey=#{city_to}&TargetCityFromKey=#{city_from}"
    #url = "http://esbsrv01.monopoly.su/ESB/ST/Read/test.ashx?KnownCityKey=BB10C4B4-9791-4E6C-BE32-07FCE9C05007&TargetCityToKey=420FD3F9-DD1A-4BC6-AE59-FF9DC7CECDE1&TargetCityFromKey=420FD3F9-DD1A-4BC6-AE59-FF9DC7CECDE1"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
  
end
