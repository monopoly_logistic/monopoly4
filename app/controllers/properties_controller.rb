class PropertiesController < ApplicationController
  before_action :set_property, only: [:show, :edit, :update, :destroy]


  def index
    @properties = Property.all
  end

  def show
  end

  def new
    @property = Property.new
  end

  def edit
  end

  def create
    @property = Property.new(property_params)

    if @property.save
      redirect_to properties_url, notice: 'Настройка подписки добавлена'
    else
      render :new
    end
  end

  def update
    if @property.update(property_params)
      redirect_to properties_url, notice: 'Настройка подписки сохранена'
    else
      render :edit
    end
  end

  def destroy
    @property.close
    redirect_to properties_url, notice: 'Настройка подписки удалена'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_property
      @property = Property.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def property_params
      params.require(:property).permit(:system_id, :objecttype_id, :start_date, :end_date, :is_close)
    end
end
