class TaskActionsController < ApplicationController
  before_action :set_task_action, only: [:show, :edit, :update, :destroy]


  def index
    @task_actions = TaskAction.all
  end

  def show
  end

  def new
    @task_action = TaskAction.new
  end

  def edit
  end

  def create
    @task_action = TaskAction.new(task_action_params)

    if @task_action.save
      redirect_to task_actions_url, notice: 'Действие по задаче добавлено'
    else
      render :new
    end
  end

  def update
    if @task_action.update(task_action_params)
      redirect_to task_actions_url, notice: 'Действие по задаче сохранено'
    else
      render :edit
    end
  end

  def destroy
    @task_action.destroy
    redirect_to task_actions_url, notice: 'Действие по задаче удалено'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_task_action
    @task_action = TaskAction.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def task_action_params
    params.require(:task_action).permit(:name, :descr, :is_close)
  end
end
