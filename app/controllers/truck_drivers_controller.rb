class TruckDriversController < ApplicationController



  def index
    @truck_drivers_count = TruckDriver.count

    if !params[:truck].blank? and !params[:driver].blank?
      @truck_drivers = TruckDriver.where("transport_id = ? and employee_id = ?", params[:truck], params[:driver])
    elsif !params[:truck].blank?
      @truck_drivers = TruckDriver.where("transport_id = ?", params[:truck])
    elsif !params[:driver].blank?
      @truck_drivers = TruckDriver.where("employee_id = ?", params[:driver])
    else
      @truck_drivers = TruckDriver.all
    end

    @truck_drivers = @truck_drivers.sort_by(&:start_date).reverse#.paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @truck_drivers }
    end
  end


  def new
    @truck_driver = TruckDriver.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @truck_driver }
    end
  end


  def edit
    @truck_driver = TruckDriver.find(params[:id])
  end


  def create
    @truck_driver = TruckDriver.new(truck_driver_params)

    respond_to do |format|
      if @truck_driver.save
        format.html { redirect_to truck_drivers_url, notice: 'Связка добавлена' }
        format.json { render json: @truck_driver, status: :created, location: @truck_driver }
      else
        format.html { render action: "new" }
        format.json { render json: @truck_driver.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @truck_driver = TruckDriver.find(params[:id])

    respond_to do |format|
      if @truck_driver.update_attributes(truck_driver_params)
        format.html { redirect_to truck_drivers_url, notice: 'Связка сохранена' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @truck_driver.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @truck_driver = TruckDriver.find(params[:id])
    @truck_driver.close
    #@truck_driver.destroy

    respond_to do |format|
      format.html { redirect_to truck_drivers_url }
    end
  end


  def unchain
    @truck_driver = TruckDriver.find(params[:id])
    @truck_driver.unchain_truck_driver
    #@truck_driver.destroy

    respond_to do |format|
      format.html { redirect_to truck_drivers_url }
    end
  end


 private

 def truck_driver_params
   params.require(:truck_driver).permit(:employee_id, :end_date, :id, :is_close, :start_date, :transport_id)
 end

end

