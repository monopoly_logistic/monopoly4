class ProcessTemplatesController < ApplicationController
  before_action :set_process_template, only: [:show, :edit, :update, :destroy]


  def index
    @process_templates = ProcessTemplate.all
  end

  def show
  end

  def new
    @process_template = ProcessTemplate.new
  end

  def edit
  end

  def create
    @process_template = ProcessTemplate.new(process_template_params)

    if @process_template.save
      redirect_to process_templates_url, notice: 'Шаблон процесса добавлен'
    else
      render :new
    end
  end

  def update
    if @process_template.update(process_template_params)
      redirect_to process_templates_url, notice: 'Шаблон процесса сохранен'
    else
      render :edit
    end
  end

  def destroy
    @process_template.destroy
    redirect_to process_templates_url, notice: 'Шаблон процесса удален'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_process_template
    @process_template = ProcessTemplate.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def process_template_params
    params.require(:process_template).permit(:process_type_id, :name, :descr, :is_close)
  end
end
