class ResourceCharacteristicValuesController < ApplicationController
  before_action :set_resource_characteristic_value, only: [:show, :edit, :update, :destroy]


  def index
    @resource_characteristic_values = ResourceCharacteristicValue.all
  end

  def show
  end

  def new
    @resource_characteristic_value = ResourceCharacteristicValue.new
  end

  def edit
  end

  def create
    @resource_characteristic_value = ResourceCharacteristicValue.new(resource_characteristic_value_params)
    respond_to do |format|
      format.html do 
        if @resource_characteristic_value.save
          redirect_to @resource_characteristic_value, notice: 'Значение характеристики добавлено'
        else
          render :new
        end
      end
      format.js do
        if @resource_characteristic_value.save
          flash.now[:notice] = 'Значение характеристики добавлено'
          render "bookings/create"
        else
          @resource_characteristic = @resource_characteristic_value.resource_characteristic 
          render "bookings/characteristic_val"
        end
      end
    end  
  end

  def update
    respond_to do |format|
      format.html do 
        if @resource_characteristic_value.update(resource_characteristic_value_params)
          redirect_to @resource_characteristic_value, notice: 'Значение характеристики сохранено'
        else
          render :edit
        end
      end
      format.js do
        if @resource_characteristic_value.update(resource_characteristic_value_params)
          flash.now[:notice] = 'Значение характеристики добавлено'
          render "bookings/create"
        else
          @resource_characteristic = @resource_characteristic_value.resource_characteristic 
          render "bookings/characteristic_val"
        end
      end
    end  
    

  end

  def destroy
    @resource_characteristic_value.destroy
    redirect_to resource_characteristic_values_url, notice: 'Значение характеристики удалено'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_resource_characteristic_value
      @resource_characteristic_value = ResourceCharacteristicValue.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def resource_characteristic_value_params
      params[:resource_characteristic_value][:is_close] = false
      params[:resource_characteristic_value][:user_id] = current_user.id
      params[:resource_characteristic_value][:truck_id] = nil if params[:resource_characteristic_value][:truck_id].blank?
      params[:resource_characteristic_value][:trailer_id] = nil if params[:resource_characteristic_value][:trailer_id].blank?
      params[:resource_characteristic_value][:driver_id] = nil if params[:resource_characteristic_value][:driver_id].blank?
      params.require(:resource_characteristic_value).permit(:resource_characteristic_id, :truck_id, :trailer_id, :driver_id, :name, :start_date, :end_date, :is_close)
    end
end
