# -*- encoding : utf-8 -*-


class TablesController < ApplicationController


  def index
    @tables = Table.all #.paginate(page: params[:page], per_page: 20).order('name')
   
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tables }
    end
  end


  def show
    @table = Table.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @table }
    end
  end


  def new
    @table = Table.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @table }
    end
  end


  def edit
    @table = Table.find(params[:id])
  end


  def create
    @table = Table.new(table_params)

    respond_to do |format|
      if @table.save
        format.html { redirect_to tables_url, notice: 'Table добавлен.' }
        format.json { render json: @table, status: :created, location: @table }
      else
        format.html { render action: "new" }
        format.json { render json: @table.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @table = Table.find(params[:id])

    respond_to do |format|
      if @table.update_attributes(table_params)
        format.html { redirect_to tables_url, notice: 'Table сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @table.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @table = Table.find(params[:id])
    @table.destroy

    respond_to do |format|
      format.html { redirect_to tables_url }
      format.json { head :no_content }
    end
  end


 private

 def table_params
   params.require(:table).permit(:description, :end, :is_close, :name, :start)
 end

end

