# -*- encoding : utf-8 -*-
class DocumenttypesController < ApplicationController


  def index
    @documenttypes = Documenttype.order(:name)#.paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @documenttypes }
    end
  end



  def new
    @documenttype = Documenttype.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @documenttype }
    end
  end


  def edit
    @documenttype = Documenttype.find(params[:id])
  end


  def create
    @documenttype = Documenttype.new(documenttype_params)

    respond_to do |format|
      if @documenttype.save
        format.html { redirect_to documenttypes_url, notice: 'Тип документа добавлен.' }
        format.json { render json: @documenttype, status: :created, location: @documenttype }
      else
        format.html { render action: "new" }
        format.json { render json: @documenttype.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @documenttype = Documenttype.find(params[:id])

    respond_to do |format|
      if @documenttype.update_attributes(documenttype_params)
        format.html { redirect_to documenttypes_url, notice: 'Тип документа сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @documenttype.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @documenttype = Documenttype.find(params[:id])
    @documenttype.destroy

    respond_to do |format|
      format.html { redirect_to documenttypes_url }
      format.json { head :no_content }
    end
  end


 private

 def documenttype_params
   params.require(:documenttype).permit(:descr, :is_close, :name)
 end

end
