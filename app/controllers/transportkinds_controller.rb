class TransportkindsController < ApplicationController

  def index
    @transportkinds = Transportkind.order(:transporttype_id)

    respond_to do |format|
      format.html # index.html.erb
    end
  end


  def new

    @transportkind = Transportkind.new
    respond_to do |format|
      format.html # new.html.erb
    end
  end


  def edit
    @transportkind = Transportkind.find(params[:id])
  end


  def create
    @transportkind = Transportkind.new(transportkind_params)
    respond_to do |format|
      if @transportkind.save
        format.html { redirect_to transportkinds_path(type: @transportkind.transporttype_id), notice: 'Вид ТС добавлен' }
      else
        format.html { render action: "new" }
      end
    end
  end


  def update
    @transportkind = Transportkind.find(params[:id])
    respond_to do |format|
      if @transportkind.update_attributes(transportkind_params)
        format.html { redirect_to transportkinds_path(type: @transportkind.transporttype_id), notice: 'Вид ТС сохранен' }
      else
        format.html { render action: "edit" }
      end
    end
  end


  def destroy
    @transportkind = Transportkind.find(params[:id])
    @transportkind.close

    respond_to do |format|
      format.html { redirect_to transportkinds_url( type: transporttype ) }
    end
  end


 private

 def transportkind_params
   params.require(:transportkind).permit(:descr,  :name, :transporttype_id, :is_close, :id)
 end


end
