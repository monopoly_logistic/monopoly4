class TransportecologicalsController < ApplicationController


  def index

    @transportecologicals = Transportecological.all

    respond_to do |format|
      format.html # index.html.erb

    end
  end



  def new
    @transportecological = Transportecological.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end


  def edit
    @transportecological = Transportecological.find(params[:id])
  end


  def create
    @transportecological = Transportecological.new(transportecological_params)

    respond_to do |format|
      if @transportecological.save
        format.html { redirect_to transportecologicals_path, notice: 'Экологический класс добавлен' }
      else
        format.html { render action: "new" }
      end
    end
  end


  def update
    @transportecological = Transportecological.find(params[:id])

    respond_to do |format|
      if @transportecological.update_attributes(transportecological_params)
        format.html { redirect_to transportecologicals_url, notice: 'Экологический класс сохранен' }
      else
        format.html { render action: "edit" }
      end
    end
  end


  def destroy
    @transportecological = Transportecological.find(params[:id])
    @transportecological.close

    respond_to do |format|
      format.html { redirect_to transportecologicals_url }
    end
  end


 private

 def transportecological_params
   params.require(:transportecological).permit(:id, :name, :is_close)
 end

end
