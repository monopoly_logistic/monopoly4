# -*- encoding : utf-8 -*-


class DepartmentsController < ApplicationController


  def index

    #    @contractors = Contractor.find(Department.select('distinct contractor_id').map(&:contractor_id))
    @contractors = Contractor.includes(:departments).select("id, name")

    if params[:contractor] and !params[:contractor][:id].blank?
      @departments = Contractor.find(params[:contractor][:id]).departments.order(:name)#.paginate(page: params[:page], per_page: 10)
    else
      @departments = Department.joins(:contractor).order('contractors.name')
      #@departments = @departments.paginate(page: params[:page], per_page: 10)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @departments }
    end
  end


  def show
    @department = Department.find(params[:id])

    respond_to do |format|
      format.js { }
    end
  end


  def new
    @department = Department.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @department }
    end
  end


  def edit
    @department = Department.find(params[:id])
  end


  def create
    @department = Department.new(department_params)

    respond_to do |format|
      if @department.save
        format.html { redirect_to departments_url, notice: 'Подразделение добавлено' }
        format.json { render json: @department, status: :created, location: @department }
      else
        format.html { render action: "new" }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @department = Department.find(params[:id])

    respond_to do |format|
      if @department.update_attributes(department_params)
        format.html { redirect_to departments_url, notice: 'Подразделение сохранено' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @department = Department.find(params[:id])
    @department.close

    respond_to do |format|
      format.html { redirect_to departments_url }
      format.json { head :no_content }
    end
  end


  private

  def department_params
    params[:department].each do |k,v|
      params[:department][k.to_sym] = nil if params[:department][k.to_sym].blank? and  k!= 'is_close'
    end
    params.require(:department).permit(:contractor_id, :descr, :id,  :name, :parent_id, :is_close)
  end

end

