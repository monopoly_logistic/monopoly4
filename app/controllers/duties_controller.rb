class DutiesController < ApplicationController

  before_action :init_group_add


  def index
    @duty = Duty.new
    @group_add = user_session[:group_add]
    @duties = Duty.last_month #.paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  def edit
    @duty = Duty.find(params[:id])
  end


  def create
    @duty = Duty.new(duty_params)
    @duty.user_id = current_user.id
    @duty.id = SecureRandom.uuid
    respond_to do |format|
      format.js do
        user_session[:group_add] << @duty if @duty.valid?
        @group_add = user_session[:group_add]
        render :group_add
      end 
    end  
  end


  def update
    @duty = Duty.find(params[:id])

    respond_to do |format|
      format.js do
        user_session[:group_add] << @duty if @duty.valid?
        @group_add = user_session[:group_add]
        render :group_add
      end 
    end  
  end


  def destroy
    
    respond_to do |format|
      format.html do 
        @duty = Duty.find(params[:id])
        @duty.destroy
        redirect_to( duties_url, notice: 'Дежурный удален.' )
      end
      format.js do
        user_session[:group_add].delete_if { |d| d.id ==  params[:id] }
        @group_add = user_session[:group_add]
        @duty = user_session[:group_add].last || Duty.new
        render :group_add
      end 
    end 
  end

  def add_all
    user_session[:group_add].each { |d| d.save }
    user_session[:group_add] = []
    redirect_to( duties_url, notice: 'Дежурные Назначены' )
  end
  

 private
 
 def group_add; end 

 def init_group_add
   user_session[:group_add] ||= []
 end
 
 def duty_params
   params.require(:duty).permit(:EmployeeKey, :IsLog, :IsMan, :MomentFrom, :MomentTo, :created_at, :user_id, :fio)
 end

end

