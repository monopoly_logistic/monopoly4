class RolesController < ApplicationController

  def index
    @roles = Role.includes(:functionalgroups, :users)

    respond_to do |format|
      format.html # index.html.erb
    end
  end


  def new
    @role = Role.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end


  def edit
    @role = Role.find(params[:id])
  end


  def create
    @role = Role.new(role_params)

    respond_to do |format|
      if @role.save
        format.html { redirect_to roles_path, notice: 'Роль создана.' }
      else
        format.html { render action: "new" }
      end
    end
  end


  def update
    @role = Role.find(params[:id])

    respond_to do |format|
      if @role.update_attributes(role_params)
        format.html { redirect_to roles_path, notice: 'Роль сохранена.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

 
  def destroy
    @role = Role.find(params[:id])
    @role.close

    respond_to do |format|
      format.html { redirect_to roles_url }
      format.json { head :no_content }
    end
  end

  def select_functional
   respond_to do |format|
      format.js   {}
   end
  @role = Role.find(params[:id])
  @functional = Functionalgroup.find(params[:functional][:id])
  @role.functionalgroups << @functional
  flash.now[:notice] = 'Функционал добавлен'

end

def del_functional_from_role
   respond_to do |format|
      format.js   {}
   end
  @role= Role.find(params[:id])
  @functional = Functionalgroup.find(params[:functional])
  @role.functionalgroups.delete(@functional)
  flash.now[:notice] = 'Удалено'
end


 private

 def role_params
   params.require(:role).permit(:name, :is_close, :descr)
 end



end
