class ContactTypesController < ApplicationController
  before_action :set_contact_type, only: [:edit, :update, :destroy]


  def index
    @contact_types = ContactType.all
  end

  def new
    @contact_type = ContactType.new
  end

  def edit
  end

  def create
    @contact_type = ContactType.new(contact_type_params)

    if @contact_type.save
      redirect_to contact_types_url, notice: 'Тип контакта добавлен'
    else
      render :new
    end
  end

  def update
    if @contact_type.update(contact_type_params)
      redirect_to contact_types_url, notice: 'Тип контакта сохранен'
    else
      render :edit
    end
  end

  def destroy
    @contact_type.destroy
    redirect_to contact_types_url, notice: 'Тип контакта удален'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact_type
      @contact_type = ContactType.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def contact_type_params
      params.require(:contact_type).permit(:name, :is_close)
    end
end
