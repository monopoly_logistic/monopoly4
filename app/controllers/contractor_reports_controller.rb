class ContractorReportsController < ApplicationController
  
  def index
    user_session[:from] = nil
    user_session[:to] = nil
    user_session[:contractor] = nil
    user_session[:direction] = nil
    
  end

  def show
    
    user_session[:from] = params[:from] unless params[:from].blank?
    user_session[:to] = params[:to] unless params[:from].blank?
    user_session[:contractor] = params[:contractor] unless params[:contractor].blank? 
    user_session[:direction] = params[:direction] unless params[:direction].blank? 
    user_session[:contractor] = nil if params[:contractor] == 'no'
    user_session[:direction] = nil if params[:direction] == 'no'
    @from = user_session[:from].to_time.to_i
    @to = user_session[:to].to_time.to_i
    @contractor = user_session[:contractor]
    @direction = user_session[:direction]
    
    @contractor_list = ContractorReport.contractor_list(@from, @to, @contractor, @direction).sort_by(&:name)
    @direction_list = ContractorReport.direction_list(@from, @to, @contractor, @direction).sort_by(&:direction)
    
    @current_contractor = @contractor_list.find_all{ |c| c.key == @contractor }.first if @contractor
    @current_direction = @direction 

    
    respond_to do |format|
      format.js { }
    end
  end
  
  def report    
    @from = user_session[:from].to_time.to_i
    @to = user_session[:to].to_time.to_i
    @contractor = user_session[:contractor]
    @direction = user_session[:direction]
    
    @report = ContractorReport.report(@from, @to, @contractor, @direction)
#    render text: @report
  end
  
  def report_info
    @from = user_session[:from].to_time.to_i
    @to = user_session[:to].to_time.to_i
    @contractor = params[:contractor]
    @direction = params[:direction]
    @r_type = params[:r_type] || 1
    @report_info = ContractorReport.report_info(@from, @to, @contractor, @direction, @r_type, 0)
    
    respond_to do |format|
      format.js { }
    end     
  end
  
    def save_xls
    @from = user_session[:from].to_time.to_i
    @to = user_session[:to].to_time.to_i
    @contractor = user_session[:contractor]
    @direction = user_session[:direction]
    
    if params[:info] == '1'
      @r_type = params[:r_type] || '1'
      @report = ContractorReport.report_info(@from, @to, @contractor, @direction, @r_type, 0)
      if @r_type == '4'
        @col_names = ['№ заявки', 'Контрагент', 'Договор', 'Направление', 'Тариф', 'Стоимость структуры и лизинга', 'Переменные издержки', 'Стоимость дебиторки', 'Итого', 'Кол-во заявок', 'Среднее']
      
      else
        @col_names = ['Контрагент', 'Договор', 'Направление', 'Тариф', 'Стоимость структуры и лизинга', 'Переменные издержки', 'Стоимость дебиторки', 'Итого', 'Кол-во заявок', 'Среднее']
      end  
    else   
      @col_names = ['Контрагент', 'Договор', 'Направление', 'Тариф', 'Стоимость структуры и лизинга', 'Переменные издержки', 'Стоимость дебиторки', 'Итого', 'Кол-во заявок', 'Среднее']
      @report = ContractorReport.report(@from, @to, @contractor, @direction)
    end  
    respond_to do |format|
      format.xls {
        report = Spreadsheet::Workbook.new
        list = report.create_worksheet name: 'report'
        list.row(0).concat @col_names
        @report.each_with_index do |r, i| 
          t = r.instance_values.values 
          if  @r_type == '4'
            t.unshift(t[-1])
          end
          list.row(i+1).push *t[0..-2]
        end
        header_format = Spreadsheet::Format.new color: :green, weight: :bold
        list.row(0).default_format = header_format
        #output to blob object
        blob = StringIO.new("")
        report.write blob
        #respond with blob object as a file
        send_data blob.string, type: :xls, filename: "report.xls"
      }
    end
    end
end
