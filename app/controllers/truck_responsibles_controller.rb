class TruckResponsiblesController < ApplicationController
  
  before_action :find_truck_responsible, only: [:new_or_edit,  :create_or_update, :show_responsibles, :unchain_responsible]
  
  def index
    @trucks = get_trucks('http://esbsrv01.monopoly.su/ESB/MS/Read/GetTrucksResponsible.ashx')
    @trucks_without_responsibles = get_trucks('http://esbsrv01.monopoly.su/ESB/MS/Read/GetTrucksWithoutResponsible.ashx')
    if @trucks['Ok'] && @trucks_without_responsibles['Ok']
      @trucks = @trucks['Data']
      @trucks_without_responsibles = @trucks_without_responsibles['Data'].sort_by { |t| t['TruckName'].to_s }
    else
      @error = "#{@trucks['ErrorMessage'] unless @trucks['Ok']} #{@trucks_without_responsibles['ErrorMessage'] unless @trucks_without_responsibles['Ok']}"
    end
  end
  
  def show_responsibles

  end
  
  def new_or_edit
    if @truck_responsibles
      @truck_responsibles_active = @truck_responsibles.find_all { |r| r['Active']}.uniq
      @truck_responsibles_active_ids = @truck_responsibles_active.map { |r| r['ResponsiblePersonId'] }
      @truck_responsible = @truck_responsibles.find { |r| r['TruckResponsibleId'] == params[:responsible_id]} 
    end
    @logists = get_logists
    if @logists['Ok']
      @logists = @logists['Data'].uniq
    else
      @error += @logists['ErrorMessage']
      @logists = nil
    end
    unless @truck
      @truck_name = params[:truck_name]
      @truck_id = params[:truck_id]
      @logists.delete_if { |l| @truck_responsibles_active_ids.include?(l['Id']) }
    end
    
  end
  
  def create_or_update
    id = params[:id]
    truck_id = params[:truck_id]
    person_id = params[:person_id]
    parent_id = params[:parent_id]
    start_date = params[:start_date].blank? ? nil : (params[:start_date].to_datetime).to_i
    end_date = params[:end_date].blank? ? nil : (params[:end_date].to_datetime).to_i
    @resp =  set_truck_responsible id, truck_id, person_id, parent_id, start_date, end_date
    if @resp['Ok']
      flash.now[:notice] = "Сохранено"
      index
      find_truck_responsible
      render :index
    else
      @error = @resp['ErrorMessage'] #+ @req
      render :new_or_edit
    end  
  end
  

  def delete_responsible
    id = params[:id]
    @resp =  set_truck_responsible id, nil, nil, nil, nil, nil, true
    if @resp['Ok']
      flash.now[:notice] = "Ответственный удален"
      index
      find_truck_responsible
      render :index
    else
      @error = @resp['ErrorMessage']
      render :new_or_edit
    end  

  end
  
  
  
private

  def find_truck_responsible
    @truck_responsibles = get_trucks("http://esbsrv01.monopoly.su/ESB/MS/Read/GetTrucksResponsible.ashx?TruckId=#{params[:truck_id]}")
    if @truck_responsibles['Ok']
      @truck_responsibles = @truck_responsibles['Data'].sort_by { |r| [r['Active'] ? 1 : 0, r['StartDate']] }.reverse
      @truck = @truck_responsibles.first
    else
      @error = @truck_responsibles['ErrorMessage']
      @truck_responsibles = nil
    end
  end
  
  def get_trucks(url)
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    #request.body =  "{'Secret':12345, 'UserLogin':'#{user_login}'}"
    response = http.request(request)
    JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
  end
  
  
  def get_logists
    uri = URI.parse('http://esbsrv01.monopoly.su/esb/Hr/JsonApi/PeopleByEmployeeId.ashx?staffunitid=DE81EB82-0329-42C6-B7F0-B129E962151B')
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    #request.body =  "{'Secret':12345, 'UserLogin':'#{user_login}'}"
    response = http.request(request)
    JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
  end
  
  
  def set_truck_responsible id = nil, truck_id = nil, person_id = nil, parent_id = nil, start_date = nil, end_date = nil, is_close = false
    uri = URI.parse('http://esbsrv01.monopoly.su/esb/ms/write/SetTruckResponsible.ashx')
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    #parameters =  {"Id" => "#{id}", "TruckId" => "#{truck_id}", "PersonId" => "#{person_id}", "ParentId" => "#{parent_id}", "StartDate" => "#{start_date}", "EndDate" => "#{end_date}", "IsClose" => false}
    #request.set_form_data(parameters)
    request.body =  "{'Id':'#{id}', 'TruckId':'#{truck_id}', 'PersonId':'#{person_id}', 'ParentId':'#{parent_id}', 'StartDate':#{start_date}, 'EndDate':#{end_date}, 'IsClose':#{is_close}}"
    response = http.request(request)
    #@req = request.body
    JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
  end
  
  
  

end