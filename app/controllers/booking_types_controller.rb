class BookingTypesController < ApplicationController
  before_action :set_booking_type, only: [:show, :edit, :update, :destroy]


  def index
    @booking_types = BookingType.all
  end

  def show
  end

  def new
    @booking_type = BookingType.new
  end

  def edit
  end

  def create
    @booking_type = BookingType.new(booking_type_params)

    if @booking_type.save
      redirect_to booking_types_url, notice: 'Тип брони добавлен'
    else
      render :new
    end
  end

  def update
    if @booking_type.update(booking_type_params)
      redirect_to booking_types_url, notice: 'Тип брони сохранен'
    else
      render :edit
    end
  end

  def destroy
    @booking_type.close
    redirect_to booking_types_url, notice: 'Тип брони удален'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_booking_type
      @booking_type = BookingType.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def booking_type_params
      params.require(:booking_type).permit(:name, :release_limit, :is_close)
    end
end
