class FunctionalsController < ApplicationController

  def index
   if params[:group]
      @group = Usergroup.find(params[:group])
      @functionals = @group.functionals
      @msg = "Группа #{@group.name}"
    elsif params[:role]
      @role = Role.find(params[:role])
      @functionals = @role.functionals
      @msg = "Роль #{@role.name}"
    elsif params[:user]
      @user = User.find(params[:user])
      @functionals = @user.functionals
      @msg = "Пользователь #{@user.email}"
    else
      @functionals = Functional.all
    end


    

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @functionals }
    end
  end


  def show
    @functional = Functional.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @functional }
    end
  end


  def new
    @functional = Functional.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @functional }
    end
  end


  def edit
    @functional = Functional.find(params[:id])
  end


  def create
    @functional = Functional.new(params[:functional])

    respond_to do |format|
      if @functional.save
        format.html { redirect_to @functional, notice: 'Functional was successfully created.' }
        format.json { render json: @functional, status: :created, location: @functional }
      else
        format.html { render action: "new" }
        format.json { render json: @functional.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @functional = Functional.find(params[:id])

    respond_to do |format|
      if @functional.update_attributes(params[:functional])
        format.html { redirect_to @functional, notice: 'Функционал сохранен' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @functional.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @functional = Functional.find(params[:id])
    @functional.close
    respond_to do |format|
      format.html { redirect_to functionals_url }
      format.json { head :no_content }
    end
  end


 private

 def functional_params
   params.require(:functional).permit(:description, :end, :name, :part, :start, :is_close)
 end


end
