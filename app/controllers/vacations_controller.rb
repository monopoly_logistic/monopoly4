class VacationsController < ApplicationController
  before_action :set_vacation, only: [:show, :edit, :update, :destroy]

  
  def index
    #@vacations = Vacation.all
    #@unv_employee = UnvEmployee.new
    #@unv_employee.unv_type_id = 'CE58D7F2-E69A-4F8C-8A36-92E9066B3D5E'
    #@employees = Employee.driver_expeditors
    @from = params[:from].blank? ? 3.days.ago.to_date : params[:from]
    @to = params[:to].blank? ? 3.days.since.to_date : params[:to]

    @unv_employees_not_return = UnvEmployee.where("planned_end <= ? and actual_end IS NULL", Time.now).includes(:person) 
    
    @vacations_return = Vacation.drivers.where(:"vacations.end_date" => @from.to_date..@to.to_date).includes(:person) 
    @unv_employees_leave = UnvEmployee.where(planned_start: @from.to_date..@to.to_date).includes(:person) 
     
    @vacations =Vacation.drivers.where("(vacations.start_date <= ? AND vacations.end_date >= ?) OR (vacations.start_date >= ? AND vacations.start_date <= ?) OR (vacations.end_date >= ? AND vacations.end_date <= ?)", @from.to_date, @to.to_date, @from.to_date, @to.to_date,  @from.to_date, @to.to_date).includes(:person)
    @unv_employees = UnvEmployee.where("(planned_start <= ? AND planned_end >= ?) OR (planned_start >= ? AND planned_start <= ?) OR (planned_end <= ? AND planned_end >= ?)", @from.to_date, @to.to_date, @from.to_date, @to.to_date, @from.to_date, @to.to_date).includes(:person)
    
  end

  def show
  end

  def new
    @vacation = Vacation.new
  end

  def edit
  end

  def create
    @vacation = Vacation.new(vacation_params)

    if @vacation.save
      redirect_to vacations_url, notice: 'Отпуск добавлен'
    else
      render :new
    end
  end

  def update
    if @vacation.update(vacation_params)
      redirect_to vacations_url, notice: 'Отпуск сохранен'
    else
      render :edit
    end
  end

  def destroy
    @vacation.destroy
    redirect_to vacations_url, notice: 'Отпуск удален'
  end
  
  def del_unv_emp
    @from = params[:from]
    @to = params[:to]
    @unv_employee = UnvEmployee.find(params[:id])
    @unv_employee.close
    index
    render :save_unv_emp
  end

  def edit_unv_emp
    @from = params[:from]
    @to = params[:to]
    @unv_employee = UnvEmployee.find(params[:id])
    #@employees = Employee.driver_expeditors.includes(:person)
  end
  
  
  def get_form
    @from = params[:from]
    @to = params[:to]
    @unv_employee = UnvEmployee.new
    @unv_employee.unv_type_id = 'CE58D7F2-E69A-4F8C-8A36-92E9066B3D5E'
    @employees = Employee.driver_expeditors.includes(:person)
  end
  
  private
  
  
  # Use callbacks to share common setup or constraints between actions.
  def set_vacation
    @vacation = Vacation.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def vacation_params
    params.require(:vacation).permit(:employee_id, :descr, :start_date, :end_date, :is_close)
  end
end
