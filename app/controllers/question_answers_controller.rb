class QuestionAnswersController < ApplicationController
  before_action :set_question_answer, only: [:show, :edit, :update, :destroy]


  def index
    @question_answers = QuestionAnswer.all
  end

  def show
  end

  def new
    @question_answer = QuestionAnswer.new
  end

  def edit
  end

  def create
    @question_answer = QuestionAnswer.new(question_answer_params)

    if @question_answer.save
      redirect_to @question_answer, notice: 'Question answer добавлен'
    else
      render :new
    end
  end

  def update
    if @question_answer.update(question_answer_params)
      redirect_to @question_answer, notice: 'Question answer сохранен'
    else
      render :edit
    end
  end

  def destroy
    @question_answer.destroy
    redirect_to question_answers_url, notice: 'Question answer удален'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_question_answer
    @question_answer = QuestionAnswer.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def question_answer_params
    params.require(:question_answer).permit(:agreement_request_id, :person_id, :dn, :question_date, :answer_date, :answer_text, :is_close, :foreign_id)
  end
end
