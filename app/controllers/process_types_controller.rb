class ProcessTypesController < ApplicationController
  before_action :set_process_type, only: [:show, :edit, :update, :destroy]


  def index
    @process_types = ProcessType.all
  end

  def show
  end

  def new
    @process_type = ProcessType.new
  end

  def edit
  end

  def create
    @process_type = ProcessType.new(process_type_params)

    if @process_type.save
      redirect_to process_types_url, notice: 'Тип процесса добавлен'
    else
      render :new
    end
  end

  def update
    if @process_type.update(process_type_params)
      redirect_to process_types_url, notice: 'Тип процесса сохранен'
    else
      render :edit
    end
  end

  def destroy
    @process_type.destroy
    redirect_to process_types_url, notice: 'Тип процесса удален'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_process_type
    @process_type = ProcessType.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def process_type_params
    params.require(:process_type).permit(:name, :descr, :is_close)
  end
end
