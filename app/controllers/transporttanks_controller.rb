class TransporttanksController < ApplicationController

  def index

      @transporttanks = Transporttank.all#.joins(:transportmodel).order('transportmodels.name')#.paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
    end
  end


  def new
    @transporttank = Transporttank.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end


  def edit
    @transporttank = Transporttank.find(params[:id])
  end


  def create
    @transporttank = Transporttank.new(transporttank_params)
    respond_to do |format|
      if @transporttank.save
        format.html { redirect_to transporttanks_url(model: @transportmodel.id), notice: 'Топливный бак добавлен' }
      else
        format.html { render action: "new" }
      end
    end
  end


  def update
    @transporttank = Transporttank.find(params[:id])
    respond_to do |format|
      if @transporttank.update_attributes(transporttank_params)
        format.html { redirect_to transporttanks_url(model: @transportmodel.id), notice: 'Топливный бак сохранен' }
      else
        format.html { render action: "edit" }
      end
    end
  end


  def destroy
    @transporttank = Transporttank.find(params[:id])
    @transporttank.close

    respond_to do |format|
      format.html { redirect_to transporttanks_url(model: transportmodel ) }
    end
  end


 private

 def transporttank_params
   params.require(:transporttank).permit(:v_tank, :v_tank_min, :transportmodel_id, :is_close)
 end

end
