# -*- encoding : utf-8 -*-


class UnvEmployeesController < ApplicationController


  def index
 #   @unv_employees = UnvEmployee.all
    @unv_employees_count = UnvEmployee.count
    @unv_employees = UnvEmployee.order(:planned_start)#.paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @unv_employees }
    end
  end


  def show
    @unv_employee = UnvEmployee.find(params[:id])

    respond_to do |format|
      format.js { }
    end
  end


  def new
    @unv_employee = UnvEmployee.new
    
    @contractors = Contractor.find(Department.select('distinct contractor_id').map(&:contractor_id))
    @departments = Department.visible

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @unv_employee }
    end
  end


  def edit
    @unv_employee = UnvEmployee.find(params[:id])
    
    @contractors = Contractor.find(Department.select('distinct contractor_id').map(&:contractor_id))
    @departments = Department.visible
  end


  def create
    @unv_employee = UnvEmployee.new(unv_employee_params)



    respond_to do |format|
      if @unv_employee.save
        format.html do 
          redirect_to unv_employees_url, notice: 'Недоступность сотрудника добавлена' 
        end
        format.js do       
          vacation_data
          
          @unv_employee = UnvEmployee.new
          @unv_employee.unv_type_id = 'CE58D7F2-E69A-4F8C-8A36-92E9066B3D5E'
          render 'vacations/save_unv_emp'
        end
      else
        format.html do
          @contractors = Contractor.find(Department.select('distinct contractor_id').map(&:contractor_id))
          @employees = Employee.visible.sort_by(&:person_fullname)
          @departments = Department.visible
          render action: "new" 
        end
        format.js do 
          vacation_data
          @employees = Employee.driver_expeditors
          render 'vacations/error_save_unv_emp'
        end
      end
    end
  end


  def update
    @unv_employee = UnvEmployee.find(params[:id])

    respond_to do |format|
      if @unv_employee.update_attributes(unv_employee_params)
        format.html do 
          redirect_to unv_employees_url, notice: 'Недоступность сотрудника сохранена' 
        end
        format.js do
          vacation_data
          render 'vacations/save_unv_emp'
        end
      else
        format.html do 
          @contractors = Contractor.find(Department.select('distinct contractor_id').map(&:contractor_id))
          @employees = Employee.visible.sort_by(&:person_fullname)
          @departments = Department.visible
          render action: "edit" 
        end
        format.js do 
          vacation_data
          @employees = Employee.driver_expeditors
          render 'vacations/error_save_unv_emp'
        end
      end
    end
  end


  def destroy
    @unv_employee = UnvEmployee.find(params[:id])
    @unv_employee.close

    respond_to do |format|
      format.html { redirect_to unv_employees_url }
      format.js {}
    end
  end


 private

   def vacation_data
    #@employees = Employee.driver_expeditors
    @from = params[:unv_employee][:from]
    @to = params[:unv_employee][:to]
    
    @unv_employees_not_return = UnvEmployee.where("planned_end <= ? and actual_end IS NULL", Time.now).includes(:person) 
    
    @vacations_return = Vacation.drivers.where(:"vacations.end_date" => @from.to_date..@to.to_date).includes(:person) 
    @unv_employees_leave = UnvEmployee.where(planned_start: @from.to_date..@to.to_date).includes(:person) 
     
    @vacations =Vacation.drivers.where("(vacations.start_date <= ? AND vacations.end_date >= ?) OR (vacations.start_date >= ? AND vacations.start_date <= ?) OR (vacations.end_date >= ? AND vacations.end_date <= ?)", @from.to_date, @to.to_date, @from.to_date, @to.to_date,  @from.to_date, @to.to_date).includes(:person)
    @unv_employees = UnvEmployee.where("(planned_start <= ? AND planned_end >= ?) OR (planned_start >= ? AND planned_start <= ?) OR (planned_end <= ? AND planned_end >= ?)", @from.to_date, @to.to_date, @from.to_date, @to.to_date, @from.to_date, @to.to_date).includes(:person)
       
   end 
  
  
 def unv_employee_params
  start_date = params[:unv_employee][:planned_start].to_date
  end_date = params[:unv_employee][:planned_end].to_date
  params[:unv_employee][:planned_start] = DateTime.new(start_date.year, start_date.month, start_date.day, 0, 0, 0) unless params[:unv_employee][:planned_start].blank?
  params[:unv_employee][:planned_end] = DateTime.new(end_date.year, end_date.month, end_date.day, 23, 59, 59) unless params[:unv_employee][:planned_end].blank?
  params.require(:unv_employee).permit(:actual_end, :actual_start, :employee_id, :id, :planned_end, :planned_start, :unv_type_id, :descr)
 end

end

