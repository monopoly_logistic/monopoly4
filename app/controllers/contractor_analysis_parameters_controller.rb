class ContractorAnalysisParametersController < ApplicationController
  before_action :set_contractor_analysis_parameter, only: [:show, :edit, :update, :destroy]


  def index
    @contractor_analysis_parameters = ContractorAnalysisParameter.all
  end

  def show
  end

  def new
    @contractor_analysis_parameter = ContractorAnalysisParameter.new
  end

  def edit
  end

  def create
    @contractor_analysis_parameter = ContractorAnalysisParameter.new(contractor_analysis_parameter_params)

    if @contractor_analysis_parameter.save
      redirect_to @contractor_analysis_parameter, notice: 'Contractor analysis parameter добавлен'
    else
      render :new
    end
  end

  def update
    if @contractor_analysis_parameter.update(contractor_analysis_parameter_params)
      redirect_to @contractor_analysis_parameter, notice: 'Contractor analysis parameter сохранен'
    else
      render :edit
    end
  end

  def destroy
    @contractor_analysis_parameter.destroy
    redirect_to contractor_analysis_parameters_url, notice: 'Contractor analysis parameter удален'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contractor_analysis_parameter
      @contractor_analysis_parameter = ContractorAnalysisParameter.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def contractor_analysis_parameter_params
      params.require(:contractor_analysis_parameter).permit(:name)
    end
end
