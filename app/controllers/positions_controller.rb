# -*- encoding : utf-8 -*-
class PositionsController < ApplicationController


  def index
    @positions = Position.order(:name)#.paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @positions }
    end
  end



  def new
    @position = Position.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @position }
    end
  end


  def edit
    @position = Position.find(params[:id])
  end


  def create
    @position = Position.new(position_params)

    respond_to do |format|
      if @position.save
        format.html { redirect_to positions_url, notice: 'Должность добавлена.' }
        format.json { render json: @position, status: :created, location: @position }
      else
        format.html { render action: "new" }
        format.json { render json: @position.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @position = Position.find(params[:id])

    respond_to do |format|
      if @position.update_attributes(position_params)
        format.html { redirect_to positions_url, notice: 'Должность сохранена.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @position.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @position = Position.find(params[:id])
    @position.close

    respond_to do |format|
      format.html { redirect_to positions_url }
      format.json { head :no_content }
    end
  end


 private

 def position_params
   params.require(:position).permit(:descr, :name, :is_close)
 end


end
