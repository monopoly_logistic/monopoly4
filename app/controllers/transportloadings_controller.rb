class TransportloadingsController < ApplicationController

  def index
    @transportloadings = Transportloading.all#.paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
    end
  end


  def new
    @transportloading = Transportloading.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end


  def edit
    @transportloading = Transportloading.find(params[:id])
  end


  def create
    @transportloading = Transportloading.new(transportloading_params)

    respond_to do |format|
      if @transportloading.save
        format.html { redirect_to transportloadings_url, notice: 'Способ загрузки добавлен' }
      else
        format.html { render action: "new" }
      end
    end
  end


  def update
    @transportloading = Transportloading.find(params[:id])

    respond_to do |format|
      if @transportloading.update_attributes(transportloading_params)
        format.html { redirect_to transportloadings_url, notice: 'Способ загрузки сохранен' }
      else
        format.html { render action: "edit" }
      end
    end
  end


  def destroy
    @transportloading = Transportloading.find(params[:id])
    @transportloading.close

    respond_to do |format|
      format.html { redirect_to transportloadings_url }
    end
  end


 private

 def transportloading_params
   params.require(:transportloading).permit(:descr, :loading, :is_close)
 end

end
