class AgreementAccessesController < ApplicationController
  before_action :set_agreement_access, only: [:edit, :update, :destroy]


  def index
    @agreement_accesses = AgreementAccess.includes(:agreement, staff_unit: [:position, department: :contractor ])
  end



  def new
    @agreement_access = AgreementAccess.new
  end

  def edit
  end

  def create
    @agreement_access = AgreementAccess.new(agreement_access_params)

    if @agreement_access.save
      redirect_to agreement_accesses_url, notice: 'Доступ к согласованию добавлен'
    else
      render :new
    end
  end

  def update
    if @agreement_access.update(agreement_access_params)
      redirect_to agreement_accesses_url, notice: 'Доступ к согласованию сохранен'
    else
      render :edit
    end
  end

  def destroy
    @agreement_access.destroy
    redirect_to agreement_accesses_url, notice: 'Доступ к согласованию удален'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_agreement_access
    @agreement_access = AgreementAccess.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def agreement_access_params
    params.require(:agreement_access).permit(:agreement_id, :staff_unit_id, :is_close, :necessary)
  end
end
