class PayslipsController < ApplicationController
  skip_before_action :authenticate_user!, :only => [:tst]
  def index
    
  end

  def show
    
    @year = params[:date][:year].to_i
    @month = params[:date][:month].to_i
    @num = '000000' + params[:num].strip
    
    @from = Date.new(@year, @month).to_time.to_i
    @to = Date.new(@year, @month + 1).to_time.to_i

    uri = URI.parse("http://esbsrv01.monopoly.su/V82adapter/paySheet.ashx?id=#{@num}&from=#{@from}&to=#{@to}")
    http = Net::HTTP.new(uri.host, uri.port)
    response = http.request(Net::HTTP::Get.new(uri.request_uri))
    @content = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
    unless @content['Result'] == 'Bad'
      @payslip = @content['PaySheet'] 
      @payslip.map!{|u| u.values.map!{|u2| u2.to_s}}
    end
    @name =  @content['FullName'] 
  end
  
  def tst
    if request.post?  
    @year = params[:date][:year].to_i
    @month = params[:date][:month].to_i
    @num = params[:num].strip
    
    @from = Date.new(@year, @month).to_time.to_i
    @to = Date.new(@year, @month + 1).to_time.to_i

    uri = URI.parse("http://esbsrv01.monopoly.su/V82adapter/paySheet.ashx?id=#{@num}&from=#{@from}&to=#{@to}")
    http = Net::HTTP.new(uri.host, uri.port)
    response = http.request(Net::HTTP::Get.new(uri.request_uri))
    @content = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
    
    end
  end
end
