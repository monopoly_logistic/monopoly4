class TimeZonesController < ApplicationController
  before_action :set_time_zone, only: [:edit, :update, :destroy]

  respond_to :html

  def index
    @time_zones = TimeZone.all.order(:area_code)
    respond_with(@time_zones)
  end

  def new
    @time_zone = TimeZone.new
    @area_codes = TimeZone.get_regions
    respond_with(@time_zone)
  end

  def edit
    @area_codes = TimeZone.get_regions
  end

  def create
    @time_zone = TimeZone.new(time_zone_params)
    @time_zone.save
    respond_with(@time_zone) do |format|
      format.html {redirect_to time_zones_url, notice: "Часовой пояс сохранен"}
    end
  end

  def update
    @time_zone.update(time_zone_params)
    respond_with(@time_zone) do |format|
      format.html {redirect_to time_zones_url, notice: "Часовой пояс сохранен"}
    end
  end

  def destroy
    @time_zone.close
    respond_with(@time_zone)
  end

  private
    def set_time_zone
      @time_zone = TimeZone.find(params[:id])
    end

    def time_zone_params
      params.require(:time_zone).permit(:area_code, :utc, :start_date, :end_date, :is_close)
    end
end
