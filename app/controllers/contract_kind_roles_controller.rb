# -*- encoding : utf-8 -*-


class ContractKindRolesController < ApplicationController


  def index
 #   @contract_kind_roles = ContractKindRole.all
    @contract_kind_roles_count = ContractKindRole.visible.count
    @contract_kind_roles = ContractKindRole.try(params[:show_del].blank? ? :visible : :invisible)
        .joins(:contract_kind)
        .order("contract_kinds.name")   
        #.paginate(page: params[:page], per_page: 20)


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contract_kind_roles }
    end
  end


  def show
    @contract_kind_role = ContractKindRole.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @contract_kind_role }
    end
  end


  def new
    @contract_kind_role = ContractKindRole.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contract_kind_role }
    end
  end


  def edit
    @contract_kind_role = ContractKindRole.find(params[:id])
  end


  def create
    @contract_kind_role = ContractKindRole.new(contract_kind_role_params)

    respond_to do |format|
      if @contract_kind_role.save
        format.html { redirect_to contract_kind_roles_url, notice: 'Роль предмета договора добавлена' }
        format.json { render json: @contract_kind_role, status: :created, location: @contract_kind_role }
      else
        format.html { render action: "new" }
        format.json { render json: @contract_kind_role.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @contract_kind_role = ContractKindRole.find(params[:id])

    respond_to do |format|
      if @contract_kind_role.update_attributes(contract_kind_role_params)
        format.html { redirect_to contract_kind_roles_url, notice: 'Роль предмета договора сохранена' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contract_kind_role.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @contract_kind_role = ContractKindRole.find(params[:id])
    @contract_kind_role.close
    #@contract_kind_role.destroy

    respond_to do |format|
      format.html { redirect_to contract_kind_roles_url }
      format.json { head :no_content }
    end
  end


 private

 def contract_kind_role_params
   params.require(:contract_kind_role).permit(:contract_kind_id, :descr, :id, :is_close, :name)
 end

end

