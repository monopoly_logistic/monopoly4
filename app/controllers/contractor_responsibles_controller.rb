class ContractorResponsiblesController < ApplicationController
  before_action :set_contractor_responsible, only: [:edit, :update, :destroy, :unchain_responsible]


  def index
    #@contractor_responsibles = ContractorResponsible.all
    @contractors = Contractor.visible.includes(:chained_contractor_responsibles, chained_contractor_responsibles: {employee: :person})
    @contractors_no_responsibles = Contractor.no_responsibles 
  end
  
  def show_responsibles
    @contractor = Contractor.find(params[:id])
    @contractor_responsibles = @contractor.contractor_responsibles
  end
  
  
  def unchain_responsible

    @contractor_responsible.unchain
    index 
    @contractor = @contractor_responsible.contractor
    @contractor_responsibles = @contractor.contractor_responsibles

    respond_to do |format|
      format.js do 
        flash[:notice] = 'Ответственный деактивирован'
        render :index
      end
    end
  end

  def new
    @contractor_responsible = ContractorResponsible.new
    @contractor = Contractor.find params[:contractor] unless params[:contractor].blank?
    @contractor_responsible.contractor_id = params[:contractor] unless params[:contractor].blank?
  end

  def edit
    @contractor = @contractor_responsible.contractor
  end

  def create
    @contractor_responsible = ContractorResponsible.new(contractor_responsible_params)
    @contractor = @contractor_responsible.contractor
    @contractor_responsibles = @contractor.contractor_responsibles
    if @contractor_responsible.save
      index
      ContractorResponsible.rebuild!
      flash.now[:notice] = "Сохранено"
      render :index
    else
      flash.now[:notice] = "Ошибка сохранения"
      render :new
    end
  end

  def update
    @contractor = @contractor_responsible.contractor
    @contractor_responsibles = @contractor.contractor_responsibles
    index
    if @contractor_responsible.update(contractor_responsible_params)
      ContractorResponsible.rebuild!
      flash.now[:notice] = "Сохранено"
      render :index
    else
      render :edit
    end  
  end

  def destroy
    @contractor_responsible.close
    @contractor = @contractor_responsible.contractor
    redirect_to contractor_responsibles_url, notice: 'Ответственный за контрагента удален'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contractor_responsible
      @contractor_responsible = ContractorResponsible.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def contractor_responsible_params
      start_date = params[:contractor_responsible][:start_date].to_date
      end_date = params[:contractor_responsible][:end_date].to_date
      params[:contractor_responsible][:start_date] = DateTime.new(start_date.year, start_date.month, start_date.day, 0, 0, 0) unless params[:contractor_responsible][:start_date].blank?
      params[:contractor_responsible][:end_date] = DateTime.new(end_date.year, end_date.month, end_date.day, 23, 59, 59) unless params[:contractor_responsible][:end_date].blank?
      params[:contractor_responsible][:id] ||= SecureRandom.uuid
      params[:contractor_responsible][:parent_id] = nil if params[:contractor_responsible][:parent_id].blank?
      params.require(:contractor_responsible).permit(:contractor_id, :employee_id, :parent_id, :start_date, :end_date, :is_close, :id)
    end
end
