class Uploading < Object
  attr_accessor :logist_name, :truck, :city_release, :current_client_route, :current_status, :release_date, :release_time, :logist_comment, :manager_name,
                :tariff, :mileage, :rr_km, :manager_comment, :logist_comment_link, :manager_comment_link
              
  def initialize args
    args.each do |k,v|
      instance_variable_set("@#{k}", v) #unless v.nil?
    end
  end
  
  def attr_values
    self.instance_values.values
  end
  
  def attr_keys
    self.instance_values.keys
  end
  
end 

class Connection < Object
  attr_accessor :order_from_key, :order_to_key, :truck_num, :logist, :previous_entry, :previous_entry_city, :previous_entry_unloading_time,
                :previous_entry_actual_time, :unv, :unv_link, :conn_time, :next_entry, :next_entry_city, :manager, :contractor, 
                :s_time_1op_next_entry, :next_entry_mileage, :is_limit_exc, :next_entry_created_at, :is_checked
              
#  def initialize args
#    args.each do |k,v|
#      instance_variable_set("@#{k}", v) #unless v.nil?
#    end
#  end
  
  def attr_values
    self.instance_values.values
  end
  
  def attr_keys
    self.instance_values.keys
  end
  
end 

class Booking_data < Object
  attr_accessor :entry_num, :logist_name, :truck_num, :region_release, :city_release, :contractor_previous_entry, :current_route_entry, 
                :current_status, :current_operation, :logist_comment, :release_date, :release_time, :coord_last_comment, :next_entry_num, 
                :manager_name, :contractor_next_entry, :region_loading, :next_route_entry, :loading_date, :loading_time, :tariff, :mileage, 
                :rr_km, :truck_key, :date_finish, :trailer_key, :driver_key, :booking_id, :trailer_name, :driver_name, :p, :pn, :d, :r, :pv, :dr, 
                :order_key, :date_end_right, :utc_region_loading, :utc_region_release, :can_edit_chars, :can_select_truck, :can_create_booking,  
                :can_edit_booking, :expensive, :color_release_date, :can_empty_mileage, :part, :citykey_release
              
#  def initialize args
#    args.each do |k,v|
#      instance_variable_set("@#{k}", v) #unless v.nil?
#    end
#  end
  
  def attr_values
    self.instance_values.values
  end
  
  def attr_keys
    self.instance_values.keys
  end
  
  def find_booking_truck
    self.truck_key.blank? ? false : (Booking.visible.where(truck_id: self.truck_key.downcase).limit(1).first || false)
  end

  
end 

