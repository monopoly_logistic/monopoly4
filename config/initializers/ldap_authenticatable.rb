# -*- encoding : utf-8 -*-
require 'net/ldap'
require 'devise/strategies/authenticatable'

module Devise
  module Strategies
    class LdapAuthenticatable < Authenticatable
      def authenticate!
        if params[:user]
          params[:user][:email] = email
          if ldap_init.bind
            @user = User.where('email = ?', email).first
            unless @user
              @user = User.create(user_data)
              @role = Role.find('cbfcb992-0685-4530-ae3f-179263d9935d'.upcase)
              @user.roles << @role
              #File.open('log/report.ldap', 'a'){ |file| file.write @role.id; file.write "\n" }
            else
              @user.update_attributes(user_data)
            end
            
            success!(@user)
            #File.open('log/report.ldap', 'a'){ |file| file.write Time.now; file.write email; file.write " - "; file.write login;  file.write "\n" }
          else
            fail(:invalid_login)
          end
          params[:user][:email] = ''
          params[:user][:password] = ''
        end
      end
      
      
      def ldap_init
        @host = 'dc01.monopoly.su'
        @host2 = 'dc02.monopoly.su'
        @port = 389
        @email = email
        @login = login
        @password = password

        ldap = Net::LDAP.new
          ldap.host = @host
          ldap.port = @port
          ldap.auth @email, @password
        ldap.host = @host2 unless ldap.bind  
        ldap
      end

      def email
        (params[:user][:email].index('@monopoly.su') ? params[:user][:email] : params[:user][:email] + '@monopoly.su').force_encoding(Encoding::UTF_8)
      end

      def login
        (params[:user][:email].index('@monopoly.su') ? params[:user][:email][0..params[:user][:email].index('@monopoly.su')-1] : params[:user][:email]).force_encoding(Encoding::UTF_8)
      end

      def password
        params[:user][:password]
      end

      def user_data
        name = ldap_search.givenname.first.force_encoding(Encoding::UTF_8)
        last_name = ldap_search.sn.first.force_encoding(Encoding::UTF_8)
        {email: email, password: password, password_confirmation: password, name: name, last_name: last_name}
      end

      def ldap_search
        ldap_init.search(
        base:         "DC=monopoly,DC=su",
        #filter:       Net::LDAP::Filter.eq( "mail", 'mihail.rogov@monopoly.su' ),
        filter:      "samaccountname=#{login[0..19]}",
        return_result: true
        ).first
      end

    end
  end
end

Warden::Strategies.add(:ldap_authenticatable, Devise::Strategies::LdapAuthenticatable)
