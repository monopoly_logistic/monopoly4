#module ActiveRecord
#  module ConnectionAdapters
#    module Sqlserver
#      module DatabaseStatements
#     
#
#        protected
#
#        def sql_for_insert(sql, pk, id_value, sequence_name, binds)
##          sql =
##            if pk
##              sql.insert(sql.index(/ (DEFAULT )?VALUES/), " OUTPUT inserted.#{pk}")
##              #"#{sql}; SELECT @@IDENTITY"
##            else
##              "#{sql}; SELECT CAST(SCOPE_IDENTITY() AS bigint) AS Ident"
##            end
#          super
#        end
#
#      end
#    end
#  end
#end


module ActiveRecord
  module ConnectionAdapters
    module SQLServer
      module DatabaseStatements

#        def sql_for_insert(sql, pk, id_value, sequence_name, binds)
#          ["#{sql}; SELECT CAST(SCOPE_IDENTITY() AS bigint) AS Ident", binds]
#        end
        def sql_for_insert(sql, pk, id_value, sequence_name, binds)
          sql = if pk && self.class.use_output_inserted
            quoted_pk = SQLServer::Utils.extract_identifiers(pk).quoted
#            sql.insert sql.index(/ (DEFAULT )?VALUES/), " OUTPUT INSERTED.#{quoted_pk}"
            sql
          else
            "#{sql}; SELECT CAST(SCOPE_IDENTITY() AS bigint) AS Ident"
          end
          super
        end
      end
    end
  end
end
