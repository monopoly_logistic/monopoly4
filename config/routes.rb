Rails.application.routes.draw do
  
  
  post 'status_ts_ol/create_comment_ol'
  get 'status_ts_ol/index'
  delete 'status_ts_ol/index'

  get 'status_ts/index'
  delete 'status_ts/index'
  get 'status_ts/show_filter_and_search'

  get 'proposal/cancels'
  post 'proposal/cancels'
  delete 'proposal/cancels'
  get 'proposal/works_dt'
  get 'proposal/works'
  get 'proposal/set_work'
  get 'proposal/job_requests'
  post 'proposal/set_work'
  delete 'proposal/set_work'
  get 'proposal/work_locations'
  get 'proposal/index'
  get 'proposal/show_entry_cancels'
  get 'proposal/show_work_cancels'
  get 'proposal/entries'
  get 'proposal/add_city_to_entry'
  get 'proposal/set_entry'
  post 'proposal/set_entry'
  delete 'proposal/set_entry'
  get 'proposal/set_reason'
  get 'proposal/add_cancel_to_reason'
  post 'proposal/set_reason'
  delete 'proposal/set_reason'
  get 'proposal/set_deps_reason'
  post 'proposal/set_deps_reason'
  delete 'proposal/set_deps_reason'

  get 'tariff_calculator/all_tariffs'
  post 'tariff_calculator/all_tariffs'
  get 'tariff_calculator/get_cities'
  get 'tariff_calculator/market'
  post 'tariff_calculator/market_get_tariff_grid'
  get 'tariff_calculator/edit_current_market_tariff'
  post 'tariff_calculator/save_current_market_tariff'
  delete 'tariff_calculator/destroy_current_market_tariff'
  get 'tariff_calculator/average'
  get 'tariff_calculator/average_active_regions'
  get 'tariff_calculator/average_active_cities'
  get 'tariff_calculator/set_seasons'
  post 'tariff_calculator/set_seasons'
  get 'tariff_calculator/zero'

  get 'truck_responsibles/index'
  get 'truck_responsibles/show_responsibles'
  get 'truck_responsibles/delete_responsible'
  get 'truck_responsibles/new_or_edit'
  post 'truck_responsibles/create_or_update'

  resources :modulators, except: [:show] do
    collection do
      post 'set_date'
    end
  end

  resources :extra_spendings do
    collection do
      get 'get_truck_and_payments'
      get 'payments'
      post 'payments'
      post 'set_date'
      get 'set_date'
    end
  end

  resources :extra_spending_types, except: [:show]

  resources :time_zones

  get 'reports/ts_spb'
  get 'reports/operation_log'

  post 'save_reports_xls/index'

  resources :gps_nav_transports do
    member do
      get 'show_gps_nav'
      get 'unchain_gps_nav'
    end
  end

  resources :transportmodel1cs

  resources :resource_characteristic_values

  resources :vacations do
    collection do
      get 'index'
      get 'get_form'
      post 'index'
    end
  end

  resources :booking_histories

  resources :contacts, except: [:show]

  resources :contact_types, except: [:show]

  resources :bookings do
    collection do
      get 'get_bookings'
      get 'transport_planning'
      get 'open_orders'
      get 'planned'
      get 'characteristic_val'
      get 'save_param'
      delete 'save_param'
      get 'show_filter_and_search'
      get 'select_filter'
      get 'save_xls'
      get 'select_truck'
      get 'empty_mileage'
      get 'get_city_locations'
      post 'save_empty_mileage'
      post 'save_truck'
      get 'get_column_values'
    end  
  end

  resources :booking_types

  resources :contractor_responsibles, except: [:show] do
    member do
      get 'show_responsibles'
      get 'unchain_responsible'
    end
  end

  resources :properties

  resources :monitoring_connections, only: [:index] do
    collection do
      get 'save_param'      
      delete 'save_param'
      get 'select_filter'
      post 'index'
      post 'create_comment'
      post 'agree'
      post 'quick_agree'
      delete 'quick_disagree'      
      delete 'disagree'
    end
  end

  
  get 'monitoring_resource_unavailable/index', as: :monitoring_resource_unavailable
  post 'monitoring_resource_unavailable/processes'
  get 'monitoring_resource_unavailable/processes'
  get 'monitoring_resource_unavailable/save_xls'

  get 'monitoring_fuel_stations/index', as: :monitoring_fuel_stations
  post 'monitoring_fuel_stations/index'
  get 'monitoring_fuel_stations/save_xls'
  post 'monitoring_fuel_stations/save_reports_xls'
  post 'monitoring_fuel_stations/refuellings'
  get 'monitoring_fuel_stations/info_by_azs_name'
  get 'monitoring_fuel_stations/expensive_refuellings'
  post 'monitoring_fuel_stations/expensive_refuellings'

  get 'uploadings/index'
  post 'uploadings/index'
  get 'uploadings/save_param'
  get 'uploadings/show_filter_and_search'

  resources :stream, only: [:uploading]


  get 'contractor_reports/save_xls'
  get 'contractor_reports/report_info'
  get 'contractor_reports/report'
  get 'contractor_reports/index'
  post 'contractor_reports/show'

  resources :contractor_analysis_parameter_changes

  resources :contractor_analysis_parameters

  resources :truck_comments

  get 'api/prm_questions'
  get 'api/show_question'
  get 'api/show_prm_question'
  post 'api/reply_prm'
  get 'api/reply'
  post 'api/send_answer'
  post 'api/send_open_answer'
  post 'api/get_azs'
  post 'api/set_azs'

  get 'payslips/index'
  post 'payslips/show'
  #post 'payslips/tst'
  #get 'payslips/tst'


  resources :question_answers

  resources :agreement_requests do
    collection do
      post 'index'
      get 'index'
    end
  end

  resources :agreement_accesses, except: [:show]

  resources :agreements, except: [:show]

  resources :process_statuses

  resources :task_statuses

  resources :task_action_accesses

  resources :process_action_accesses

  resources :template_steps

  resources :process_actions

  resources :task_actions

  resources :process_templates

  resources :process_types

  resources :citizenships

  resources :task_templates

  resources :task_types

  resources :contract_relationships

  resources :contracts

  resources :contract_kind_roles

  resources :contract_subkinds

  resources :contract_kinds

  resources :contract_types

  resources :contractor_representatives

  resources :unv_comments


  resources :truck_drivers, except: [:show] do
    collection do
      post 'index'
    end
  end


  resources :couplers do
    member do
      get 'unchain'
    end
  end


  resources :unv_employees

  resources :resourse_types

  resources :unv_types

  resources :employee_histories


  resources :employees
  post "employees/index"

  resources :staff_units


  resources :departments do

  end


  get "unavailables/index" => "unavailables#index"
  get "unavailables/ol" => "unavailables#ol"
  get "unavailables/connections" => "unavailables#connections"
  post "unavailables/index" => "unavailables#index"
  post "unavailables/ol" => "unavailables#ol"
  post "unavailables/connections" => "unavailables#connections"
  post "unavailables/save_xls" => "unavailables#save_xls"
  post "unavailables/show_filter_and_search" => "unavailables#show_filter_and_search"
  post "unavailables/create_comment" => "unavailables#create_comment"
  post "unavailables/create_comment_ol" => "unavailables#create_comment_ol"
  post "unavailables/agree" => "unavailables#agree"
  post "unavailables/quick_agree" => "unavailables#quick_agree"
  delete "unavailables/disagree" => "unavailables#disagree"


  resources :duties do
    collection do
      get 'add_all'
    end  
  end


  #get "wialon/index"

  resources :tables

  resources :columns

  resources :objecttypepropertycolumns

  resources :objecttypeproperties

  resources :objecttypes

  resources :systems

  resources :phoneuses

  resources :phones

  resources :positions

  resources :contractortypes

  resources :contractors


  resources :documents do
    collection do
      get 'show_part'
      post 'select_kind'
      post "new_from"
      get 'select_kind'
      get 'edit'
      delete 'del_file'
    end
  end
  post "documents/select_transport/:id" => 'documents#select_transport'


  resources :documentkinds

  resources :documenttypes


  resources :functionalgroups do
    collection do
      post 'savesort'
      get 'savesort'
      get 'update_menu'
    end
  end


  resources :people do
    collection do
      post 'phone_person'
      get 'new_person'
      get 'phone_person'
      get 'drivers'
    end
  end


  resources :transportexploitations

  resources :transporttanks

  resources :transportloadings

  resources :transportecologicals

  resources :enginemodels

  resources :enginetypes

  resources :transportkinds

  resources :transportcolors

  resources :transporttypes


  resources :transports do
    collection do
      get 'groupings'
      post 'create_and_update_coupler'
      patch 'create_and_update_coupler'
      post 'create_and_update_driver'
      patch 'create_and_update_driver'
      get 'get_transportkind'
    end
    member do
      get 'show_couplers_and_drivers'
      get 'show_couplers'
      get 'show_drivers'
      get 'new_and_edit_coupler'
      get 'unchain_coupler'
      get 'new_and_edit_driver'
      get 'unchain_driver'
    end
  end
  post "transports/select_brand/:id" => "transports#select_brand"
  post "transports/select_tank/:id" => "transports#select_tank"
  post "transports/select_color/:id" => "transports#select_color"
  post "transports/select_enginemodel/:id" => "transports#select_enginemodel"
  post "transports/select_exploitation/:id" => "transports#select_exploitation"



  get "sms/index"
  post "sms/send_sms"

  resources :testguids, as: 'tst'


  resources :temps do
    collection do
      get 'strm'
    end

  end

  resources :roles
  post "roles/select_functional/:id" => "roles#select_functional"

  get "main/index"

  root :to => 'main#index'
  get '/' => 'main#index'


  devise_for :users, :skip => [:sessions]
  as :user do
    get 'signin' => 'devise/sessions#new', :as => :new_user_session
    post 'signin' => 'devise/sessions#create', :as => :user_session
    delete 'signout' => 'devise/sessions#destroy', :as => :destroy_user_session
  end

  post 'users/select_role/:id' => "users#select_role"
  post 'users/select_f_group/:id' => "users#select_f_group"



  scope "/admin" do
    resources :users, :usergroups, :roles, :functionals
  end

    resources :transportmodels do
      collection do
        get 'add_tank'
        get 'add_loading'
        get 'chng_attr'
      end
    end
    post "transportmodels/select_loading/:id" => 'transportmodels#select_loading'
    
  scope "/transport" do
    resources :transportbrands, except: [:show]
    resources :transportcategories
  end

  
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  
  
    get ':controller(/:action(/:id))(.:format)'
  
end
