require 'rails_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

RSpec.describe TimeZonesController, :type => :controller do

  # This should return the minimal set of attributes required to create a valid
  # TimeZone. As you add validations to TimeZone, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    skip("Add a hash of attributes valid for your model")
  }

  let(:invalid_attributes) {
    skip("Add a hash of attributes invalid for your model")
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # TimeZonesController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET index" do
    it "assigns all time_zones as @time_zones" do
      time_zone = TimeZone.create! valid_attributes
      get :index, {}, valid_session
      expect(assigns(:time_zones)).to eq([time_zone])
    end
  end

  describe "GET show" do
    it "assigns the requested time_zone as @time_zone" do
      time_zone = TimeZone.create! valid_attributes
      get :show, {:id => time_zone.to_param}, valid_session
      expect(assigns(:time_zone)).to eq(time_zone)
    end
  end

  describe "GET new" do
    it "assigns a new time_zone as @time_zone" do
      get :new, {}, valid_session
      expect(assigns(:time_zone)).to be_a_new(TimeZone)
    end
  end

  describe "GET edit" do
    it "assigns the requested time_zone as @time_zone" do
      time_zone = TimeZone.create! valid_attributes
      get :edit, {:id => time_zone.to_param}, valid_session
      expect(assigns(:time_zone)).to eq(time_zone)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new TimeZone" do
        expect {
          post :create, {:time_zone => valid_attributes}, valid_session
        }.to change(TimeZone, :count).by(1)
      end

      it "assigns a newly created time_zone as @time_zone" do
        post :create, {:time_zone => valid_attributes}, valid_session
        expect(assigns(:time_zone)).to be_a(TimeZone)
        expect(assigns(:time_zone)).to be_persisted
      end

      it "redirects to the created time_zone" do
        post :create, {:time_zone => valid_attributes}, valid_session
        expect(response).to redirect_to(TimeZone.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved time_zone as @time_zone" do
        post :create, {:time_zone => invalid_attributes}, valid_session
        expect(assigns(:time_zone)).to be_a_new(TimeZone)
      end

      it "re-renders the 'new' template" do
        post :create, {:time_zone => invalid_attributes}, valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested time_zone" do
        time_zone = TimeZone.create! valid_attributes
        put :update, {:id => time_zone.to_param, :time_zone => new_attributes}, valid_session
        time_zone.reload
        skip("Add assertions for updated state")
      end

      it "assigns the requested time_zone as @time_zone" do
        time_zone = TimeZone.create! valid_attributes
        put :update, {:id => time_zone.to_param, :time_zone => valid_attributes}, valid_session
        expect(assigns(:time_zone)).to eq(time_zone)
      end

      it "redirects to the time_zone" do
        time_zone = TimeZone.create! valid_attributes
        put :update, {:id => time_zone.to_param, :time_zone => valid_attributes}, valid_session
        expect(response).to redirect_to(time_zone)
      end
    end

    describe "with invalid params" do
      it "assigns the time_zone as @time_zone" do
        time_zone = TimeZone.create! valid_attributes
        put :update, {:id => time_zone.to_param, :time_zone => invalid_attributes}, valid_session
        expect(assigns(:time_zone)).to eq(time_zone)
      end

      it "re-renders the 'edit' template" do
        time_zone = TimeZone.create! valid_attributes
        put :update, {:id => time_zone.to_param, :time_zone => invalid_attributes}, valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested time_zone" do
      time_zone = TimeZone.create! valid_attributes
      expect {
        delete :destroy, {:id => time_zone.to_param}, valid_session
      }.to change(TimeZone, :count).by(-1)
    end

    it "redirects to the time_zones list" do
      time_zone = TimeZone.create! valid_attributes
      delete :destroy, {:id => time_zone.to_param}, valid_session
      expect(response).to redirect_to(time_zones_url)
    end
  end

end
