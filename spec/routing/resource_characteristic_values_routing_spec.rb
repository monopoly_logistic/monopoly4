require "rails_helper"

RSpec.describe ResourceCharacteristicValuesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/resource_characteristic_values").to route_to("resource_characteristic_values#index")
    end

    it "routes to #new" do
      expect(:get => "/resource_characteristic_values/new").to route_to("resource_characteristic_values#new")
    end

    it "routes to #show" do
      expect(:get => "/resource_characteristic_values/1").to route_to("resource_characteristic_values#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/resource_characteristic_values/1/edit").to route_to("resource_characteristic_values#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/resource_characteristic_values").to route_to("resource_characteristic_values#create")
    end

    it "routes to #update" do
      expect(:put => "/resource_characteristic_values/1").to route_to("resource_characteristic_values#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/resource_characteristic_values/1").to route_to("resource_characteristic_values#destroy", :id => "1")
    end

  end
end
