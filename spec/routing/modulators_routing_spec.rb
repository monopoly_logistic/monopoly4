require "rails_helper"

RSpec.describe ModulatorsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/modulators").to route_to("modulators#index")
    end

    it "routes to #new" do
      expect(:get => "/modulators/new").to route_to("modulators#new")
    end

    it "routes to #show" do
      expect(:get => "/modulators/1").to route_to("modulators#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/modulators/1/edit").to route_to("modulators#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/modulators").to route_to("modulators#create")
    end

    it "routes to #update" do
      expect(:put => "/modulators/1").to route_to("modulators#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/modulators/1").to route_to("modulators#destroy", :id => "1")
    end

  end
end
