require "rails_helper"

RSpec.describe Transportmodel1csController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/transportmodel1cs").to route_to("transportmodel1cs#index")
    end

    it "routes to #new" do
      expect(:get => "/transportmodel1cs/new").to route_to("transportmodel1cs#new")
    end

    it "routes to #show" do
      expect(:get => "/transportmodel1cs/1").to route_to("transportmodel1cs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/transportmodel1cs/1/edit").to route_to("transportmodel1cs#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/transportmodel1cs").to route_to("transportmodel1cs#create")
    end

    it "routes to #update" do
      expect(:put => "/transportmodel1cs/1").to route_to("transportmodel1cs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/transportmodel1cs/1").to route_to("transportmodel1cs#destroy", :id => "1")
    end

  end
end
