require "rails_helper"

RSpec.describe ExtraSpendingsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/extra_spendings").to route_to("extra_spendings#index")
    end

    it "routes to #new" do
      expect(:get => "/extra_spendings/new").to route_to("extra_spendings#new")
    end

    it "routes to #show" do
      expect(:get => "/extra_spendings/1").to route_to("extra_spendings#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/extra_spendings/1/edit").to route_to("extra_spendings#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/extra_spendings").to route_to("extra_spendings#create")
    end

    it "routes to #update" do
      expect(:put => "/extra_spendings/1").to route_to("extra_spendings#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/extra_spendings/1").to route_to("extra_spendings#destroy", :id => "1")
    end

  end
end
