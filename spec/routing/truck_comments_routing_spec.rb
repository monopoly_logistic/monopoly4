require "rails_helper"

RSpec.describe TruckCommentsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/truck_comments").to route_to("truck_comments#index")
    end

    it "routes to #new" do
      expect(:get => "/truck_comments/new").to route_to("truck_comments#new")
    end

    it "routes to #show" do
      expect(:get => "/truck_comments/1").to route_to("truck_comments#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/truck_comments/1/edit").to route_to("truck_comments#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/truck_comments").to route_to("truck_comments#create")
    end

    it "routes to #update" do
      expect(:put => "/truck_comments/1").to route_to("truck_comments#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/truck_comments/1").to route_to("truck_comments#destroy", :id => "1")
    end

  end
end
