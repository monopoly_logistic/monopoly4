require "rails_helper"

RSpec.describe ContractorResponsiblesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/contractor_responsibles").to route_to("contractor_responsibles#index")
    end

    it "routes to #new" do
      expect(:get => "/contractor_responsibles/new").to route_to("contractor_responsibles#new")
    end

    it "routes to #show" do
      expect(:get => "/contractor_responsibles/1").to route_to("contractor_responsibles#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/contractor_responsibles/1/edit").to route_to("contractor_responsibles#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/contractor_responsibles").to route_to("contractor_responsibles#create")
    end

    it "routes to #update" do
      expect(:put => "/contractor_responsibles/1").to route_to("contractor_responsibles#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/contractor_responsibles/1").to route_to("contractor_responsibles#destroy", :id => "1")
    end

  end
end
