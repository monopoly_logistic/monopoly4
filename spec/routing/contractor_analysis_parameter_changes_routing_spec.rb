require "rails_helper"

RSpec.describe ContractorAnalysisParameterChangesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/contractor_analysis_parameter_changes").to route_to("contractor_analysis_parameter_changes#index")
    end

    it "routes to #new" do
      expect(:get => "/contractor_analysis_parameter_changes/new").to route_to("contractor_analysis_parameter_changes#new")
    end

    it "routes to #show" do
      expect(:get => "/contractor_analysis_parameter_changes/1").to route_to("contractor_analysis_parameter_changes#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/contractor_analysis_parameter_changes/1/edit").to route_to("contractor_analysis_parameter_changes#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/contractor_analysis_parameter_changes").to route_to("contractor_analysis_parameter_changes#create")
    end

    it "routes to #update" do
      expect(:put => "/contractor_analysis_parameter_changes/1").to route_to("contractor_analysis_parameter_changes#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/contractor_analysis_parameter_changes/1").to route_to("contractor_analysis_parameter_changes#destroy", :id => "1")
    end

  end
end
