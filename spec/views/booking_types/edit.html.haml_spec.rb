require 'rails_helper'

RSpec.describe "booking_types/edit", :type => :view do
  before(:each) do
    @booking_type = assign(:booking_type, BookingType.create!(
      :name => "MyString",
      :release_limit => 1,
      :is_close => false
    ))
  end

  it "renders the edit booking_type form" do
    render

    assert_select "form[action=?][method=?]", booking_type_path(@booking_type), "post" do

      assert_select "input#booking_type_name[name=?]", "booking_type[name]"

      assert_select "input#booking_type_release_limit[name=?]", "booking_type[release_limit]"

      assert_select "input#booking_type_is_close[name=?]", "booking_type[is_close]"
    end
  end
end
