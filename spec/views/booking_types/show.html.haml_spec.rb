require 'rails_helper'

RSpec.describe "booking_types/show", :type => :view do
  before(:each) do
    @booking_type = assign(:booking_type, BookingType.create!(
      :name => "Name",
      :release_limit => 1,
      :is_close => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/false/)
  end
end
