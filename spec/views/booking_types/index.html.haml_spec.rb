require 'rails_helper'

RSpec.describe "booking_types/index", :type => :view do
  before(:each) do
    assign(:booking_types, [
      BookingType.create!(
        :name => "Name",
        :release_limit => 1,
        :is_close => false
      ),
      BookingType.create!(
        :name => "Name",
        :release_limit => 1,
        :is_close => false
      )
    ])
  end

  it "renders a list of booking_types" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
