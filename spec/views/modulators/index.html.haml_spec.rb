require 'rails_helper'

RSpec.describe "modulators/index", :type => :view do
  before(:each) do
    assign(:modulators, [
      Modulator.create!(
        :contractor_id => "Contractor",
        :traffic => "9.99",
        :is_close => false
      ),
      Modulator.create!(
        :contractor_id => "Contractor",
        :traffic => "9.99",
        :is_close => false
      )
    ])
  end

  it "renders a list of modulators" do
    render
    assert_select "tr>td", :text => "Contractor".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
