require 'rails_helper'

RSpec.describe "modulators/show", :type => :view do
  before(:each) do
    @modulator = assign(:modulator, Modulator.create!(
      :contractor_id => "Contractor",
      :traffic => "9.99",
      :is_close => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Contractor/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/false/)
  end
end
