require 'rails_helper'

RSpec.describe "gps_nav_transports/show", :type => :view do
  before(:each) do
    @gps_nav_transport = assign(:gps_nav_transport, GpsNavTransport.create!(
      :transport_id => "Transport",
      :gps_nav_id => "Gps Nav",
      :user_id => "User"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Transport/)
    expect(rendered).to match(/Gps Nav/)
    expect(rendered).to match(/User/)
  end
end
