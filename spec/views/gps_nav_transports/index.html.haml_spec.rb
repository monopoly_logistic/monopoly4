require 'rails_helper'

RSpec.describe "gps_nav_transports/index", :type => :view do
  before(:each) do
    assign(:gps_nav_transports, [
      GpsNavTransport.create!(
        :transport_id => "Transport",
        :gps_nav_id => "Gps Nav",
        :user_id => "User"
      ),
      GpsNavTransport.create!(
        :transport_id => "Transport",
        :gps_nav_id => "Gps Nav",
        :user_id => "User"
      )
    ])
  end

  it "renders a list of gps_nav_transports" do
    render
    assert_select "tr>td", :text => "Transport".to_s, :count => 2
    assert_select "tr>td", :text => "Gps Nav".to_s, :count => 2
    assert_select "tr>td", :text => "User".to_s, :count => 2
  end
end
