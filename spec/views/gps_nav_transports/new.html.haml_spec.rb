require 'rails_helper'

RSpec.describe "gps_nav_transports/new", :type => :view do
  before(:each) do
    assign(:gps_nav_transport, GpsNavTransport.new(
      :transport_id => "MyString",
      :gps_nav_id => "MyString",
      :user_id => "MyString"
    ))
  end

  it "renders new gps_nav_transport form" do
    render

    assert_select "form[action=?][method=?]", gps_nav_transports_path, "post" do

      assert_select "input#gps_nav_transport_transport_id[name=?]", "gps_nav_transport[transport_id]"

      assert_select "input#gps_nav_transport_gps_nav_id[name=?]", "gps_nav_transport[gps_nav_id]"

      assert_select "input#gps_nav_transport_user_id[name=?]", "gps_nav_transport[user_id]"
    end
  end
end
