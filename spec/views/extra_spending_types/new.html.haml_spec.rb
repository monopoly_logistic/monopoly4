require 'rails_helper'

RSpec.describe "extra_spending_types/new", :type => :view do
  before(:each) do
    assign(:extra_spending_type, ExtraSpendingType.new(
      :name => "MyString",
      :is_close => ""
    ))
  end

  it "renders new extra_spending_type form" do
    render

    assert_select "form[action=?][method=?]", extra_spending_types_path, "post" do

      assert_select "input#extra_spending_type_name[name=?]", "extra_spending_type[name]"

      assert_select "input#extra_spending_type_is_close[name=?]", "extra_spending_type[is_close]"
    end
  end
end
