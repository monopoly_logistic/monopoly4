require 'rails_helper'

RSpec.describe "properties/show", :type => :view do
  before(:each) do
    @property = assign(:property, Property.create!(
      :system_id => "System",
      :objecttype_id => "Objecttype",
      :is_close => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/System/)
    expect(rendered).to match(/Objecttype/)
    expect(rendered).to match(/false/)
  end
end
