require 'rails_helper'

RSpec.describe "properties/index", :type => :view do
  before(:each) do
    assign(:properties, [
      Property.create!(
        :system_id => "System",
        :objecttype_id => "Objecttype",
        :is_close => false
      ),
      Property.create!(
        :system_id => "System",
        :objecttype_id => "Objecttype",
        :is_close => false
      )
    ])
  end

  it "renders a list of properties" do
    render
    assert_select "tr>td", :text => "System".to_s, :count => 2
    assert_select "tr>td", :text => "Objecttype".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
