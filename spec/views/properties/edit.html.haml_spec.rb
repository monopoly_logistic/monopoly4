require 'rails_helper'

RSpec.describe "properties/edit", :type => :view do
  before(:each) do
    @property = assign(:property, Property.create!(
      :system_id => "MyString",
      :objecttype_id => "MyString",
      :is_close => false
    ))
  end

  it "renders the edit property form" do
    render

    assert_select "form[action=?][method=?]", property_path(@property), "post" do

      assert_select "input#property_system_id[name=?]", "property[system_id]"

      assert_select "input#property_objecttype_id[name=?]", "property[objecttype_id]"

      assert_select "input#property_is_close[name=?]", "property[is_close]"
    end
  end
end
