require 'rails_helper'

RSpec.describe "booking_histories/show", :type => :view do
  before(:each) do
    @booking_history = assign(:booking_history, BookingHistory.create!(
      :booking_id => "Booking",
      :status => false,
      :status_user_id => "Status User",
      :is_close => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Booking/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/Status User/)
    expect(rendered).to match(/false/)
  end
end
