require 'rails_helper'

RSpec.describe "booking_histories/index", :type => :view do
  before(:each) do
    assign(:booking_histories, [
      BookingHistory.create!(
        :booking_id => "Booking",
        :status => false,
        :status_user_id => "Status User",
        :is_close => false
      ),
      BookingHistory.create!(
        :booking_id => "Booking",
        :status => false,
        :status_user_id => "Status User",
        :is_close => false
      )
    ])
  end

  it "renders a list of booking_histories" do
    render
    assert_select "tr>td", :text => "Booking".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "Status User".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
