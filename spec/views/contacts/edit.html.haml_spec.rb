require 'rails_helper'

RSpec.describe "contacts/edit", :type => :view do
  before(:each) do
    @contact = assign(:contact, Contact.create!(
      :contact_type_id => "MyString",
      :person_id => "MyString",
      :name => "MyString",
      :is_close => false
    ))
  end

  it "renders the edit contact form" do
    render

    assert_select "form[action=?][method=?]", contact_path(@contact), "post" do

      assert_select "input#contact_contact_type_id[name=?]", "contact[contact_type_id]"

      assert_select "input#contact_person_id[name=?]", "contact[person_id]"

      assert_select "input#contact_name[name=?]", "contact[name]"

      assert_select "input#contact_is_close[name=?]", "contact[is_close]"
    end
  end
end
