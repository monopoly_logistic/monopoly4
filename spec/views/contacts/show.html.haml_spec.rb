require 'rails_helper'

RSpec.describe "contacts/show", :type => :view do
  before(:each) do
    @contact = assign(:contact, Contact.create!(
      :contact_type_id => "Contact Type",
      :person_id => "Person",
      :name => "Name",
      :is_close => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Contact Type/)
    expect(rendered).to match(/Person/)
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/false/)
  end
end
