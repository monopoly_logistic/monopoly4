require 'rails_helper'

RSpec.describe "contacts/new", :type => :view do
  before(:each) do
    assign(:contact, Contact.new(
      :contact_type_id => "MyString",
      :person_id => "MyString",
      :name => "MyString",
      :is_close => false
    ))
  end

  it "renders new contact form" do
    render

    assert_select "form[action=?][method=?]", contacts_path, "post" do

      assert_select "input#contact_contact_type_id[name=?]", "contact[contact_type_id]"

      assert_select "input#contact_person_id[name=?]", "contact[person_id]"

      assert_select "input#contact_name[name=?]", "contact[name]"

      assert_select "input#contact_is_close[name=?]", "contact[is_close]"
    end
  end
end
