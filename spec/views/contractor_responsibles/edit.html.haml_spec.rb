require 'rails_helper'

RSpec.describe "contractor_responsibles/edit", :type => :view do
  before(:each) do
    @contractor_responsible = assign(:contractor_responsible, ContractorResponsible.create!(
      :contractor_id => "MyString",
      :employee_id => "MyString",
      :is_close => false
    ))
  end

  it "renders the edit contractor_responsible form" do
    render

    assert_select "form[action=?][method=?]", contractor_responsible_path(@contractor_responsible), "post" do

      assert_select "input#contractor_responsible_contractor_id[name=?]", "contractor_responsible[contractor_id]"

      assert_select "input#contractor_responsible_employee_id[name=?]", "contractor_responsible[employee_id]"

      assert_select "input#contractor_responsible_is_close[name=?]", "contractor_responsible[is_close]"
    end
  end
end
