require 'rails_helper'

RSpec.describe "contractor_responsibles/show", :type => :view do
  before(:each) do
    @contractor_responsible = assign(:contractor_responsible, ContractorResponsible.create!(
      :contractor_id => "Contractor",
      :employee_id => "Employee",
      :is_close => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Contractor/)
    expect(rendered).to match(/Employee/)
    expect(rendered).to match(/false/)
  end
end
