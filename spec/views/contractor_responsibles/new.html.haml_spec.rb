require 'rails_helper'

RSpec.describe "contractor_responsibles/new", :type => :view do
  before(:each) do
    assign(:contractor_responsible, ContractorResponsible.new(
      :contractor_id => "MyString",
      :employee_id => "MyString",
      :is_close => false
    ))
  end

  it "renders new contractor_responsible form" do
    render

    assert_select "form[action=?][method=?]", contractor_responsibles_path, "post" do

      assert_select "input#contractor_responsible_contractor_id[name=?]", "contractor_responsible[contractor_id]"

      assert_select "input#contractor_responsible_employee_id[name=?]", "contractor_responsible[employee_id]"

      assert_select "input#contractor_responsible_is_close[name=?]", "contractor_responsible[is_close]"
    end
  end
end
