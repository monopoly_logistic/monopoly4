require 'rails_helper'

RSpec.describe "contractor_analysis_parameters/show", :type => :view do
  before(:each) do
    @contractor_analysis_parameter = assign(:contractor_analysis_parameter, ContractorAnalysisParameter.create!(
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
  end
end
