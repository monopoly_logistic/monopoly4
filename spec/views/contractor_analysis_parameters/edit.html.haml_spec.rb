require 'rails_helper'

RSpec.describe "contractor_analysis_parameters/edit", :type => :view do
  before(:each) do
    @contractor_analysis_parameter = assign(:contractor_analysis_parameter, ContractorAnalysisParameter.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit contractor_analysis_parameter form" do
    render

    assert_select "form[action=?][method=?]", contractor_analysis_parameter_path(@contractor_analysis_parameter), "post" do

      assert_select "input#contractor_analysis_parameter_name[name=?]", "contractor_analysis_parameter[name]"
    end
  end
end
