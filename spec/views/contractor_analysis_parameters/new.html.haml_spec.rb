require 'rails_helper'

RSpec.describe "contractor_analysis_parameters/new", :type => :view do
  before(:each) do
    assign(:contractor_analysis_parameter, ContractorAnalysisParameter.new(
      :name => "MyString"
    ))
  end

  it "renders new contractor_analysis_parameter form" do
    render

    assert_select "form[action=?][method=?]", contractor_analysis_parameters_path, "post" do

      assert_select "input#contractor_analysis_parameter_name[name=?]", "contractor_analysis_parameter[name]"
    end
  end
end
