require 'rails_helper'

RSpec.describe "extra_spendings/edit", :type => :view do
  before(:each) do
    @extra_spending = assign(:extra_spending, ExtraSpending.create!(
      :person_id => "MyString",
      :transport_id => "MyString",
      :extra_spending_type_id => "MyString",
      :spending_sum => "9.99",
      :is_urgently => false,
      :is_required_payment => false,
      :user_id => "MyString",
      :agreement_id => "MyString",
      :agreement_result => 1,
      :agr_text => "MyText",
      :payment_request_id => "MyString",
      :is_request_status => false,
      :request_error => "MyText",
      :agreement_request_id => "MyString",
      :request_result => 1,
      :agreed_sum => "9.99",
      :is_close => ""
    ))
  end

  it "renders the edit extra_spending form" do
    render

    assert_select "form[action=?][method=?]", extra_spending_path(@extra_spending), "post" do

      assert_select "input#extra_spending_person_id[name=?]", "extra_spending[person_id]"

      assert_select "input#extra_spending_transport_id[name=?]", "extra_spending[transport_id]"

      assert_select "input#extra_spending_extra_spending_type_id[name=?]", "extra_spending[extra_spending_type_id]"

      assert_select "input#extra_spending_spending_sum[name=?]", "extra_spending[spending_sum]"

      assert_select "input#extra_spending_is_urgently[name=?]", "extra_spending[is_urgently]"

      assert_select "input#extra_spending_is_required_payment[name=?]", "extra_spending[is_required_payment]"

      assert_select "input#extra_spending_user_id[name=?]", "extra_spending[user_id]"

      assert_select "input#extra_spending_agreement_id[name=?]", "extra_spending[agreement_id]"

      assert_select "input#extra_spending_agreement_result[name=?]", "extra_spending[agreement_result]"

      assert_select "textarea#extra_spending_agr_text[name=?]", "extra_spending[agr_text]"

      assert_select "input#extra_spending_payment_request_id[name=?]", "extra_spending[payment_request_id]"

      assert_select "input#extra_spending_is_request_status[name=?]", "extra_spending[is_request_status]"

      assert_select "textarea#extra_spending_request_error[name=?]", "extra_spending[request_error]"

      assert_select "input#extra_spending_agreement_request_id[name=?]", "extra_spending[agreement_request_id]"

      assert_select "input#extra_spending_request_result[name=?]", "extra_spending[request_result]"

      assert_select "input#extra_spending_agreed_sum[name=?]", "extra_spending[agreed_sum]"

      assert_select "input#extra_spending_is_close[name=?]", "extra_spending[is_close]"
    end
  end
end
