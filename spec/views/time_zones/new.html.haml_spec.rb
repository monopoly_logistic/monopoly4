require 'rails_helper'

RSpec.describe "time_zones/new", :type => :view do
  before(:each) do
    assign(:time_zone, TimeZone.new(
      :area_code => "MyString",
      :utc => 1,
      :is_close => false
    ))
  end

  it "renders new time_zone form" do
    render

    assert_select "form[action=?][method=?]", time_zones_path, "post" do

      assert_select "input#time_zone_area_code[name=?]", "time_zone[area_code]"

      assert_select "input#time_zone_utc[name=?]", "time_zone[utc]"

      assert_select "input#time_zone_is_close[name=?]", "time_zone[is_close]"
    end
  end
end
