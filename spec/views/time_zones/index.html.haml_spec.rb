require 'rails_helper'

RSpec.describe "time_zones/index", :type => :view do
  before(:each) do
    assign(:time_zones, [
      TimeZone.create!(
        :area_code => "Area Code",
        :utc => 1,
        :is_close => false
      ),
      TimeZone.create!(
        :area_code => "Area Code",
        :utc => 1,
        :is_close => false
      )
    ])
  end

  it "renders a list of time_zones" do
    render
    assert_select "tr>td", :text => "Area Code".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
