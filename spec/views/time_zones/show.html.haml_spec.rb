require 'rails_helper'

RSpec.describe "time_zones/show", :type => :view do
  before(:each) do
    @time_zone = assign(:time_zone, TimeZone.create!(
      :area_code => "Area Code",
      :utc => 1,
      :is_close => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Area Code/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/false/)
  end
end
