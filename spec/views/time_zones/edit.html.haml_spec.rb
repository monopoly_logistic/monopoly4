require 'rails_helper'

RSpec.describe "time_zones/edit", :type => :view do
  before(:each) do
    @time_zone = assign(:time_zone, TimeZone.create!(
      :area_code => "MyString",
      :utc => 1,
      :is_close => false
    ))
  end

  it "renders the edit time_zone form" do
    render

    assert_select "form[action=?][method=?]", time_zone_path(@time_zone), "post" do

      assert_select "input#time_zone_area_code[name=?]", "time_zone[area_code]"

      assert_select "input#time_zone_utc[name=?]", "time_zone[utc]"

      assert_select "input#time_zone_is_close[name=?]", "time_zone[is_close]"
    end
  end
end
