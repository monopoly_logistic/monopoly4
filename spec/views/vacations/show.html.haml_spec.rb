require 'rails_helper'

RSpec.describe "vacations/show", :type => :view do
  before(:each) do
    @vacation = assign(:vacation, Vacation.create!(
      :employee_id => "Employee",
      :descr => "Descr",
      :is_close => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Employee/)
    expect(rendered).to match(/Descr/)
    expect(rendered).to match(/false/)
  end
end
