require 'rails_helper'

RSpec.describe "vacations/new", :type => :view do
  before(:each) do
    assign(:vacation, Vacation.new(
      :employee_id => "MyString",
      :descr => "MyString",
      :is_close => false
    ))
  end

  it "renders new vacation form" do
    render

    assert_select "form[action=?][method=?]", vacations_path, "post" do

      assert_select "input#vacation_employee_id[name=?]", "vacation[employee_id]"

      assert_select "input#vacation_descr[name=?]", "vacation[descr]"

      assert_select "input#vacation_is_close[name=?]", "vacation[is_close]"
    end
  end
end
