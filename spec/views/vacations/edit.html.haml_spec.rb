require 'rails_helper'

RSpec.describe "vacations/edit", :type => :view do
  before(:each) do
    @vacation = assign(:vacation, Vacation.create!(
      :employee_id => "MyString",
      :descr => "MyString",
      :is_close => false
    ))
  end

  it "renders the edit vacation form" do
    render

    assert_select "form[action=?][method=?]", vacation_path(@vacation), "post" do

      assert_select "input#vacation_employee_id[name=?]", "vacation[employee_id]"

      assert_select "input#vacation_descr[name=?]", "vacation[descr]"

      assert_select "input#vacation_is_close[name=?]", "vacation[is_close]"
    end
  end
end
