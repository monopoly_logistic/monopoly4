require 'rails_helper'

RSpec.describe "vacations/index", :type => :view do
  before(:each) do
    assign(:vacations, [
      Vacation.create!(
        :employee_id => "Employee",
        :descr => "Descr",
        :is_close => false
      ),
      Vacation.create!(
        :employee_id => "Employee",
        :descr => "Descr",
        :is_close => false
      )
    ])
  end

  it "renders a list of vacations" do
    render
    assert_select "tr>td", :text => "Employee".to_s, :count => 2
    assert_select "tr>td", :text => "Descr".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
