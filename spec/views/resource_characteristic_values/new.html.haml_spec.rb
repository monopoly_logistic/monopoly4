require 'rails_helper'

RSpec.describe "resource_characteristic_values/new", :type => :view do
  before(:each) do
    assign(:resource_characteristic_value, ResourceCharacteristicValue.new(
      :resource_characteristic_id => "MyString",
      :truck_id => "MyString",
      :trailer_id => "MyString",
      :driver_id => "MyString",
      :name => "MyString",
      :is_close => false
    ))
  end

  it "renders new resource_characteristic_value form" do
    render

    assert_select "form[action=?][method=?]", resource_characteristic_values_path, "post" do

      assert_select "input#resource_characteristic_value_resource_characteristic_id[name=?]", "resource_characteristic_value[resource_characteristic_id]"

      assert_select "input#resource_characteristic_value_truck_id[name=?]", "resource_characteristic_value[truck_id]"

      assert_select "input#resource_characteristic_value_trailer_id[name=?]", "resource_characteristic_value[trailer_id]"

      assert_select "input#resource_characteristic_value_driver_id[name=?]", "resource_characteristic_value[driver_id]"

      assert_select "input#resource_characteristic_value_name[name=?]", "resource_characteristic_value[name]"

      assert_select "input#resource_characteristic_value_is_close[name=?]", "resource_characteristic_value[is_close]"
    end
  end
end
