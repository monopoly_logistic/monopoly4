require 'rails_helper'

RSpec.describe "resource_characteristic_values/index", :type => :view do
  before(:each) do
    assign(:resource_characteristic_values, [
      ResourceCharacteristicValue.create!(
        :resource_characteristic_id => "Resource Characteristic",
        :truck_id => "Truck",
        :trailer_id => "Trailer",
        :driver_id => "Driver",
        :name => "Name",
        :is_close => false
      ),
      ResourceCharacteristicValue.create!(
        :resource_characteristic_id => "Resource Characteristic",
        :truck_id => "Truck",
        :trailer_id => "Trailer",
        :driver_id => "Driver",
        :name => "Name",
        :is_close => false
      )
    ])
  end

  it "renders a list of resource_characteristic_values" do
    render
    assert_select "tr>td", :text => "Resource Characteristic".to_s, :count => 2
    assert_select "tr>td", :text => "Truck".to_s, :count => 2
    assert_select "tr>td", :text => "Trailer".to_s, :count => 2
    assert_select "tr>td", :text => "Driver".to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
