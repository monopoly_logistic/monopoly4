require 'rails_helper'

RSpec.describe "truck_comments/index", :type => :view do
  before(:each) do
    assign(:truck_comments, [
      TruckComment.create!(
        :truck_id => "Truck",
        :comment => "MyText",
        :user_id => "User",
        :is_close => false
      ),
      TruckComment.create!(
        :truck_id => "Truck",
        :comment => "MyText",
        :user_id => "User",
        :is_close => false
      )
    ])
  end

  it "renders a list of truck_comments" do
    render
    assert_select "tr>td", :text => "Truck".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "User".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
