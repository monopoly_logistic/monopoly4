require 'rails_helper'

RSpec.describe "truck_comments/show", :type => :view do
  before(:each) do
    @truck_comment = assign(:truck_comment, TruckComment.create!(
      :truck_id => "Truck",
      :comment => "MyText",
      :user_id => "User",
      :is_close => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Truck/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/User/)
    expect(rendered).to match(/false/)
  end
end
