require 'rails_helper'

RSpec.describe "bookings/show", :type => :view do
  before(:each) do
    @booking = assign(:booking, Booking.create!(
      :booking_type_id => "Booking Type",
      :truck_id => "Truck",
      :trailer_id => "Trailer",
      :driver_id => "Driver",
      :user_id => "User",
      :comment => "MyText",
      :status => false,
      :status_user_id => "Status User",
      :source_id => "Source",
      :target_id => "Target"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Booking Type/)
    expect(rendered).to match(/Truck/)
    expect(rendered).to match(/Trailer/)
    expect(rendered).to match(/Driver/)
    expect(rendered).to match(/User/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/Status User/)
    expect(rendered).to match(/Source/)
    expect(rendered).to match(/Target/)
  end
end
