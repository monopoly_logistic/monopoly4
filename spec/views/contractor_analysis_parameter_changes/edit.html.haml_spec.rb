require 'rails_helper'

RSpec.describe "contractor_analysis_parameter_changes/edit", :type => :view do
  before(:each) do
    @contractor_analysis_parameter_change = assign(:contractor_analysis_parameter_change, ContractorAnalysisParameterChange.create!(
      :contractor_analysis_parameter_id => "MyString",
      :parameter_value => "",
      :is_close => false
    ))
  end

  it "renders the edit contractor_analysis_parameter_change form" do
    render

    assert_select "form[action=?][method=?]", contractor_analysis_parameter_change_path(@contractor_analysis_parameter_change), "post" do

      assert_select "input#contractor_analysis_parameter_change_contractor_analysis_parameter_id[name=?]", "contractor_analysis_parameter_change[contractor_analysis_parameter_id]"

      assert_select "input#contractor_analysis_parameter_change_parameter_value[name=?]", "contractor_analysis_parameter_change[parameter_value]"

      assert_select "input#contractor_analysis_parameter_change_is_close[name=?]", "contractor_analysis_parameter_change[is_close]"
    end
  end
end
