require 'rails_helper'

RSpec.describe "contractor_analysis_parameter_changes/index", :type => :view do
  before(:each) do
    assign(:contractor_analysis_parameter_changes, [
      ContractorAnalysisParameterChange.create!(
        :contractor_analysis_parameter_id => "Contractor Analysis Parameter",
        :parameter_value => "",
        :is_close => false
      ),
      ContractorAnalysisParameterChange.create!(
        :contractor_analysis_parameter_id => "Contractor Analysis Parameter",
        :parameter_value => "",
        :is_close => false
      )
    ])
  end

  it "renders a list of contractor_analysis_parameter_changes" do
    render
    assert_select "tr>td", :text => "Contractor Analysis Parameter".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
