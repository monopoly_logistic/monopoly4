require 'rails_helper'

RSpec.describe "transportmodel1cs/show", :type => :view do
  before(:each) do
    @transportmodel1c = assign(:transportmodel1c, Transportmodel1c.create!(
      :transportmodel_1c_id => "Transportmodel 1c",
      :transportmodel_1c_name => "Transportmodel 1c Name",
      :transportmodel_id => "Transportmodel",
      :is_close => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Transportmodel 1c/)
    expect(rendered).to match(/Transportmodel 1c Name/)
    expect(rendered).to match(/Transportmodel/)
    expect(rendered).to match(/false/)
  end
end
