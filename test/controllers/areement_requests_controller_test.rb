require 'test_helper'

class AreementRequestsControllerTest < ActionController::TestCase
  setup do
    @agreement_request = Agreement_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:@agreement_requests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create agreement_request" do
    assert_difference('AreementRequest.count') do
      post :create, agreement_request: { agreement_id: @agreement_request.agreement_id, business_object_id: @agreement_request.business_object_id, foreign_id: @agreement_request.foreign_id, initiator_id: @agreement_request.initiator_id, req_text: @agreement_request.req_text, start_date: @agreement_request.start_date, status: @agreement_request.status, status_updated_at: @agreement_request.status_updated_at }
    end

    assert_redirected_to agreement_request_path(assigns(:agreement_request))
  end

  test "should show agreement_request" do
    get :show, id: @agreement_request
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @agreement_request
    assert_response :success
  end

  test "should update agreement_request" do
    patch :update, id: @agreement_request, agreement_request: { agreement_id: @agreement_request.agreement_id, business_object_id: @agreement_request.business_object_id, foreign_id: @agreement_request.foreign_id, initiator_id: @agreement_request.initiator_id, req_text: @agreement_request.req_text, start_date: @agreement_request.start_date, status: @agreement_request.status, status_updated_at: @agreement_request.status_updated_at }
    assert_redirected_to agreement_request_path(assigns(:agreement_request))
  end

  test "should destroy agreement_request" do
    assert_difference('AreementRequest.count', -1) do
      delete :destroy, id: @agreement_request
    end

    assert_redirected_to agreement_requests_path
  end
end
