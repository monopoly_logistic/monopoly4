require 'test_helper'

class ProcessActionsControllerTest < ActionController::TestCase
  setup do
    @process_action = process_actions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:process_actions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create process_action" do
    assert_difference('ProcessAction.count') do
      post :create, process_action: { descr: @process_action.descr, is_close: @process_action.is_close, name: @process_action.name }
    end

    assert_redirected_to process_action_path(assigns(:process_action))
  end

  test "should show process_action" do
    get :show, id: @process_action
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @process_action
    assert_response :success
  end

  test "should update process_action" do
    patch :update, id: @process_action, process_action: { descr: @process_action.descr, is_close: @process_action.is_close, name: @process_action.name }
    assert_redirected_to process_action_path(assigns(:process_action))
  end

  test "should destroy process_action" do
    assert_difference('ProcessAction.count', -1) do
      delete :destroy, id: @process_action
    end

    assert_redirected_to process_actions_path
  end
end
