require 'test_helper'

class PayslipsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get show" do
    get :show
    assert_response :success
  end

  test "should get for_print" do
    get :for_print
    assert_response :success
  end

end
