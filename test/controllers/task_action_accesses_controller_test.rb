require 'test_helper'

class TaskActionAccessesControllerTest < ActionController::TestCase
  setup do
    @task_action_access = task_action_accesses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:task_action_accesses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create task_action_access" do
    assert_difference('TaskActionAccess.count') do
      post :create, task_action_access: { employee_id: @task_action_access.employee_id, end_date: @task_action_access.end_date, is_close: @task_action_access.is_close, staff_unit_id: @task_action_access.staff_unit_id, start_date: @task_action_access.start_date, task_action_id: @task_action_access.task_action_id, task_template_id: @task_action_access.task_template_id }
    end

    assert_redirected_to task_action_access_path(assigns(:task_action_access))
  end

  test "should show task_action_access" do
    get :show, id: @task_action_access
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @task_action_access
    assert_response :success
  end

  test "should update task_action_access" do
    patch :update, id: @task_action_access, task_action_access: { employee_id: @task_action_access.employee_id, end_date: @task_action_access.end_date, is_close: @task_action_access.is_close, staff_unit_id: @task_action_access.staff_unit_id, start_date: @task_action_access.start_date, task_action_id: @task_action_access.task_action_id, task_template_id: @task_action_access.task_template_id }
    assert_redirected_to task_action_access_path(assigns(:task_action_access))
  end

  test "should destroy task_action_access" do
    assert_difference('TaskActionAccess.count', -1) do
      delete :destroy, id: @task_action_access
    end

    assert_redirected_to task_action_accesses_path
  end
end
