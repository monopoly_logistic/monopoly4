require 'test_helper'

class TemplateStepsControllerTest < ActionController::TestCase
  setup do
    @template_step = template_steps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:template_steps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create template_step" do
    assert_difference('TemplateStep.count') do
      post :create, template_step: { is_close: @template_step.is_close, lft: @template_step.lft, name: @template_step.name, parent2_id: @template_step.parent2_id, parent_id: @template_step.parent_id, process_template_id: @template_step.process_template_id, rgt: @template_step.rgt, task_template_id: @template_step.task_template_id }
    end

    assert_redirected_to template_step_path(assigns(:template_step))
  end

  test "should show template_step" do
    get :show, id: @template_step
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @template_step
    assert_response :success
  end

  test "should update template_step" do
    patch :update, id: @template_step, template_step: { is_close: @template_step.is_close, lft: @template_step.lft, name: @template_step.name, parent2_id: @template_step.parent2_id, parent_id: @template_step.parent_id, process_template_id: @template_step.process_template_id, rgt: @template_step.rgt, task_template_id: @template_step.task_template_id }
    assert_redirected_to template_step_path(assigns(:template_step))
  end

  test "should destroy template_step" do
    assert_difference('TemplateStep.count', -1) do
      delete :destroy, id: @template_step
    end

    assert_redirected_to template_steps_path
  end
end
