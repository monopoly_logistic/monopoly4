require 'test_helper'

class TaskActionsControllerTest < ActionController::TestCase
  setup do
    @task_action = task_actions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:task_actions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create task_action" do
    assert_difference('TaskAction.count') do
      post :create, task_action: { descr: @task_action.descr, is_close: @task_action.is_close, name: @task_action.name }
    end

    assert_redirected_to task_action_path(assigns(:task_action))
  end

  test "should show task_action" do
    get :show, id: @task_action
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @task_action
    assert_response :success
  end

  test "should update task_action" do
    patch :update, id: @task_action, task_action: { descr: @task_action.descr, is_close: @task_action.is_close, name: @task_action.name }
    assert_redirected_to task_action_path(assigns(:task_action))
  end

  test "should destroy task_action" do
    assert_difference('TaskAction.count', -1) do
      delete :destroy, id: @task_action
    end

    assert_redirected_to task_actions_path
  end
end
