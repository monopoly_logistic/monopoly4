require 'test_helper'

class ContractorRepresentativesControllerTest < ActionController::TestCase
  setup do
    @contractor_representative = contractor_representatives(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:contractor_representatives)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create contractor_representative" do
    assert_difference('ContractorRepresentative.count') do
      post :create, contractor_representative: { contractor_id: @contractor_representative.contractor_id, end_date: @contractor_representative.end_date, id: @contractor_representative.id, is_close: @contractor_representative.is_close, person_id: @contractor_representative.person_id, position_id: @contractor_representative.position_id, start_date: @contractor_representative.start_date }
    end

    assert_redirected_to contractor_representative_path(assigns(:contractor_representative))
  end

  test "should show contractor_representative" do
    get :show, id: @contractor_representative
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @contractor_representative
    assert_response :success
  end

  test "should update contractor_representative" do
    put :update, id: @contractor_representative, contractor_representative: { contractor_id: @contractor_representative.contractor_id, end_date: @contractor_representative.end_date, id: @contractor_representative.id, is_close: @contractor_representative.is_close, person_id: @contractor_representative.person_id, position_id: @contractor_representative.position_id, start_date: @contractor_representative.start_date }
    assert_redirected_to contractor_representative_path(assigns(:contractor_representative))
  end

  test "should destroy contractor_representative" do
    assert_difference('ContractorRepresentative.count', -1) do
      delete :destroy, id: @contractor_representative
    end

    assert_redirected_to contractor_representatives_path
  end
end
