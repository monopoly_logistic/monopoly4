require 'test_helper'

class CouplersControllerTest < ActionController::TestCase
  setup do
    @coupler = couplers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:couplers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create coupler" do
    assert_difference('Coupler.count') do
      post :create, coupler: { end_date: @coupler.end_date, id: @coupler.id, start_date: @coupler.start_date, trailer: @coupler.trailer, truck: @coupler.truck }
    end

    assert_redirected_to coupler_path(assigns(:coupler))
  end

  test "should show coupler" do
    get :show, id: @coupler
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @coupler
    assert_response :success
  end

  test "should update coupler" do
    put :update, id: @coupler, coupler: { end_date: @coupler.end_date, id: @coupler.id, start_date: @coupler.start_date, trailer: @coupler.trailer, truck: @coupler.truck }
    assert_redirected_to coupler_path(assigns(:coupler))
  end

  test "should destroy coupler" do
    assert_difference('Coupler.count', -1) do
      delete :destroy, id: @coupler
    end

    assert_redirected_to couplers_path
  end
end
