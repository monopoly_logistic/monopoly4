require 'test_helper'

class UnvCommentsControllerTest < ActionController::TestCase
  setup do
    @unv_comment = unv_comments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:unv_comments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create unv_comment" do
    assert_difference('UnvComment.count') do
      post :create, unv_comment: { OrderFromKey: @unv_comment.OrderFromKey, OrderToKey: @unv_comment.OrderToKey, comment: @unv_comment.comment, id: @unv_comment.id, user_id: @unv_comment.user_id }
    end

    assert_redirected_to unv_comment_path(assigns(:unv_comment))
  end

  test "should show unv_comment" do
    get :show, id: @unv_comment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @unv_comment
    assert_response :success
  end

  test "should update unv_comment" do
    put :update, id: @unv_comment, unv_comment: { OrderFromKey: @unv_comment.OrderFromKey, OrderToKey: @unv_comment.OrderToKey, comment: @unv_comment.comment, id: @unv_comment.id, user_id: @unv_comment.user_id }
    assert_redirected_to unv_comment_path(assigns(:unv_comment))
  end

  test "should destroy unv_comment" do
    assert_difference('UnvComment.count', -1) do
      delete :destroy, id: @unv_comment
    end

    assert_redirected_to unv_comments_path
  end
end
